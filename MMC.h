#ifndef MMC_H
#define MMC_H

#include <TLorentzVector.h>

class MMC {
  public:
    MMC();
    TVector2 correctMET(TVector2 MET, TLorentzVector lepton, TLorentzVector lepFV, double METscanPoints, double sigma);
    double massScanMMC(TVector2 MET, TLorentzVector lepton, TLorentzVector lepFV, double scanPoints);
    double massScanMMCWithoutHisto(TVector2 MET, TLorentzVector lepton, TLorentzVector lepFV, double scanPoints);
    double energyScanMMC(TVector2 MET, TLorentzVector lepton, TLorentzVector lepFV, double scanPoints);
    double mCollProj(TLorentzVector lepton, TLorentzVector lepFV, TVector2 MET);
    double mCollATLAS(TLorentzVector lepton, TLorentzVector lepFV, TVector2 MET);
    double calculateWeightMassScan(double mNu);
    double fittedMassWeight(double mNu);
    vector<double> calculateWeightEnergyScanCloserPz(double nuEnergy, TVector2 MET, TLorentzVector lepton);
    vector<double> calculateWeightEnergyScanSameSignPz(double nuEnergy, TVector2 MET, TLorentzVector lepton);
    vector<double> calculateWeightEnergyScanCloserPzVector(double nuEnergy, TVector2 MET, TLorentzVector lepton);
    vector<double> calculateWeightEnergyScanSameSignPzVector(double nuEnergy, TVector2 MET, TLorentzVector lepton);
    double calculateMassEnergyScanCloserPz(double nuEnergy, TLorentzVector lepton, TLorentzVector higgsLepton, TVector2 MET);
    double calculateMassEnergyScanSameSignPz(double nuEnergy, TLorentzVector lepton, TLorentzVector higgsLepton, TVector2 MET);
    double calculateMassEnergyScanPlus(double nuEnergy, TLorentzVector lepton, TLorentzVector higgsLepton, TVector2 MET);
    double calculateMassEnergyScanMinus(double nuEnergy, TLorentzVector lepton, TLorentzVector higgsLepton, TVector2 MET);
    double nuZComponentPlus(TLorentzVector lepton, TVector2 MET, double nuEnergy);
    double nuZComponentMinus(TLorentzVector lepton, TVector2 MET, double nuEnergy);
    // scan with four solutions
    double calculateMassScanningM(double mNu, TLorentzVector lepton, TLorentzVector lepFV, TVector2 MET);
    double nuZUsingNuMassPlus(TVector2 MET, double nuM, double nuE);
    double nuZUsingNuMassMinus(TVector2 MET, double nuM, double nuE);
    double nuSystemEnergyPlus(TVector2 MET, TLorentzVector lepton,  double mNu);
    double nuSystemEnergyMinus(TVector2 MET, TLorentzVector lepton,  double mNu);
    // scan with mass scan and two solutions
    double pNuZPlus(TVector2 MET, TLorentzVector lepton, double mNu);
    double pNuZMinus(TVector2 MET, TLorentzVector lepton, double mNu);
    double calculateMassNuMScan(double mNu, TLorentzVector lepton, TLorentzVector lepFV, TVector2 MET);
    double calculateMassNuMScanPlus(double mNu, TLorentzVector lepton, TLorentzVector lepFV, TVector2 MET);
    double calculateMassNuMScanMinus(double mNu, TLorentzVector lepton, TLorentzVector lepFV, TVector2 MET);
    // lepton labeling from Uli Baumanns master thesis
    double fLimit(double phiLepton, double phiLepFV);
    bool leptonLabeling(TLorentzVector lepFV, TLorentzVector lepton, TVector2 MET);
  private:
    double nuEnergy(TVector2 MET, double pNuZ, double mNu);
    double leptonRestEnergy(TLorentzVector tau, TLorentzVector lepton, TLorentzVector nu);
    double tauLabEnergy(TLorentzVector tau, double leptonRestE, double leptonLabE);
    double nuLabEnergy(double tauE, double leptonE);
    TLorentzVector nuFourVector(TVector2 nu, double pz, double nuEnergy);
};
#endif // MMC_H
