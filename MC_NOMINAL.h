//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue May 10 17:52:40 2016 by ROOT version 6.04/14
// from TTree tau/tau
// found on file: /storage/groups/atlsch/uli/samples/GGF_TauE/user.ubaumann.mc12_8TeV.189894.PowHegPythia8_ggH125_taue.merge.e3057_s1773_s1776_r4485_r4540_p1344.hsg4_1.2_NTUP_TAU.root.13493252/user.ubaumann.4576451._000001.NTUP_TAU.root
//////////////////////////////////////////////////////////

#ifndef MC_NOMINAL_h
#define MC_NOMINAL_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"

class MC_NOMINAL {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Bool_t          EF_2e12Tvh_loose1;
   Bool_t          EF_2e7T_loose1_mu6;
   Bool_t          EF_2e7T_medium1_mu6;
   Bool_t          EF_2mu13;
   Bool_t          EF_2mu15;
   Bool_t          EF_e12Tvh_medium1_mu8;
   Bool_t          EF_e22vh_medium1;
   Bool_t          EF_e22vhi_medium1;
   Bool_t          EF_e24vh_medium1;
   Bool_t          EF_e24vhi_medium1;
   Bool_t          EF_e60_medium1;
   Bool_t          EF_e7T_loose1_2mu6;
   Bool_t          EF_e7T_medium1_2mu6;
   Bool_t          EF_mu18_medium;
   Bool_t          EF_mu18_tight;
   Bool_t          EF_mu18_tight_2mu4_EFFS;
   Bool_t          EF_mu18_tight_e7_medium1;
   Bool_t          EF_mu18_tight_mu8_EFFS;
   Bool_t          EF_mu24_medium;
   Bool_t          EF_mu24_tight;
   Bool_t          EF_mu24_tight_mu6_EFFS;
   Bool_t          EF_mu24i_tight;
   Bool_t          EF_mu36_tight;
   Bool_t          EF_mu40_MSonly_barrel_tight;
   Bool_t          EF_tau20T_medium1_e15vh_medium1;
   Bool_t          EF_tau20T_medium_mu15;
   Bool_t          EF_tau20_medium1_mu15;
   vector<int>     *trig_EF_tau_EF_tau20T_medium1_e15vh_medium1;
   vector<int>     *trig_EF_tau_EF_tau20T_medium_mu15;
   vector<int>     *trig_EF_tau_EF_tau20_medium1_mu15;
   vector<int>     *trig_L2_combmuonfeature_L2_2mu13;
   vector<int>     *trig_L2_combmuonfeature_L2_2mu15;
   vector<int>     *trig_L2_combmuonfeature_L2_2mu6;
   vector<int>     *trig_L2_combmuonfeature_L2_mu15;
   vector<int>     *trig_L2_combmuonfeature_L2_mu18_medium;
   vector<int>     *trig_L2_combmuonfeature_L2_mu18_tight;
   vector<int>     *trig_L2_combmuonfeature_L2_mu18_tight_e7_medium1;
   vector<int>     *trig_L2_combmuonfeature_L2_mu24_medium;
   vector<int>     *trig_L2_combmuonfeature_L2_mu24_tight;
   vector<int>     *trig_L2_combmuonfeature_L2_mu36_tight;
   vector<int>     *trig_L2_combmuonfeature_L2_mu6;
   vector<int>     *trig_L2_combmuonfeature_L2_mu8;
   vector<int>     *trig_EF_trigmuonef_EF_2mu13;
   vector<int>     *trig_EF_trigmuonef_EF_2mu15;
   vector<int>     *trig_EF_trigmuonef_EF_2mu6;
   vector<int>     *trig_EF_trigmuonef_EF_mu15;
   vector<int>     *trig_EF_trigmuonef_EF_mu18_medium;
   vector<int>     *trig_EF_trigmuonef_EF_mu18_tight;
   vector<int>     *trig_EF_trigmuonef_EF_mu18_tight_2mu4_EFFS;
   vector<int>     *trig_EF_trigmuonef_EF_mu18_tight_e7_medium1;
   vector<int>     *trig_EF_trigmuonef_EF_mu18_tight_mu8_EFFS;
   vector<int>     *trig_EF_trigmuonef_EF_mu24_medium;
   vector<int>     *trig_EF_trigmuonef_EF_mu24_tight;
   vector<int>     *trig_EF_trigmuonef_EF_mu24_tight_mu6_EFFS;
   vector<int>     *trig_EF_trigmuonef_EF_mu24i_tight;
   vector<int>     *trig_EF_trigmuonef_EF_mu36_tight;
   vector<int>     *trig_EF_trigmuonef_EF_mu40_MSonly_barrel_tight;
   vector<int>     *trig_EF_trigmuonef_EF_mu6;
   vector<int>     *trig_EF_trigmuonef_EF_mu8;
   vector<int>     *trig_EF_trigmugirl_EF_2mu13;
   vector<int>     *trig_EF_trigmugirl_EF_2mu15;
   vector<int>     *trig_EF_trigmugirl_EF_2mu6;
   vector<int>     *trig_EF_trigmugirl_EF_mu15;
   vector<int>     *trig_EF_trigmugirl_EF_mu18_medium;
   vector<int>     *trig_EF_trigmugirl_EF_mu18_tight;
   vector<int>     *trig_EF_trigmugirl_EF_mu18_tight_2mu4_EFFS;
   vector<int>     *trig_EF_trigmugirl_EF_mu18_tight_e7_medium1;
   vector<int>     *trig_EF_trigmugirl_EF_mu18_tight_mu8_EFFS;
   vector<int>     *trig_EF_trigmugirl_EF_mu24_medium;
   vector<int>     *trig_EF_trigmugirl_EF_mu24_tight;
   vector<int>     *trig_EF_trigmugirl_EF_mu24_tight_mu6_EFFS;
   vector<int>     *trig_EF_trigmugirl_EF_mu24i_tight;
   vector<int>     *trig_EF_trigmugirl_EF_mu36_tight;
   vector<int>     *trig_EF_trigmugirl_EF_mu40_MSonly_barrel_tight;
   vector<int>     *trig_EF_trigmugirl_EF_mu6;
   vector<int>     *trig_EF_trigmugirl_EF_mu8;
   Int_t           tau_n;
   vector<float>   *tau_Et;
   vector<float>   *tau_pt;
   vector<float>   *tau_m;
   vector<float>   *tau_eta;
   vector<float>   *tau_phi;
   vector<float>   *tau_charge;
   vector<float>   *tau_BDTEleScore;
   vector<float>   *tau_BDTJetScore;
   vector<float>   *tau_likelihood;
   vector<float>   *tau_SafeLikelihood;
   vector<int>     *tau_electronVetoLoose;
   vector<int>     *tau_electronVetoMedium;
   vector<int>     *tau_electronVetoTight;
   vector<int>     *tau_muonVeto;
   vector<int>     *tau_tauLlhLoose;
   vector<int>     *tau_tauLlhMedium;
   vector<int>     *tau_tauLlhTight;
   vector<int>     *tau_JetBDTSigLoose;
   vector<int>     *tau_JetBDTSigMedium;
   vector<int>     *tau_JetBDTSigTight;
   vector<int>     *tau_EleBDTLoose;
   vector<int>     *tau_EleBDTMedium;
   vector<int>     *tau_EleBDTTight;
   vector<int>     *tau_author;
   vector<int>     *tau_numTrack;
   vector<float>   *tau_etOverPtLeadTrk;
   vector<float>   *tau_leadTrkPt;
   vector<float>   *tau_jet_jvtxf;
   vector<int>     *tau_track_n;
   vector<vector<float> > *tau_track_d0;
   vector<vector<float> > *tau_track_z0;
   vector<vector<float> > *tau_track_phi;
   vector<vector<float> > *tau_track_theta;
   vector<vector<float> > *tau_track_qoverp;
   vector<vector<float> > *tau_track_pt;
   vector<vector<float> > *tau_track_eta;
   vector<vector<float> > *tau_track_atPV_d0;
   vector<vector<float> > *tau_track_atPV_z0;
   vector<vector<float> > *tau_track_atPV_phi;
   vector<vector<float> > *tau_track_atPV_theta;
   vector<vector<float> > *tau_track_atPV_qoverp;
   vector<vector<float> > *tau_track_atPV_pt;
   vector<vector<float> > *tau_track_atPV_eta;
   Int_t           mc_n;
   vector<float>   *mc_pt;
   vector<float>   *mc_m;
   vector<float>   *mc_eta;
   vector<float>   *mc_phi;
   vector<int>     *mc_status;
   vector<int>     *mc_barcode;
   vector<int>     *mc_pdgId;
   vector<float>   *mc_charge;
   vector<vector<int> > *mc_child_index;
   vector<vector<int> > *mc_parent_index;
   Int_t           jet_antikt4truth_n;
   vector<float>   *jet_antikt4truth_E;
   vector<float>   *jet_antikt4truth_pt;
   vector<float>   *jet_antikt4truth_m;
   vector<float>   *jet_antikt4truth_eta;
   vector<float>   *jet_antikt4truth_phi;
   vector<float>   *el_charge;
   vector<int>     *el_author;
   vector<unsigned int> *el_isEM;
   vector<unsigned int> *el_OQ;
   vector<int>     *el_loose;
   vector<int>     *el_medium;
   vector<int>     *el_tight;
   vector<int>     *el_loosePP;
   vector<int>     *el_mediumPP;
   vector<int>     *el_tightPP;
   vector<float>   *el_Ethad;
   vector<float>   *el_Ethad1;
   vector<float>   *el_f1;
   vector<float>   *el_Emax2;
   vector<float>   *el_wstot;
   vector<float>   *el_emaxs1;
   vector<float>   *el_E237;
   vector<float>   *el_E277;
   vector<float>   *el_weta2;
   vector<float>   *el_f3;
   vector<float>   *el_Etcone20;
   vector<float>   *el_Etcone30;
   vector<float>   *el_Etcone40;
   vector<float>   *el_ptcone20;
   vector<float>   *el_ptcone30;
   vector<float>   *el_ptcone40;
   vector<float>   *el_deltaeta1;
   vector<float>   *el_deltaphi2;
   vector<float>   *el_expectHitInBLayer;
   vector<float>   *el_trackd0_physics;
   vector<float>   *el_reta;
   vector<float>   *el_topoEtcone20;
   vector<float>   *el_topoEtcone30;
   vector<float>   *el_topoEtcone40;
   vector<float>   *el_etap;
   vector<float>   *el_Es2;
   vector<float>   *el_etas2;
   vector<float>   *el_phis2;
   vector<float>   *el_cl_E;
   vector<float>   *el_cl_pt;
   vector<float>   *el_cl_eta;
   vector<float>   *el_cl_phi;
   vector<float>   *el_trackphi;
   vector<float>   *el_trackqoverp;
   vector<float>   *el_trackpt;
   vector<float>   *el_tracketa;
   vector<int>     *el_nBLHits;
   vector<int>     *el_nPixHits;
   vector<int>     *el_nSCTHits;
   vector<int>     *el_nTRTHits;
   vector<int>     *el_nBLayerOutliers;
   vector<int>     *el_nPixelOutliers;
   vector<int>     *el_nSCTOutliers;
   vector<int>     *el_nTRTOutliers;
   vector<int>     *el_nSiHits;
   vector<float>   *el_TRTHighTOutliersRatio;
   vector<float>   *el_trackd0pv;
   vector<float>   *el_trackz0pv;
   vector<float>   *el_tracksigd0pv;
   vector<float>   *el_tracksigz0pv;
   vector<float>   *el_trackd0pvunbiased;
   vector<float>   *el_trackz0pvunbiased;
   vector<float>   *el_tracksigd0pvunbiased;
   vector<float>   *el_tracksigz0pvunbiased;
   vector<float>   *el_topoEtcone20_corrected;
   vector<float>   *el_topoEtcone30_corrected;
   vector<float>   *el_topoEtcone40_corrected;
   vector<float>   *el_ED_median;
   Int_t           jet_n;
   vector<float>   *jet_E;
   vector<float>   *jet_pt;
   vector<float>   *jet_m;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_EtaOrigin;
   vector<float>   *jet_PhiOrigin;
   vector<float>   *jet_MOrigin;
   vector<float>   *jet_Timing;
   vector<float>   *jet_LArQuality;
   vector<float>   *jet_sumPtTrk;
   vector<float>   *jet_HECQuality;
   vector<float>   *jet_NegativeE;
   vector<float>   *jet_AverageLArQF;
   vector<float>   *jet_BCH_CORR_CELL;
   vector<float>   *jet_BCH_CORR_DOTX;
   vector<float>   *jet_BCH_CORR_JET;
   vector<int>     *jet_SamplingMax;
   vector<float>   *jet_fracSamplingMax;
   vector<float>   *jet_hecf;
   vector<int>     *jet_isUgly;
   vector<int>     *jet_isBadLooseMinus;
   vector<int>     *jet_isBadLoose;
   vector<int>     *jet_isBadMedium;
   vector<int>     *jet_isBadTight;
   vector<float>   *jet_emfrac;
   vector<float>   *jet_Offset;
   vector<float>   *jet_EMJES;
   vector<float>   *jet_EMJES_EtaCorr;
   vector<float>   *jet_EMJESnooffset;
   vector<float>   *jet_emscale_E;
   vector<float>   *jet_emscale_pt;
   vector<float>   *jet_emscale_m;
   vector<float>   *jet_emscale_eta;
   vector<float>   *jet_emscale_phi;
   vector<float>   *jet_ActiveArea;
   vector<float>   *jet_ActiveAreaPx;
   vector<float>   *jet_ActiveAreaPy;
   vector<float>   *jet_ActiveAreaPz;
   vector<float>   *jet_ActiveAreaE;
   vector<float>   *jet_jvtxf;
   vector<vector<float> > *jet_jvtxfFull;
   vector<float>   *jet_jvtx_x;
   vector<float>   *jet_jvtx_y;
   vector<float>   *jet_jvtx_z;
   vector<float>   *jet_constscale_E;
   vector<float>   *jet_constscale_pt;
   vector<float>   *jet_constscale_m;
   vector<float>   *jet_constscale_eta;
   vector<float>   *jet_constscale_phi;
   vector<float>   *jet_flavor_weight_Comb;
   vector<float>   *jet_flavor_weight_IP2D;
   vector<float>   *jet_flavor_weight_IP3D;
   vector<float>   *jet_flavor_weight_SV0;
   vector<float>   *jet_flavor_weight_SV1;
   vector<float>   *jet_flavor_weight_SV2;
   vector<float>   *jet_flavor_weight_SoftMuonTagChi2;
   vector<float>   *jet_flavor_weight_SecondSoftMuonTagChi2;
   vector<float>   *jet_flavor_weight_JetFitterTagNN;
   vector<float>   *jet_flavor_weight_JetFitterCOMBNN;
   vector<float>   *jet_flavor_weight_MV1;
   vector<float>   *jet_flavor_weight_MV2;
   vector<float>   *jet_flavor_weight_GbbNN;
   vector<float>   *jet_flavor_weight_JetFitterCharm;
   vector<float>   *jet_flavor_weight_MV3_bVSu;
   vector<float>   *jet_flavor_weight_MV3_bVSc;
   vector<float>   *jet_flavor_weight_MV3_cVSu;
   vector<int>     *jet_flavor_truth_label;
   vector<float>   *jet_flavor_truth_dRminToB;
   vector<float>   *jet_flavor_truth_dRminToC;
   vector<float>   *jet_flavor_truth_dRminToT;
   vector<int>     *jet_flavor_truth_BHadronpdg;
   Int_t           jet_AntiKt4TopoEM_n;
   vector<float>   *jet_AntiKt4TopoEM_E;
   vector<float>   *jet_AntiKt4TopoEM_pt;
   vector<float>   *jet_AntiKt4TopoEM_m;
   vector<float>   *jet_AntiKt4TopoEM_eta;
   vector<float>   *jet_AntiKt4TopoEM_phi;
   vector<float>   *jet_AntiKt4TopoEM_EtaOrigin;
   vector<float>   *jet_AntiKt4TopoEM_PhiOrigin;
   vector<float>   *jet_AntiKt4TopoEM_MOrigin;
   vector<float>   *jet_AntiKt4TopoEM_Timing;
   vector<float>   *jet_AntiKt4TopoEM_LArQuality;
   vector<float>   *jet_AntiKt4TopoEM_sumPtTrk;
   vector<float>   *jet_AntiKt4TopoEM_HECQuality;
   vector<float>   *jet_AntiKt4TopoEM_NegativeE;
   vector<float>   *jet_AntiKt4TopoEM_AverageLArQF;
   vector<float>   *jet_AntiKt4TopoEM_BCH_CORR_CELL;
   vector<float>   *jet_AntiKt4TopoEM_BCH_CORR_DOTX;
   vector<float>   *jet_AntiKt4TopoEM_BCH_CORR_JET;
   vector<int>     *jet_AntiKt4TopoEM_SamplingMax;
   vector<float>   *jet_AntiKt4TopoEM_fracSamplingMax;
   vector<float>   *jet_AntiKt4TopoEM_hecf;
   vector<int>     *jet_AntiKt4TopoEM_isUgly;
   vector<int>     *jet_AntiKt4TopoEM_isBadLooseMinus;
   vector<int>     *jet_AntiKt4TopoEM_isBadLoose;
   vector<int>     *jet_AntiKt4TopoEM_isBadMedium;
   vector<int>     *jet_AntiKt4TopoEM_isBadTight;
   vector<float>   *jet_AntiKt4TopoEM_emfrac;
   vector<float>   *jet_AntiKt4TopoEM_Offset;
   vector<float>   *jet_AntiKt4TopoEM_EMJES;
   vector<float>   *jet_AntiKt4TopoEM_EMJES_EtaCorr;
   vector<float>   *jet_AntiKt4TopoEM_EMJESnooffset;
   vector<float>   *jet_AntiKt4TopoEM_emscale_E;
   vector<float>   *jet_AntiKt4TopoEM_emscale_pt;
   vector<float>   *jet_AntiKt4TopoEM_emscale_m;
   vector<float>   *jet_AntiKt4TopoEM_emscale_eta;
   vector<float>   *jet_AntiKt4TopoEM_emscale_phi;
   vector<float>   *jet_AntiKt4TopoEM_ActiveArea;
   vector<float>   *jet_AntiKt4TopoEM_ActiveAreaPx;
   vector<float>   *jet_AntiKt4TopoEM_ActiveAreaPy;
   vector<float>   *jet_AntiKt4TopoEM_ActiveAreaPz;
   vector<float>   *jet_AntiKt4TopoEM_ActiveAreaE;
   vector<float>   *jet_AntiKt4TopoEM_jvtxf;
   vector<vector<float> > *jet_AntiKt4TopoEM_jvtxfFull;
   vector<float>   *jet_AntiKt4TopoEM_jvtx_x;
   vector<float>   *jet_AntiKt4TopoEM_jvtx_y;
   vector<float>   *jet_AntiKt4TopoEM_jvtx_z;
   vector<float>   *jet_AntiKt4TopoEM_flavor_weight_Comb;
   vector<float>   *jet_AntiKt4TopoEM_flavor_weight_IP2D;
   vector<float>   *jet_AntiKt4TopoEM_flavor_weight_IP3D;
   vector<float>   *jet_AntiKt4TopoEM_flavor_weight_SV0;
   vector<float>   *jet_AntiKt4TopoEM_flavor_weight_SV1;
   vector<float>   *jet_AntiKt4TopoEM_flavor_weight_SV2;
   vector<float>   *jet_AntiKt4TopoEM_flavor_weight_SoftMuonTagChi2;
   vector<float>   *jet_AntiKt4TopoEM_flavor_weight_SecondSoftMuonTagChi2;
   vector<float>   *jet_AntiKt4TopoEM_flavor_weight_JetFitterTagNN;
   vector<float>   *jet_AntiKt4TopoEM_flavor_weight_JetFitterCOMBNN;
   vector<float>   *jet_AntiKt4TopoEM_flavor_weight_MV1;
   vector<float>   *jet_AntiKt4TopoEM_flavor_weight_MV2;
   vector<float>   *jet_AntiKt4TopoEM_flavor_weight_GbbNN;
   vector<float>   *jet_AntiKt4TopoEM_flavor_weight_JetFitterCharm;
   vector<float>   *jet_AntiKt4TopoEM_flavor_weight_MV3_bVSu;
   vector<float>   *jet_AntiKt4TopoEM_flavor_weight_MV3_bVSc;
   vector<float>   *jet_AntiKt4TopoEM_flavor_weight_MV3_cVSu;
   vector<int>     *jet_AntiKt4TopoEM_flavor_truth_label;
   vector<float>   *jet_AntiKt4TopoEM_flavor_truth_dRminToB;
   vector<float>   *jet_AntiKt4TopoEM_flavor_truth_dRminToC;
   vector<float>   *jet_AntiKt4TopoEM_flavor_truth_dRminToT;
   vector<int>     *jet_AntiKt4TopoEM_flavor_truth_BHadronpdg;
   Float_t         Eventshape_rhoKt4EM;
   Float_t         Eventshape_rhoKt4LC;
   Float_t         MET_RefFinal_etx;
   Float_t         MET_RefFinal_ety;
   Float_t         MET_RefFinal_phi;
   Float_t         MET_RefFinal_et;
   Float_t         MET_RefFinal_sumet;
   Float_t         MET_RefEle_etx;
   Float_t         MET_RefEle_ety;
   Float_t         MET_RefEle_phi;
   Float_t         MET_RefEle_et;
   Float_t         MET_RefEle_sumet;
   Float_t         MET_RefJet_etx;
   Float_t         MET_RefJet_ety;
   Float_t         MET_RefJet_phi;
   Float_t         MET_RefJet_et;
   Float_t         MET_RefJet_sumet;
   Float_t         MET_RefMuon_Staco_etx;
   Float_t         MET_RefMuon_Staco_ety;
   Float_t         MET_RefMuon_Staco_phi;
   Float_t         MET_RefMuon_Staco_et;
   Float_t         MET_RefMuon_Staco_sumet;
   Float_t         MET_RefGamma_etx;
   Float_t         MET_RefGamma_ety;
   Float_t         MET_RefGamma_phi;
   Float_t         MET_RefGamma_et;
   Float_t         MET_RefGamma_sumet;
   Float_t         MET_RefTau_etx;
   Float_t         MET_RefTau_ety;
   Float_t         MET_RefTau_phi;
   Float_t         MET_RefTau_et;
   Float_t         MET_RefTau_sumet;
   Float_t         MET_Track_etx;
   Float_t         MET_Track_ety;
   Float_t         MET_Track_phi;
   Float_t         MET_Track_et;
   Float_t         MET_Track_sumet;
   Float_t         MET_MuonBoy_etx;
   Float_t         MET_MuonBoy_ety;
   Float_t         MET_MuonBoy_phi;
   Float_t         MET_MuonBoy_et;
   Float_t         MET_MuonBoy_sumet;
   Float_t         MET_RefJet_JVF_etx;
   Float_t         MET_RefJet_JVF_ety;
   Float_t         MET_RefJet_JVF_phi;
   Float_t         MET_RefJet_JVF_et;
   Float_t         MET_RefJet_JVF_sumet;
   Float_t         MET_RefJet_JVFCut_etx;
   Float_t         MET_RefJet_JVFCut_ety;
   Float_t         MET_RefJet_JVFCut_phi;
   Float_t         MET_RefJet_JVFCut_et;
   Float_t         MET_RefJet_JVFCut_sumet;
   Float_t         MET_CellOut_Eflow_STVF_etx;
   Float_t         MET_CellOut_Eflow_STVF_ety;
   Float_t         MET_CellOut_Eflow_STVF_phi;
   Float_t         MET_CellOut_Eflow_STVF_et;
   Float_t         MET_CellOut_Eflow_STVF_sumet;
   Float_t         MET_CellOut_Eflow_JetArea_etx;
   Float_t         MET_CellOut_Eflow_JetArea_ety;
   Float_t         MET_CellOut_Eflow_JetArea_phi;
   Float_t         MET_CellOut_Eflow_JetArea_et;
   Float_t         MET_CellOut_Eflow_JetArea_sumet;
   Float_t         MET_CellOut_Eflow_JetAreaJVF_etx;
   Float_t         MET_CellOut_Eflow_JetAreaJVF_ety;
   Float_t         MET_CellOut_Eflow_JetAreaJVF_phi;
   Float_t         MET_CellOut_Eflow_JetAreaJVF_et;
   Float_t         MET_CellOut_Eflow_JetAreaJVF_sumet;
   Float_t         MET_CellOut_Eflow_JetAreaRhoEta5JVF_etx;
   Float_t         MET_CellOut_Eflow_JetAreaRhoEta5JVF_ety;
   Float_t         MET_CellOut_Eflow_JetAreaRhoEta5JVF_phi;
   Float_t         MET_CellOut_Eflow_JetAreaRhoEta5JVF_et;
   Float_t         MET_CellOut_Eflow_JetAreaRhoEta5JVF_sumet;
   Float_t         MET_RefFinal_STVF_etx;
   Float_t         MET_RefFinal_STVF_ety;
   Float_t         MET_RefFinal_STVF_phi;
   Float_t         MET_RefFinal_STVF_et;
   Float_t         MET_RefFinal_STVF_sumet;
   Float_t         MET_RefMuon_Track_etx;
   Float_t         MET_RefMuon_Track_ety;
   Float_t         MET_RefMuon_Track_phi;
   Float_t         MET_RefMuon_Track_et;
   Float_t         MET_RefMuon_Track_sumet;
   Float_t         MET_RefMuon_Track_Staco_etx;
   Float_t         MET_RefMuon_Track_Staco_ety;
   Float_t         MET_RefMuon_Track_Staco_phi;
   Float_t         MET_RefMuon_Track_Staco_et;
   Float_t         MET_RefMuon_Track_Staco_sumet;
   Float_t         MET_LocHadTopo_etx;
   Float_t         MET_LocHadTopo_ety;
   Float_t         MET_LocHadTopo_phi;
   Float_t         MET_LocHadTopo_et;
   Float_t         MET_LocHadTopo_sumet;
   Float_t         MET_LocHadTopo_etx_CentralReg;
   Float_t         MET_LocHadTopo_ety_CentralReg;
   Float_t         MET_LocHadTopo_sumet_CentralReg;
   Float_t         MET_LocHadTopo_phi_CentralReg;
   Float_t         MET_LocHadTopo_etx_EndcapRegion;
   Float_t         MET_LocHadTopo_ety_EndcapRegion;
   Float_t         MET_LocHadTopo_sumet_EndcapRegion;
   Float_t         MET_LocHadTopo_phi_EndcapRegion;
   Float_t         MET_LocHadTopo_etx_ForwardReg;
   Float_t         MET_LocHadTopo_ety_ForwardReg;
   Float_t         MET_LocHadTopo_sumet_ForwardReg;
   Float_t         MET_LocHadTopo_phi_ForwardReg;
   Float_t         MET_Truth_NonInt_etx;
   Float_t         MET_Truth_NonInt_ety;
   Float_t         MET_Truth_NonInt_sumet;
   Float_t         MET_Truth_PileUp_NonInt_etx;
   Float_t         MET_Truth_PileUp_NonInt_ety;
   Float_t         MET_Truth_PileUp_NonInt_sumet;
   Int_t           el_MET_n;
   vector<vector<float> > *el_MET_wpx;
   vector<vector<float> > *el_MET_wpy;
   vector<vector<float> > *el_MET_wet;
   vector<vector<unsigned int> > *el_MET_statusWord;
   Int_t           ph_MET_n;
   vector<vector<float> > *ph_MET_wpx;
   vector<vector<float> > *ph_MET_wpy;
   vector<vector<float> > *ph_MET_wet;
   vector<vector<unsigned int> > *ph_MET_statusWord;
   Int_t           mu_staco_MET_n;
   vector<vector<float> > *mu_staco_MET_wpx;
   vector<vector<float> > *mu_staco_MET_wpy;
   vector<vector<float> > *mu_staco_MET_wet;
   vector<vector<unsigned int> > *mu_staco_MET_statusWord;
   Int_t           tau_MET_n;
   vector<vector<float> > *tau_MET_wpx;
   vector<vector<float> > *tau_MET_wpy;
   vector<vector<float> > *tau_MET_wet;
   vector<vector<unsigned int> > *tau_MET_statusWord;
   Int_t           jet_AntiKt4LCTopo_MET_n;
   vector<vector<float> > *jet_AntiKt4LCTopo_MET_wpx;
   vector<vector<float> > *jet_AntiKt4LCTopo_MET_wpy;
   vector<vector<float> > *jet_AntiKt4LCTopo_MET_wet;
   vector<vector<unsigned int> > *jet_AntiKt4LCTopo_MET_statusWord;
   Int_t           tau_MET_BDTMedium_n;
   vector<vector<float> > *tau_MET_BDTMedium_wpx;
   vector<vector<float> > *tau_MET_BDTMedium_wpy;
   vector<vector<float> > *tau_MET_BDTMedium_wet;
   vector<vector<unsigned int> > *tau_MET_BDTMedium_statusWord;
   Int_t           jet_AntiKt4LCTopo_MET_BDTMedium_n;
   vector<vector<float> > *jet_AntiKt4LCTopo_MET_BDTMedium_wpx;
   vector<vector<float> > *jet_AntiKt4LCTopo_MET_BDTMedium_wpy;
   vector<vector<float> > *jet_AntiKt4LCTopo_MET_BDTMedium_wet;
   vector<vector<unsigned int> > *jet_AntiKt4LCTopo_MET_BDTMedium_statusWord;
   Float_t         MET_RefFinal_BDTMedium_etx;
   Float_t         MET_RefFinal_BDTMedium_ety;
   Float_t         MET_RefFinal_BDTMedium_phi;
   Float_t         MET_RefFinal_BDTMedium_et;
   Float_t         MET_RefFinal_BDTMedium_sumet;
   Float_t         MET_RefTau_BDTMedium_etx;
   Float_t         MET_RefTau_BDTMedium_ety;
   Float_t         MET_RefTau_BDTMedium_phi;
   Float_t         MET_RefTau_BDTMedium_et;
   Float_t         MET_RefTau_BDTMedium_sumet;
   Float_t         MET_RefJet_BDTMedium_etx;
   Float_t         MET_RefJet_BDTMedium_ety;
   Float_t         MET_RefJet_BDTMedium_phi;
   Float_t         MET_RefJet_BDTMedium_et;
   Float_t         MET_RefJet_BDTMedium_sumet;
   vector<float>   *mu_staco_E;
   vector<float>   *mu_staco_pt;
   vector<float>   *mu_staco_eta;
   vector<float>   *mu_staco_phi;
   vector<float>   *mu_staco_px;
   vector<float>   *mu_staco_py;
   vector<float>   *mu_staco_pz;
   vector<float>   *mu_staco_charge;
   vector<int>     *mu_staco_author;
   vector<float>   *mu_staco_matchchi2;
   vector<float>   *mu_staco_etcone20;
   vector<float>   *mu_staco_etcone30;
   vector<float>   *mu_staco_etcone40;
   vector<float>   *mu_staco_ptcone20;
   vector<float>   *mu_staco_ptcone30;
   vector<float>   *mu_staco_ptcone40;
   vector<int>     *mu_staco_isStandAloneMuon;
   vector<int>     *mu_staco_isCombinedMuon;
   vector<int>     *mu_staco_isLowPtReconstructedMuon;
   vector<int>     *mu_staco_isSegmentTaggedMuon;
   vector<int>     *mu_staco_isCaloMuonId;
   vector<int>     *mu_staco_alsoFoundByLowPt;
   vector<int>     *mu_staco_alsoFoundByCaloMuonId;
   vector<int>     *mu_staco_loose;
   vector<int>     *mu_staco_medium;
   vector<int>     *mu_staco_tight;
   vector<float>   *mu_staco_z0_exPV;
   vector<float>   *mu_staco_id_d0_exPV;
   vector<float>   *mu_staco_id_z0_exPV;
   vector<float>   *mu_staco_id_phi_exPV;
   vector<float>   *mu_staco_id_theta_exPV;
   vector<float>   *mu_staco_id_qoverp_exPV;
   vector<float>   *mu_staco_me_phi_exPV;
   vector<float>   *mu_staco_me_theta_exPV;
   vector<float>   *mu_staco_me_qoverp_exPV;
   vector<float>   *mu_staco_ms_phi;
   vector<float>   *mu_staco_ms_theta;
   vector<float>   *mu_staco_ms_qoverp;
   vector<float>   *mu_staco_id_phi;
   vector<float>   *mu_staco_id_theta;
   vector<float>   *mu_staco_id_qoverp;
   vector<float>   *mu_staco_me_phi;
   vector<float>   *mu_staco_me_theta;
   vector<float>   *mu_staco_me_qoverp;
   vector<int>     *mu_staco_nBLHits;
   vector<int>     *mu_staco_nPixHits;
   vector<int>     *mu_staco_nSCTHits;
   vector<int>     *mu_staco_nTRTHits;
   vector<int>     *mu_staco_nPixHoles;
   vector<int>     *mu_staco_nSCTHoles;
   vector<int>     *mu_staco_nTRTOutliers;
   vector<int>     *mu_staco_nPixelDeadSensors;
   vector<int>     *mu_staco_nSCTDeadSensors;
   vector<int>     *mu_staco_expectBLayerHit;
   vector<float>   *mu_staco_trackd0pv;
   vector<float>   *mu_staco_trackz0pv;
   vector<float>   *mu_staco_tracksigd0pv;
   vector<float>   *mu_staco_tracksigz0pv;
   vector<float>   *mu_staco_trackd0pvunbiased;
   vector<float>   *mu_staco_trackz0pvunbiased;
   vector<float>   *mu_staco_tracksigd0pvunbiased;
   vector<float>   *mu_staco_tracksigz0pvunbiased;
   Int_t           top_hfor_type;
   Int_t           vxp_n;
   vector<float>   *vxp_x;
   vector<float>   *vxp_y;
   vector<float>   *vxp_z;
   vector<int>     *vxp_type;
   vector<int>     *vxp_nTracks;
   UInt_t          EventNumber;
   UInt_t          bcid;
   Float_t         actualIntPerXing;
   Float_t         averageIntPerXing;
   UInt_t          mc_channel_number;
   UInt_t          mc_event_number;
   Float_t         mc_event_weight;
   UInt_t          larFlags;
   UInt_t          coreFlags;
   UInt_t          larError;
   UInt_t          tileError;
   Int_t           trig_L1_emtau_n;
   vector<float>   *trig_L1_emtau_eta;
   vector<float>   *trig_L1_emtau_phi;
   vector<float>   *trig_L1_emtau_EMClus;
   vector<float>   *trig_L1_emtau_hadCore;
   Int_t           trig_EF_tau_n;
   vector<float>   *trig_EF_tau_Et;
   vector<float>   *trig_EF_tau_pt;
   vector<float>   *trig_EF_tau_m;
   vector<float>   *trig_EF_tau_eta;
   vector<float>   *trig_EF_tau_phi;
   Int_t           trig_Nav_n;
   vector<short>   *trig_Nav_chain_ChainId;
   vector<vector<int> > *trig_Nav_chain_RoIType;
   vector<vector<int> > *trig_Nav_chain_RoIIndex;
   UInt_t          trig_DB_SMK;
   Int_t           trig_L1_mu_n;
   vector<float>   *trig_L1_mu_pt;
   vector<float>   *trig_L1_mu_eta;
   vector<float>   *trig_L1_mu_phi;
   vector<string>  *trig_L1_mu_thrName;
   Int_t           trig_L2_muonfeature_n;
   vector<float>   *trig_L2_muonfeature_pt;
   vector<float>   *trig_L2_muonfeature_eta;
   vector<float>   *trig_L2_muonfeature_phi;
   Int_t           trig_L2_combmuonfeature_n;
   vector<float>   *trig_L2_combmuonfeature_pt;
   vector<float>   *trig_L2_combmuonfeature_eta;
   vector<float>   *trig_L2_combmuonfeature_phi;
   Int_t           trig_EF_trigmuonef_n;
   vector<int>     *trig_EF_trigmuonef_track_n;
   vector<vector<int> > *trig_EF_trigmuonef_track_MuonType;
   vector<vector<float> > *trig_EF_trigmuonef_track_SA_pt;
   vector<vector<float> > *trig_EF_trigmuonef_track_SA_eta;
   vector<vector<float> > *trig_EF_trigmuonef_track_SA_phi;
   vector<vector<float> > *trig_EF_trigmuonef_track_CB_pt;
   vector<vector<float> > *trig_EF_trigmuonef_track_CB_eta;
   vector<vector<float> > *trig_EF_trigmuonef_track_CB_phi;
   Int_t           trig_EF_trigmugirl_n;
   vector<int>     *trig_EF_trigmugirl_track_n;
   vector<vector<int> > *trig_EF_trigmugirl_track_MuonType;
   vector<vector<float> > *trig_EF_trigmugirl_track_SA_pt;
   vector<vector<float> > *trig_EF_trigmugirl_track_SA_eta;
   vector<vector<float> > *trig_EF_trigmugirl_track_SA_phi;
   vector<vector<float> > *trig_EF_trigmugirl_track_CB_pt;
   vector<vector<float> > *trig_EF_trigmugirl_track_CB_eta;
   vector<vector<float> > *trig_EF_trigmugirl_track_CB_phi;
   Int_t           trig_RoI_L2_mu_n;
   vector<int>     *trig_RoI_L2_mu_MuonFeature;
   vector<int>     *trig_RoI_L2_mu_CombinedMuonFeature;
   vector<int>     *trig_RoI_L2_mu_CombinedMuonFeatureStatus;
   vector<int>     *trig_RoI_L2_mu_Muon_ROI;
   Int_t           trig_RoI_EF_mu_n;
   vector<int>     *trig_RoI_EF_mu_Muon_ROI;
   vector<vector<int> > *trig_RoI_EF_mu_TrigMuonEFInfoContainer;
   vector<vector<int> > *trig_RoI_EF_mu_TrigMuonEFInfoContainerStatus;
   vector<vector<int> > *trig_RoI_EF_mu_TrigMuonEFIsolationContainer;
   vector<vector<int> > *trig_RoI_EF_mu_TrigMuonEFIsolationContainerStatus;
   UInt_t          RunNumber;
   UInt_t          lbn;
   Int_t           el_n;
   Int_t           mu_staco_n;
   Int_t           mcevt0_event_number;
   Double_t        mcevt0_event_scale;
   Double_t        mcevt0_alphaQCD;
   Double_t        mcevt0_alphaQED;
   Int_t           mcevt0_pdf_id1;
   Int_t           mcevt0_pdf_id2;
   Double_t        mcevt0_pdf_x1;
   Double_t        mcevt0_pdf_x2;
   Double_t        mcevt0_pdf_scale;
   Double_t        mcevt0_pdf1;
   Double_t        mcevt0_pdf2;
   vector<double>  *mcevt0_weight;
   Int_t           truthLP_n;
   vector<float>   *truthLP_pt;
   vector<float>   *truthLP_m;
   vector<float>   *truthLP_eta;
   vector<float>   *truthLP_phi;
   vector<int>     *truthLP_pdgId;
   vector<float>   *truthLP_charge;
   vector<int>     *truthLP_status;
   vector<int>     *truthLP_pdgId_parent;
   Int_t           truthTau_n;
   vector<float>   *truthTau_pt;
   vector<float>   *truthTau_m;
   vector<float>   *truthTau_eta;
   vector<float>   *truthTau_phi;
   vector<int>     *truthTau_pdgId;
   vector<float>   *truthTau_charge;
   vector<int>     *truthTau_status;
   vector<int>     *truthTau_pdgId_parent;
   vector<bool>    *truthTau_isLeptonicDecay;
   vector<int>     *truthTau_nProng;
   vector<float>   *truthTau_vis_Et;
   vector<float>   *truthTau_vis_m;
   vector<float>   *truthTau_vis_eta;
   vector<float>   *truthTau_vis_phi;
   vector<float>   *truthTau_invis_Et;
   vector<float>   *truthTau_invis_m;
   vector<float>   *truthTau_invis_eta;
   vector<float>   *truthTau_invis_phi;
   vector<int>     *truthTau_child_n;
   vector<vector<float> > *truthTau_child_pt;
   vector<vector<float> > *truthTau_child_m;
   vector<vector<float> > *truthTau_child_eta;
   vector<vector<float> > *truthTau_child_phi;
   vector<vector<int> > *truthTau_child_pdgId;
   vector<vector<float> > *truthTau_child_charge;
   vector<vector<int> > *truthTau_child_status;
   Int_t           truthH_n;
   vector<float>   *truthH_pt;
   vector<float>   *truthH_m;
   vector<float>   *truthH_eta;
   vector<float>   *truthH_phi;
   vector<int>     *truthH_pdgId;
   vector<float>   *truthH_charge;
   vector<int>     *truthH_status;
   vector<int>     *truthH_child_n;
   vector<vector<float> > *truthH_child_pt;
   vector<vector<float> > *truthH_child_m;
   vector<vector<float> > *truthH_child_eta;
   vector<vector<float> > *truthH_child_phi;
   vector<vector<int> > *truthH_child_pdgId;
   vector<vector<float> > *truthH_child_charge;
   vector<vector<int> > *truthH_child_status;
   Int_t           truthZ_n;
   vector<float>   *truthZ_pt;
   vector<float>   *truthZ_m;
   vector<float>   *truthZ_eta;
   vector<float>   *truthZ_phi;
   vector<int>     *truthZ_pdgId;
   vector<int>     *truthZ_status;
   vector<int>     *truthZ_pdgId_parent;
   vector<int>     *truthZ_child_n;
   vector<vector<float> > *truthZ_child_pt;
   vector<vector<float> > *truthZ_child_m;
   vector<vector<float> > *truthZ_child_eta;
   vector<vector<float> > *truthZ_child_phi;
   vector<vector<int> > *truthZ_child_pdgId;
   vector<vector<float> > *truthZ_child_charge;
   vector<vector<int> > *truthZ_child_status;
   Int_t           truthW_n;
   vector<float>   *truthW_pt;
   vector<float>   *truthW_m;
   vector<float>   *truthW_eta;
   vector<float>   *truthW_phi;
   vector<int>     *truthW_pdgId;
   vector<float>   *truthW_charge;
   vector<int>     *truthW_status;
   vector<int>     *truthW_pdgId_parent;
   vector<int>     *truthW_child_n;
   vector<vector<float> > *truthW_child_pt;
   vector<vector<float> > *truthW_child_m;
   vector<vector<float> > *truthW_child_eta;
   vector<vector<float> > *truthW_child_phi;
   vector<vector<int> > *truthW_child_pdgId;
   vector<vector<float> > *truthW_child_charge;
   vector<vector<int> > *truthW_child_status;
   Int_t           truthSpecial_n;
   vector<float>   *truthSpecial_pt;
   vector<float>   *truthSpecial_m;
   vector<float>   *truthSpecial_eta;
   vector<float>   *truthSpecial_phi;
   vector<int>     *truthSpecial_pdgId;

   // List of branches
   TBranch        *b_EF_2e12Tvh_loose1;   //!
   TBranch        *b_EF_2e7T_loose1_mu6;   //!
   TBranch        *b_EF_2e7T_medium1_mu6;   //!
   TBranch        *b_EF_2mu13;   //!
   TBranch        *b_EF_2mu15;   //!
   TBranch        *b_EF_e12Tvh_medium1_mu8;   //!
   TBranch        *b_EF_e22vh_medium1;   //!
   TBranch        *b_EF_e22vhi_medium1;   //!
   TBranch        *b_EF_e24vh_medium1;   //!
   TBranch        *b_EF_e24vhi_medium1;   //!
   TBranch        *b_EF_e60_medium1;   //!
   TBranch        *b_EF_e7T_loose1_2mu6;   //!
   TBranch        *b_EF_e7T_medium1_2mu6;   //!
   TBranch        *b_EF_mu18_medium;   //!
   TBranch        *b_EF_mu18_tight;   //!
   TBranch        *b_EF_mu18_tight_2mu4_EFFS;   //!
   TBranch        *b_EF_mu18_tight_e7_medium1;   //!
   TBranch        *b_EF_mu18_tight_mu8_EFFS;   //!
   TBranch        *b_EF_mu24_medium;   //!
   TBranch        *b_EF_mu24_tight;   //!
   TBranch        *b_EF_mu24_tight_mu6_EFFS;   //!
   TBranch        *b_EF_mu24i_tight;   //!
   TBranch        *b_EF_mu36_tight;   //!
   TBranch        *b_EF_mu40_MSonly_barrel_tight;   //!
   TBranch        *b_EF_tau20T_medium1_e15vh_medium1;   //!
   TBranch        *b_EF_tau20T_medium_mu15;   //!
   TBranch        *b_EF_tau20_medium1_mu15;   //!
   TBranch        *b_trig_EF_tau_EF_tau20T_medium1_e15vh_medium1;   //!
   TBranch        *b_trig_EF_tau_EF_tau20T_medium_mu15;   //!
   TBranch        *b_trig_EF_tau_EF_tau20_medium1_mu15;   //!
   TBranch        *b_trig_L2_combmuonfeature_L2_2mu13;   //!
   TBranch        *b_trig_L2_combmuonfeature_L2_2mu15;   //!
   TBranch        *b_trig_L2_combmuonfeature_L2_2mu6;   //!
   TBranch        *b_trig_L2_combmuonfeature_L2_mu15;   //!
   TBranch        *b_trig_L2_combmuonfeature_L2_mu18_medium;   //!
   TBranch        *b_trig_L2_combmuonfeature_L2_mu18_tight;   //!
   TBranch        *b_trig_L2_combmuonfeature_L2_mu18_tight_e7_medium1;   //!
   TBranch        *b_trig_L2_combmuonfeature_L2_mu24_medium;   //!
   TBranch        *b_trig_L2_combmuonfeature_L2_mu24_tight;   //!
   TBranch        *b_trig_L2_combmuonfeature_L2_mu36_tight;   //!
   TBranch        *b_trig_L2_combmuonfeature_L2_mu6;   //!
   TBranch        *b_trig_L2_combmuonfeature_L2_mu8;   //!
   TBranch        *b_trig_EF_trigmuonef_EF_2mu13;   //!
   TBranch        *b_trig_EF_trigmuonef_EF_2mu15;   //!
   TBranch        *b_trig_EF_trigmuonef_EF_2mu6;   //!
   TBranch        *b_trig_EF_trigmuonef_EF_mu15;   //!
   TBranch        *b_trig_EF_trigmuonef_EF_mu18_medium;   //!
   TBranch        *b_trig_EF_trigmuonef_EF_mu18_tight;   //!
   TBranch        *b_trig_EF_trigmuonef_EF_mu18_tight_2mu4_EFFS;   //!
   TBranch        *b_trig_EF_trigmuonef_EF_mu18_tight_e7_medium1;   //!
   TBranch        *b_trig_EF_trigmuonef_EF_mu18_tight_mu8_EFFS;   //!
   TBranch        *b_trig_EF_trigmuonef_EF_mu24_medium;   //!
   TBranch        *b_trig_EF_trigmuonef_EF_mu24_tight;   //!
   TBranch        *b_trig_EF_trigmuonef_EF_mu24_tight_mu6_EFFS;   //!
   TBranch        *b_trig_EF_trigmuonef_EF_mu24i_tight;   //!
   TBranch        *b_trig_EF_trigmuonef_EF_mu36_tight;   //!
   TBranch        *b_trig_EF_trigmuonef_EF_mu40_MSonly_barrel_tight;   //!
   TBranch        *b_trig_EF_trigmuonef_EF_mu6;   //!
   TBranch        *b_trig_EF_trigmuonef_EF_mu8;   //!
   TBranch        *b_trig_EF_trigmugirl_EF_2mu13;   //!
   TBranch        *b_trig_EF_trigmugirl_EF_2mu15;   //!
   TBranch        *b_trig_EF_trigmugirl_EF_2mu6;   //!
   TBranch        *b_trig_EF_trigmugirl_EF_mu15;   //!
   TBranch        *b_trig_EF_trigmugirl_EF_mu18_medium;   //!
   TBranch        *b_trig_EF_trigmugirl_EF_mu18_tight;   //!
   TBranch        *b_trig_EF_trigmugirl_EF_mu18_tight_2mu4_EFFS;   //!
   TBranch        *b_trig_EF_trigmugirl_EF_mu18_tight_e7_medium1;   //!
   TBranch        *b_trig_EF_trigmugirl_EF_mu18_tight_mu8_EFFS;   //!
   TBranch        *b_trig_EF_trigmugirl_EF_mu24_medium;   //!
   TBranch        *b_trig_EF_trigmugirl_EF_mu24_tight;   //!
   TBranch        *b_trig_EF_trigmugirl_EF_mu24_tight_mu6_EFFS;   //!
   TBranch        *b_trig_EF_trigmugirl_EF_mu24i_tight;   //!
   TBranch        *b_trig_EF_trigmugirl_EF_mu36_tight;   //!
   TBranch        *b_trig_EF_trigmugirl_EF_mu40_MSonly_barrel_tight;   //!
   TBranch        *b_trig_EF_trigmugirl_EF_mu6;   //!
   TBranch        *b_trig_EF_trigmugirl_EF_mu8;   //!
   TBranch        *b_tau_n;   //!
   TBranch        *b_tau_Et;   //!
   TBranch        *b_tau_pt;   //!
   TBranch        *b_tau_m;   //!
   TBranch        *b_tau_eta;   //!
   TBranch        *b_tau_phi;   //!
   TBranch        *b_tau_charge;   //!
   TBranch        *b_tau_BDTEleScore;   //!
   TBranch        *b_tau_BDTJetScore;   //!
   TBranch        *b_tau_likelihood;   //!
   TBranch        *b_tau_SafeLikelihood;   //!
   TBranch        *b_tau_electronVetoLoose;   //!
   TBranch        *b_tau_electronVetoMedium;   //!
   TBranch        *b_tau_electronVetoTight;   //!
   TBranch        *b_tau_muonVeto;   //!
   TBranch        *b_tau_tauLlhLoose;   //!
   TBranch        *b_tau_tauLlhMedium;   //!
   TBranch        *b_tau_tauLlhTight;   //!
   TBranch        *b_tau_JetBDTSigLoose;   //!
   TBranch        *b_tau_JetBDTSigMedium;   //!
   TBranch        *b_tau_JetBDTSigTight;   //!
   TBranch        *b_tau_EleBDTLoose;   //!
   TBranch        *b_tau_EleBDTMedium;   //!
   TBranch        *b_tau_EleBDTTight;   //!
   TBranch        *b_tau_author;   //!
   TBranch        *b_tau_numTrack;   //!
   TBranch        *b_tau_etOverPtLeadTrk;   //!
   TBranch        *b_tau_leadTrkPt;   //!
   TBranch        *b_tau_jet_jvtxf;   //!
   TBranch        *b_tau_track_n;   //!
   TBranch        *b_tau_track_d0;   //!
   TBranch        *b_tau_track_z0;   //!
   TBranch        *b_tau_track_phi;   //!
   TBranch        *b_tau_track_theta;   //!
   TBranch        *b_tau_track_qoverp;   //!
   TBranch        *b_tau_track_pt;   //!
   TBranch        *b_tau_track_eta;   //!
   TBranch        *b_tau_track_atPV_d0;   //!
   TBranch        *b_tau_track_atPV_z0;   //!
   TBranch        *b_tau_track_atPV_phi;   //!
   TBranch        *b_tau_track_atPV_theta;   //!
   TBranch        *b_tau_track_atPV_qoverp;   //!
   TBranch        *b_tau_track_atPV_pt;   //!
   TBranch        *b_tau_track_atPV_eta;   //!
   TBranch        *b_mc_n;   //!
   TBranch        *b_mc_pt;   //!
   TBranch        *b_mc_m;   //!
   TBranch        *b_mc_eta;   //!
   TBranch        *b_mc_phi;   //!
   TBranch        *b_mc_status;   //!
   TBranch        *b_mc_barcode;   //!
   TBranch        *b_mc_pdgId;   //!
   TBranch        *b_mc_charge;   //!
   TBranch        *b_mc_child_index;   //!
   TBranch        *b_mc_parent_index;   //!
   TBranch        *b_jet_antikt4truth_n;   //!
   TBranch        *b_jet_antikt4truth_E;   //!
   TBranch        *b_jet_antikt4truth_pt;   //!
   TBranch        *b_jet_antikt4truth_m;   //!
   TBranch        *b_jet_antikt4truth_eta;   //!
   TBranch        *b_jet_antikt4truth_phi;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_author;   //!
   TBranch        *b_el_isEM;   //!
   TBranch        *b_el_OQ;   //!
   TBranch        *b_el_loose;   //!
   TBranch        *b_el_medium;   //!
   TBranch        *b_el_tight;   //!
   TBranch        *b_el_loosePP;   //!
   TBranch        *b_el_mediumPP;   //!
   TBranch        *b_el_tightPP;   //!
   TBranch        *b_el_Ethad;   //!
   TBranch        *b_el_Ethad1;   //!
   TBranch        *b_el_f1;   //!
   TBranch        *b_el_Emax2;   //!
   TBranch        *b_el_wstot;   //!
   TBranch        *b_el_emaxs1;   //!
   TBranch        *b_el_E237;   //!
   TBranch        *b_el_E277;   //!
   TBranch        *b_el_weta2;   //!
   TBranch        *b_el_f3;   //!
   TBranch        *b_el_Etcone20;   //!
   TBranch        *b_el_Etcone30;   //!
   TBranch        *b_el_Etcone40;   //!
   TBranch        *b_el_ptcone20;   //!
   TBranch        *b_el_ptcone30;   //!
   TBranch        *b_el_ptcone40;   //!
   TBranch        *b_el_deltaeta1;   //!
   TBranch        *b_el_deltaphi2;   //!
   TBranch        *b_el_expectHitInBLayer;   //!
   TBranch        *b_el_trackd0_physics;   //!
   TBranch        *b_el_reta;   //!
   TBranch        *b_el_topoEtcone20;   //!
   TBranch        *b_el_topoEtcone30;   //!
   TBranch        *b_el_topoEtcone40;   //!
   TBranch        *b_el_etap;   //!
   TBranch        *b_el_Es2;   //!
   TBranch        *b_el_etas2;   //!
   TBranch        *b_el_phis2;   //!
   TBranch        *b_el_cl_E;   //!
   TBranch        *b_el_cl_pt;   //!
   TBranch        *b_el_cl_eta;   //!
   TBranch        *b_el_cl_phi;   //!
   TBranch        *b_el_trackphi;   //!
   TBranch        *b_el_trackqoverp;   //!
   TBranch        *b_el_trackpt;   //!
   TBranch        *b_el_tracketa;   //!
   TBranch        *b_el_nBLHits;   //!
   TBranch        *b_el_nPixHits;   //!
   TBranch        *b_el_nSCTHits;   //!
   TBranch        *b_el_nTRTHits;   //!
   TBranch        *b_el_nBLayerOutliers;   //!
   TBranch        *b_el_nPixelOutliers;   //!
   TBranch        *b_el_nSCTOutliers;   //!
   TBranch        *b_el_nTRTOutliers;   //!
   TBranch        *b_el_nSiHits;   //!
   TBranch        *b_el_TRTHighTOutliersRatio;   //!
   TBranch        *b_el_trackd0pv;   //!
   TBranch        *b_el_trackz0pv;   //!
   TBranch        *b_el_tracksigd0pv;   //!
   TBranch        *b_el_tracksigz0pv;   //!
   TBranch        *b_el_trackd0pvunbiased;   //!
   TBranch        *b_el_trackz0pvunbiased;   //!
   TBranch        *b_el_tracksigd0pvunbiased;   //!
   TBranch        *b_el_tracksigz0pvunbiased;   //!
   TBranch        *b_el_topoEtcone20_corrected;   //!
   TBranch        *b_el_topoEtcone30_corrected;   //!
   TBranch        *b_el_topoEtcone40_corrected;   //!
   TBranch        *b_el_ED_median;   //!
   TBranch        *b_jet_n;   //!
   TBranch        *b_jet_E;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_m;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_EtaOrigin;   //!
   TBranch        *b_jet_PhiOrigin;   //!
   TBranch        *b_jet_MOrigin;   //!
   TBranch        *b_jet_Timing;   //!
   TBranch        *b_jet_LArQuality;   //!
   TBranch        *b_jet_sumPtTrk;   //!
   TBranch        *b_jet_HECQuality;   //!
   TBranch        *b_jet_NegativeE;   //!
   TBranch        *b_jet_AverageLArQF;   //!
   TBranch        *b_jet_BCH_CORR_CELL;   //!
   TBranch        *b_jet_BCH_CORR_DOTX;   //!
   TBranch        *b_jet_BCH_CORR_JET;   //!
   TBranch        *b_jet_SamplingMax;   //!
   TBranch        *b_jet_fracSamplingMax;   //!
   TBranch        *b_jet_hecf;   //!
   TBranch        *b_jet_isUgly;   //!
   TBranch        *b_jet_isBadLooseMinus;   //!
   TBranch        *b_jet_isBadLoose;   //!
   TBranch        *b_jet_isBadMedium;   //!
   TBranch        *b_jet_isBadTight;   //!
   TBranch        *b_jet_emfrac;   //!
   TBranch        *b_jet_Offset;   //!
   TBranch        *b_jet_EMJES;   //!
   TBranch        *b_jet_EMJES_EtaCorr;   //!
   TBranch        *b_jet_EMJESnooffset;   //!
   TBranch        *b_jet_emscale_E;   //!
   TBranch        *b_jet_emscale_pt;   //!
   TBranch        *b_jet_emscale_m;   //!
   TBranch        *b_jet_emscale_eta;   //!
   TBranch        *b_jet_emscale_phi;   //!
   TBranch        *b_jet_ActiveArea;   //!
   TBranch        *b_jet_ActiveAreaPx;   //!
   TBranch        *b_jet_ActiveAreaPy;   //!
   TBranch        *b_jet_ActiveAreaPz;   //!
   TBranch        *b_jet_ActiveAreaE;   //!
   TBranch        *b_jet_jvtxf;   //!
   TBranch        *b_jet_jvtxfFull;   //!
   TBranch        *b_jet_jvtx_x;   //!
   TBranch        *b_jet_jvtx_y;   //!
   TBranch        *b_jet_jvtx_z;   //!
   TBranch        *b_jet_constscale_E;   //!
   TBranch        *b_jet_constscale_pt;   //!
   TBranch        *b_jet_constscale_m;   //!
   TBranch        *b_jet_constscale_eta;   //!
   TBranch        *b_jet_constscale_phi;   //!
   TBranch        *b_jet_flavor_weight_Comb;   //!
   TBranch        *b_jet_flavor_weight_IP2D;   //!
   TBranch        *b_jet_flavor_weight_IP3D;   //!
   TBranch        *b_jet_flavor_weight_SV0;   //!
   TBranch        *b_jet_flavor_weight_SV1;   //!
   TBranch        *b_jet_flavor_weight_SV2;   //!
   TBranch        *b_jet_flavor_weight_SoftMuonTagChi2;   //!
   TBranch        *b_jet_flavor_weight_SecondSoftMuonTagChi2;   //!
   TBranch        *b_jet_flavor_weight_JetFitterTagNN;   //!
   TBranch        *b_jet_flavor_weight_JetFitterCOMBNN;   //!
   TBranch        *b_jet_flavor_weight_MV1;   //!
   TBranch        *b_jet_flavor_weight_MV2;   //!
   TBranch        *b_jet_flavor_weight_GbbNN;   //!
   TBranch        *b_jet_flavor_weight_JetFitterCharm;   //!
   TBranch        *b_jet_flavor_weight_MV3_bVSu;   //!
   TBranch        *b_jet_flavor_weight_MV3_bVSc;   //!
   TBranch        *b_jet_flavor_weight_MV3_cVSu;   //!
   TBranch        *b_jet_flavor_truth_label;   //!
   TBranch        *b_jet_flavor_truth_dRminToB;   //!
   TBranch        *b_jet_flavor_truth_dRminToC;   //!
   TBranch        *b_jet_flavor_truth_dRminToT;   //!
   TBranch        *b_jet_flavor_truth_BHadronpdg;   //!
   TBranch        *b_jet_AntiKt4TopoEM_n;   //!
   TBranch        *b_jet_AntiKt4TopoEM_E;   //!
   TBranch        *b_jet_AntiKt4TopoEM_pt;   //!
   TBranch        *b_jet_AntiKt4TopoEM_m;   //!
   TBranch        *b_jet_AntiKt4TopoEM_eta;   //!
   TBranch        *b_jet_AntiKt4TopoEM_phi;   //!
   TBranch        *b_jet_AntiKt4TopoEM_EtaOrigin;   //!
   TBranch        *b_jet_AntiKt4TopoEM_PhiOrigin;   //!
   TBranch        *b_jet_AntiKt4TopoEM_MOrigin;   //!
   TBranch        *b_jet_AntiKt4TopoEM_Timing;   //!
   TBranch        *b_jet_AntiKt4TopoEM_LArQuality;   //!
   TBranch        *b_jet_AntiKt4TopoEM_sumPtTrk;   //!
   TBranch        *b_jet_AntiKt4TopoEM_HECQuality;   //!
   TBranch        *b_jet_AntiKt4TopoEM_NegativeE;   //!
   TBranch        *b_jet_AntiKt4TopoEM_AverageLArQF;   //!
   TBranch        *b_jet_AntiKt4TopoEM_BCH_CORR_CELL;   //!
   TBranch        *b_jet_AntiKt4TopoEM_BCH_CORR_DOTX;   //!
   TBranch        *b_jet_AntiKt4TopoEM_BCH_CORR_JET;   //!
   TBranch        *b_jet_AntiKt4TopoEM_SamplingMax;   //!
   TBranch        *b_jet_AntiKt4TopoEM_fracSamplingMax;   //!
   TBranch        *b_jet_AntiKt4TopoEM_hecf;   //!
   TBranch        *b_jet_AntiKt4TopoEM_isUgly;   //!
   TBranch        *b_jet_AntiKt4TopoEM_isBadLooseMinus;   //!
   TBranch        *b_jet_AntiKt4TopoEM_isBadLoose;   //!
   TBranch        *b_jet_AntiKt4TopoEM_isBadMedium;   //!
   TBranch        *b_jet_AntiKt4TopoEM_isBadTight;   //!
   TBranch        *b_jet_AntiKt4TopoEM_emfrac;   //!
   TBranch        *b_jet_AntiKt4TopoEM_Offset;   //!
   TBranch        *b_jet_AntiKt4TopoEM_EMJES;   //!
   TBranch        *b_jet_AntiKt4TopoEM_EMJES_EtaCorr;   //!
   TBranch        *b_jet_AntiKt4TopoEM_EMJESnooffset;   //!
   TBranch        *b_jet_AntiKt4TopoEM_emscale_E;   //!
   TBranch        *b_jet_AntiKt4TopoEM_emscale_pt;   //!
   TBranch        *b_jet_AntiKt4TopoEM_emscale_m;   //!
   TBranch        *b_jet_AntiKt4TopoEM_emscale_eta;   //!
   TBranch        *b_jet_AntiKt4TopoEM_emscale_phi;   //!
   TBranch        *b_jet_AntiKt4TopoEM_ActiveArea;   //!
   TBranch        *b_jet_AntiKt4TopoEM_ActiveAreaPx;   //!
   TBranch        *b_jet_AntiKt4TopoEM_ActiveAreaPy;   //!
   TBranch        *b_jet_AntiKt4TopoEM_ActiveAreaPz;   //!
   TBranch        *b_jet_AntiKt4TopoEM_ActiveAreaE;   //!
   TBranch        *b_jet_AntiKt4TopoEM_jvtxf;   //!
   TBranch        *b_jet_AntiKt4TopoEM_jvtxfFull;   //!
   TBranch        *b_jet_AntiKt4TopoEM_jvtx_x;   //!
   TBranch        *b_jet_AntiKt4TopoEM_jvtx_y;   //!
   TBranch        *b_jet_AntiKt4TopoEM_jvtx_z;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_weight_Comb;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_weight_IP2D;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_weight_IP3D;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_weight_SV0;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_weight_SV1;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_weight_SV2;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_weight_SoftMuonTagChi2;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_weight_SecondSoftMuonTagChi2;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_weight_JetFitterTagNN;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_weight_JetFitterCOMBNN;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_weight_MV1;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_weight_MV2;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_weight_GbbNN;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_weight_JetFitterCharm;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_weight_MV3_bVSu;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_weight_MV3_bVSc;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_weight_MV3_cVSu;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_truth_label;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_truth_dRminToB;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_truth_dRminToC;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_truth_dRminToT;   //!
   TBranch        *b_jet_AntiKt4TopoEM_flavor_truth_BHadronpdg;   //!
   TBranch        *b_Eventshape_rhoKt4EM;   //!
   TBranch        *b_Eventshape_rhoKt4LC;   //!
   TBranch        *b_MET_RefFinal_etx;   //!
   TBranch        *b_MET_RefFinal_ety;   //!
   TBranch        *b_MET_RefFinal_phi;   //!
   TBranch        *b_MET_RefFinal_et;   //!
   TBranch        *b_MET_RefFinal_sumet;   //!
   TBranch        *b_MET_RefEle_etx;   //!
   TBranch        *b_MET_RefEle_ety;   //!
   TBranch        *b_MET_RefEle_phi;   //!
   TBranch        *b_MET_RefEle_et;   //!
   TBranch        *b_MET_RefEle_sumet;   //!
   TBranch        *b_MET_RefJet_etx;   //!
   TBranch        *b_MET_RefJet_ety;   //!
   TBranch        *b_MET_RefJet_phi;   //!
   TBranch        *b_MET_RefJet_et;   //!
   TBranch        *b_MET_RefJet_sumet;   //!
   TBranch        *b_MET_RefMuon_Staco_etx;   //!
   TBranch        *b_MET_RefMuon_Staco_ety;   //!
   TBranch        *b_MET_RefMuon_Staco_phi;   //!
   TBranch        *b_MET_RefMuon_Staco_et;   //!
   TBranch        *b_MET_RefMuon_Staco_sumet;   //!
   TBranch        *b_MET_RefGamma_etx;   //!
   TBranch        *b_MET_RefGamma_ety;   //!
   TBranch        *b_MET_RefGamma_phi;   //!
   TBranch        *b_MET_RefGamma_et;   //!
   TBranch        *b_MET_RefGamma_sumet;   //!
   TBranch        *b_MET_RefTau_etx;   //!
   TBranch        *b_MET_RefTau_ety;   //!
   TBranch        *b_MET_RefTau_phi;   //!
   TBranch        *b_MET_RefTau_et;   //!
   TBranch        *b_MET_RefTau_sumet;   //!
   TBranch        *b_MET_Track_etx;   //!
   TBranch        *b_MET_Track_ety;   //!
   TBranch        *b_MET_Track_phi;   //!
   TBranch        *b_MET_Track_et;   //!
   TBranch        *b_MET_Track_sumet;   //!
   TBranch        *b_MET_MuonBoy_etx;   //!
   TBranch        *b_MET_MuonBoy_ety;   //!
   TBranch        *b_MET_MuonBoy_phi;   //!
   TBranch        *b_MET_MuonBoy_et;   //!
   TBranch        *b_MET_MuonBoy_sumet;   //!
   TBranch        *b_MET_RefJet_JVF_etx;   //!
   TBranch        *b_MET_RefJet_JVF_ety;   //!
   TBranch        *b_MET_RefJet_JVF_phi;   //!
   TBranch        *b_MET_RefJet_JVF_et;   //!
   TBranch        *b_MET_RefJet_JVF_sumet;   //!
   TBranch        *b_MET_RefJet_JVFCut_etx;   //!
   TBranch        *b_MET_RefJet_JVFCut_ety;   //!
   TBranch        *b_MET_RefJet_JVFCut_phi;   //!
   TBranch        *b_MET_RefJet_JVFCut_et;   //!
   TBranch        *b_MET_RefJet_JVFCut_sumet;   //!
   TBranch        *b_MET_CellOut_Eflow_STVF_etx;   //!
   TBranch        *b_MET_CellOut_Eflow_STVF_ety;   //!
   TBranch        *b_MET_CellOut_Eflow_STVF_phi;   //!
   TBranch        *b_MET_CellOut_Eflow_STVF_et;   //!
   TBranch        *b_MET_CellOut_Eflow_STVF_sumet;   //!
   TBranch        *b_MET_CellOut_Eflow_JetArea_etx;   //!
   TBranch        *b_MET_CellOut_Eflow_JetArea_ety;   //!
   TBranch        *b_MET_CellOut_Eflow_JetArea_phi;   //!
   TBranch        *b_MET_CellOut_Eflow_JetArea_et;   //!
   TBranch        *b_MET_CellOut_Eflow_JetArea_sumet;   //!
   TBranch        *b_MET_CellOut_Eflow_JetAreaJVF_etx;   //!
   TBranch        *b_MET_CellOut_Eflow_JetAreaJVF_ety;   //!
   TBranch        *b_MET_CellOut_Eflow_JetAreaJVF_phi;   //!
   TBranch        *b_MET_CellOut_Eflow_JetAreaJVF_et;   //!
   TBranch        *b_MET_CellOut_Eflow_JetAreaJVF_sumet;   //!
   TBranch        *b_MET_CellOut_Eflow_JetAreaRhoEta5JVF_etx;   //!
   TBranch        *b_MET_CellOut_Eflow_JetAreaRhoEta5JVF_ety;   //!
   TBranch        *b_MET_CellOut_Eflow_JetAreaRhoEta5JVF_phi;   //!
   TBranch        *b_MET_CellOut_Eflow_JetAreaRhoEta5JVF_et;   //!
   TBranch        *b_MET_CellOut_Eflow_JetAreaRhoEta5JVF_sumet;   //!
   TBranch        *b_MET_RefFinal_STVF_etx;   //!
   TBranch        *b_MET_RefFinal_STVF_ety;   //!
   TBranch        *b_MET_RefFinal_STVF_phi;   //!
   TBranch        *b_MET_RefFinal_STVF_et;   //!
   TBranch        *b_MET_RefFinal_STVF_sumet;   //!
   TBranch        *b_MET_RefMuon_Track_etx;   //!
   TBranch        *b_MET_RefMuon_Track_ety;   //!
   TBranch        *b_MET_RefMuon_Track_phi;   //!
   TBranch        *b_MET_RefMuon_Track_et;   //!
   TBranch        *b_MET_RefMuon_Track_sumet;   //!
   TBranch        *b_MET_RefMuon_Track_Staco_etx;   //!
   TBranch        *b_MET_RefMuon_Track_Staco_ety;   //!
   TBranch        *b_MET_RefMuon_Track_Staco_phi;   //!
   TBranch        *b_MET_RefMuon_Track_Staco_et;   //!
   TBranch        *b_MET_RefMuon_Track_Staco_sumet;   //!
   TBranch        *b_MET_LocHadTopo_etx;   //!
   TBranch        *b_MET_LocHadTopo_ety;   //!
   TBranch        *b_MET_LocHadTopo_phi;   //!
   TBranch        *b_MET_LocHadTopo_et;   //!
   TBranch        *b_MET_LocHadTopo_sumet;   //!
   TBranch        *b_MET_LocHadTopo_etx_CentralReg;   //!
   TBranch        *b_MET_LocHadTopo_ety_CentralReg;   //!
   TBranch        *b_MET_LocHadTopo_sumet_CentralReg;   //!
   TBranch        *b_MET_LocHadTopo_phi_CentralReg;   //!
   TBranch        *b_MET_LocHadTopo_etx_EndcapRegion;   //!
   TBranch        *b_MET_LocHadTopo_ety_EndcapRegion;   //!
   TBranch        *b_MET_LocHadTopo_sumet_EndcapRegion;   //!
   TBranch        *b_MET_LocHadTopo_phi_EndcapRegion;   //!
   TBranch        *b_MET_LocHadTopo_etx_ForwardReg;   //!
   TBranch        *b_MET_LocHadTopo_ety_ForwardReg;   //!
   TBranch        *b_MET_LocHadTopo_sumet_ForwardReg;   //!
   TBranch        *b_MET_LocHadTopo_phi_ForwardReg;   //!
   TBranch        *b_MET_Truth_NonInt_etx;   //!
   TBranch        *b_MET_Truth_NonInt_ety;   //!
   TBranch        *b_MET_Truth_NonInt_sumet;   //!
   TBranch        *b_MET_Truth_PileUp_NonInt_etx;   //!
   TBranch        *b_MET_Truth_PileUp_NonInt_ety;   //!
   TBranch        *b_MET_Truth_PileUp_NonInt_sumet;   //!
   TBranch        *b_el_MET_n;   //!
   TBranch        *b_el_MET_wpx;   //!
   TBranch        *b_el_MET_wpy;   //!
   TBranch        *b_el_MET_wet;   //!
   TBranch        *b_el_MET_statusWord;   //!
   TBranch        *b_ph_MET_n;   //!
   TBranch        *b_ph_MET_wpx;   //!
   TBranch        *b_ph_MET_wpy;   //!
   TBranch        *b_ph_MET_wet;   //!
   TBranch        *b_ph_MET_statusWord;   //!
   TBranch        *b_mu_staco_MET_n;   //!
   TBranch        *b_mu_staco_MET_wpx;   //!
   TBranch        *b_mu_staco_MET_wpy;   //!
   TBranch        *b_mu_staco_MET_wet;   //!
   TBranch        *b_mu_staco_MET_statusWord;   //!
   TBranch        *b_tau_MET_n;   //!
   TBranch        *b_tau_MET_wpx;   //!
   TBranch        *b_tau_MET_wpy;   //!
   TBranch        *b_tau_MET_wet;   //!
   TBranch        *b_tau_MET_statusWord;   //!
   TBranch        *b_jet_AntiKt4LCTopo_MET_n;   //!
   TBranch        *b_jet_AntiKt4LCTopo_MET_wpx;   //!
   TBranch        *b_jet_AntiKt4LCTopo_MET_wpy;   //!
   TBranch        *b_jet_AntiKt4LCTopo_MET_wet;   //!
   TBranch        *b_jet_AntiKt4LCTopo_MET_statusWord;   //!
   TBranch        *b_tau_MET_BDTMedium_n;   //!
   TBranch        *b_tau_MET_BDTMedium_wpx;   //!
   TBranch        *b_tau_MET_BDTMedium_wpy;   //!
   TBranch        *b_tau_MET_BDTMedium_wet;   //!
   TBranch        *b_tau_MET_BDTMedium_statusWord;   //!
   TBranch        *b_jet_AntiKt4LCTopo_MET_BDTMedium_n;   //!
   TBranch        *b_jet_AntiKt4LCTopo_MET_BDTMedium_wpx;   //!
   TBranch        *b_jet_AntiKt4LCTopo_MET_BDTMedium_wpy;   //!
   TBranch        *b_jet_AntiKt4LCTopo_MET_BDTMedium_wet;   //!
   TBranch        *b_jet_AntiKt4LCTopo_MET_BDTMedium_statusWord;   //!
   TBranch        *b_MET_RefFinal_BDTMedium_etx;   //!
   TBranch        *b_MET_RefFinal_BDTMedium_ety;   //!
   TBranch        *b_MET_RefFinal_BDTMedium_phi;   //!
   TBranch        *b_MET_RefFinal_BDTMedium_et;   //!
   TBranch        *b_MET_RefFinal_BDTMedium_sumet;   //!
   TBranch        *b_MET_RefTau_BDTMedium_etx;   //!
   TBranch        *b_MET_RefTau_BDTMedium_ety;   //!
   TBranch        *b_MET_RefTau_BDTMedium_phi;   //!
   TBranch        *b_MET_RefTau_BDTMedium_et;   //!
   TBranch        *b_MET_RefTau_BDTMedium_sumet;   //!
   TBranch        *b_MET_RefJet_BDTMedium_etx;   //!
   TBranch        *b_MET_RefJet_BDTMedium_ety;   //!
   TBranch        *b_MET_RefJet_BDTMedium_phi;   //!
   TBranch        *b_MET_RefJet_BDTMedium_et;   //!
   TBranch        *b_MET_RefJet_BDTMedium_sumet;   //!
   TBranch        *b_mu_staco_E;   //!
   TBranch        *b_mu_staco_pt;   //!
   TBranch        *b_mu_staco_eta;   //!
   TBranch        *b_mu_staco_phi;   //!
   TBranch        *b_mu_staco_px;   //!
   TBranch        *b_mu_staco_py;   //!
   TBranch        *b_mu_staco_pz;   //!
   TBranch        *b_mu_staco_charge;   //!
   TBranch        *b_mu_staco_author;   //!
   TBranch        *b_mu_staco_matchchi2;   //!
   TBranch        *b_mu_staco_etcone20;   //!
   TBranch        *b_mu_staco_etcone30;   //!
   TBranch        *b_mu_staco_etcone40;   //!
   TBranch        *b_mu_staco_ptcone20;   //!
   TBranch        *b_mu_staco_ptcone30;   //!
   TBranch        *b_mu_staco_ptcone40;   //!
   TBranch        *b_mu_staco_isStandAloneMuon;   //!
   TBranch        *b_mu_staco_isCombinedMuon;   //!
   TBranch        *b_mu_staco_isLowPtReconstructedMuon;   //!
   TBranch        *b_mu_staco_isSegmentTaggedMuon;   //!
   TBranch        *b_mu_staco_isCaloMuonId;   //!
   TBranch        *b_mu_staco_alsoFoundByLowPt;   //!
   TBranch        *b_mu_staco_alsoFoundByCaloMuonId;   //!
   TBranch        *b_mu_staco_loose;   //!
   TBranch        *b_mu_staco_medium;   //!
   TBranch        *b_mu_staco_tight;   //!
   TBranch        *b_mu_staco_z0_exPV;   //!
   TBranch        *b_mu_staco_id_d0_exPV;   //!
   TBranch        *b_mu_staco_id_z0_exPV;   //!
   TBranch        *b_mu_staco_id_phi_exPV;   //!
   TBranch        *b_mu_staco_id_theta_exPV;   //!
   TBranch        *b_mu_staco_id_qoverp_exPV;   //!
   TBranch        *b_mu_staco_me_phi_exPV;   //!
   TBranch        *b_mu_staco_me_theta_exPV;   //!
   TBranch        *b_mu_staco_me_qoverp_exPV;   //!
   TBranch        *b_mu_staco_ms_phi;   //!
   TBranch        *b_mu_staco_ms_theta;   //!
   TBranch        *b_mu_staco_ms_qoverp;   //!
   TBranch        *b_mu_staco_id_phi;   //!
   TBranch        *b_mu_staco_id_theta;   //!
   TBranch        *b_mu_staco_id_qoverp;   //!
   TBranch        *b_mu_staco_me_phi;   //!
   TBranch        *b_mu_staco_me_theta;   //!
   TBranch        *b_mu_staco_me_qoverp;   //!
   TBranch        *b_mu_staco_nBLHits;   //!
   TBranch        *b_mu_staco_nPixHits;   //!
   TBranch        *b_mu_staco_nSCTHits;   //!
   TBranch        *b_mu_staco_nTRTHits;   //!
   TBranch        *b_mu_staco_nPixHoles;   //!
   TBranch        *b_mu_staco_nSCTHoles;   //!
   TBranch        *b_mu_staco_nTRTOutliers;   //!
   TBranch        *b_mu_staco_nPixelDeadSensors;   //!
   TBranch        *b_mu_staco_nSCTDeadSensors;   //!
   TBranch        *b_mu_staco_expectBLayerHit;   //!
   TBranch        *b_mu_staco_trackd0pv;   //!
   TBranch        *b_mu_staco_trackz0pv;   //!
   TBranch        *b_mu_staco_tracksigd0pv;   //!
   TBranch        *b_mu_staco_tracksigz0pv;   //!
   TBranch        *b_mu_staco_trackd0pvunbiased;   //!
   TBranch        *b_mu_staco_trackz0pvunbiased;   //!
   TBranch        *b_mu_staco_tracksigd0pvunbiased;   //!
   TBranch        *b_mu_staco_tracksigz0pvunbiased;   //!
   TBranch        *b_top_hfor_type;   //!
   TBranch        *b_vxp_n;   //!
   TBranch        *b_vxp_x;   //!
   TBranch        *b_vxp_y;   //!
   TBranch        *b_vxp_z;   //!
   TBranch        *b_vxp_type;   //!
   TBranch        *b_vxp_nTracks;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_bcid;   //!
   TBranch        *b_actualIntPerXing;   //!
   TBranch        *b_averageIntPerXing;   //!
   TBranch        *b_mc_channel_number;   //!
   TBranch        *b_mc_event_number;   //!
   TBranch        *b_mc_event_weight;   //!
   TBranch        *b_larFlags;   //!
   TBranch        *b_coreFlags;   //!
   TBranch        *b_larError;   //!
   TBranch        *b_tileError;   //!
   TBranch        *b_trig_L1_emtau_n;   //!
   TBranch        *b_trig_L1_emtau_eta;   //!
   TBranch        *b_trig_L1_emtau_phi;   //!
   TBranch        *b_trig_L1_emtau_EMClus;   //!
   TBranch        *b_trig_L1_emtau_hadCore;   //!
   TBranch        *b_trig_EF_tau_n;   //!
   TBranch        *b_trig_EF_tau_Et;   //!
   TBranch        *b_trig_EF_tau_pt;   //!
   TBranch        *b_trig_EF_tau_m;   //!
   TBranch        *b_trig_EF_tau_eta;   //!
   TBranch        *b_trig_EF_tau_phi;   //!
   TBranch        *b_trig_Nav_n;   //!
   TBranch        *b_trig_Nav_chain_ChainId;   //!
   TBranch        *b_trig_Nav_chain_RoIType;   //!
   TBranch        *b_trig_Nav_chain_RoIIndex;   //!
   TBranch        *b_trig_DB_SMK;   //!
   TBranch        *b_trig_L1_mu_n;   //!
   TBranch        *b_trig_L1_mu_pt;   //!
   TBranch        *b_trig_L1_mu_eta;   //!
   TBranch        *b_trig_L1_mu_phi;   //!
   TBranch        *b_trig_L1_mu_thrName;   //!
   TBranch        *b_trig_L2_muonfeature_n;   //!
   TBranch        *b_trig_L2_muonfeature_pt;   //!
   TBranch        *b_trig_L2_muonfeature_eta;   //!
   TBranch        *b_trig_L2_muonfeature_phi;   //!
   TBranch        *b_trig_L2_combmuonfeature_n;   //!
   TBranch        *b_trig_L2_combmuonfeature_pt;   //!
   TBranch        *b_trig_L2_combmuonfeature_eta;   //!
   TBranch        *b_trig_L2_combmuonfeature_phi;   //!
   TBranch        *b_trig_EF_trigmuonef_n;   //!
   TBranch        *b_trig_EF_trigmuonef_track_n;   //!
   TBranch        *b_trig_EF_trigmuonef_track_MuonType;   //!
   TBranch        *b_trig_EF_trigmuonef_track_SA_pt;   //!
   TBranch        *b_trig_EF_trigmuonef_track_SA_eta;   //!
   TBranch        *b_trig_EF_trigmuonef_track_SA_phi;   //!
   TBranch        *b_trig_EF_trigmuonef_track_CB_pt;   //!
   TBranch        *b_trig_EF_trigmuonef_track_CB_eta;   //!
   TBranch        *b_trig_EF_trigmuonef_track_CB_phi;   //!
   TBranch        *b_trig_EF_trigmugirl_n;   //!
   TBranch        *b_trig_EF_trigmugirl_track_n;   //!
   TBranch        *b_trig_EF_trigmugirl_track_MuonType;   //!
   TBranch        *b_trig_EF_trigmugirl_track_SA_pt;   //!
   TBranch        *b_trig_EF_trigmugirl_track_SA_eta;   //!
   TBranch        *b_trig_EF_trigmugirl_track_SA_phi;   //!
   TBranch        *b_trig_EF_trigmugirl_track_CB_pt;   //!
   TBranch        *b_trig_EF_trigmugirl_track_CB_eta;   //!
   TBranch        *b_trig_EF_trigmugirl_track_CB_phi;   //!
   TBranch        *b_trig_RoI_L2_mu_n;   //!
   TBranch        *b_trig_RoI_L2_mu_MuonFeature;   //!
   TBranch        *b_trig_RoI_L2_mu_CombinedMuonFeature;   //!
   TBranch        *b_trig_RoI_L2_mu_CombinedMuonFeatureStatus;   //!
   TBranch        *b_trig_RoI_L2_mu_Muon_ROI;   //!
   TBranch        *b_trig_RoI_EF_mu_n;   //!
   TBranch        *b_trig_RoI_EF_mu_Muon_ROI;   //!
   TBranch        *b_trig_RoI_EF_mu_TrigMuonEFInfoContainer;   //!
   TBranch        *b_trig_RoI_EF_mu_TrigMuonEFInfoContainerStatus;   //!
   TBranch        *b_trig_RoI_EF_mu_TrigMuonEFIsolationContainer;   //!
   TBranch        *b_trig_RoI_EF_mu_TrigMuonEFIsolationContainerStatus;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_lbn;   //!
   TBranch        *b_el_n;   //!
   TBranch        *b_mu_staco_n;   //!
   TBranch        *b_mcevt0_event_number;   //!
   TBranch        *b_mcevt0_event_scale;   //!
   TBranch        *b_mcevt0_alphaQCD;   //!
   TBranch        *b_mcevt0_alphaQED;   //!
   TBranch        *b_mcevt0_pdf_id1;   //!
   TBranch        *b_mcevt0_pdf_id2;   //!
   TBranch        *b_mcevt0_pdf_x1;   //!
   TBranch        *b_mcevt0_pdf_x2;   //!
   TBranch        *b_mcevt0_pdf_scale;   //!
   TBranch        *b_mcevt0_pdf1;   //!
   TBranch        *b_mcevt0_pdf2;   //!
   TBranch        *b_mcevt0_weight;   //!
   TBranch        *b_truthLP_n;   //!
   TBranch        *b_truthLP_pt;   //!
   TBranch        *b_truthLP_m;   //!
   TBranch        *b_truthLP_eta;   //!
   TBranch        *b_truthLP_phi;   //!
   TBranch        *b_truthLP_pdgId;   //!
   TBranch        *b_truthLP_charge;   //!
   TBranch        *b_truthLP_status;   //!
   TBranch        *b_truthLP_pdgId_parent;   //!
   TBranch        *b_truthTau_n;   //!
   TBranch        *b_truthTau_pt;   //!
   TBranch        *b_truthTau_m;   //!
   TBranch        *b_truthTau_eta;   //!
   TBranch        *b_truthTau_phi;   //!
   TBranch        *b_truthTau_pdgId;   //!
   TBranch        *b_truthTau_charge;   //!
   TBranch        *b_truthTau_status;   //!
   TBranch        *b_truthTau_pdgId_parent;   //!
   TBranch        *b_truthTau_isLeptonicDecay;   //!
   TBranch        *b_truthTau_nProng;   //!
   TBranch        *b_truthTau_vis_Et;   //!
   TBranch        *b_truthTau_vis_m;   //!
   TBranch        *b_truthTau_vis_eta;   //!
   TBranch        *b_truthTau_vis_phi;   //!
   TBranch        *b_truthTau_invis_Et;   //!
   TBranch        *b_truthTau_invis_m;   //!
   TBranch        *b_truthTau_invis_eta;   //!
   TBranch        *b_truthTau_invis_phi;   //!
   TBranch        *b_truthTau_child_n;   //!
   TBranch        *b_truthTau_child_pt;   //!
   TBranch        *b_truthTau_child_m;   //!
   TBranch        *b_truthTau_child_eta;   //!
   TBranch        *b_truthTau_child_phi;   //!
   TBranch        *b_truthTau_child_pdgId;   //!
   TBranch        *b_truthTau_child_charge;   //!
   TBranch        *b_truthTau_child_status;   //!
   TBranch        *b_truthH_n;   //!
   TBranch        *b_truthH_pt;   //!
   TBranch        *b_truthH_m;   //!
   TBranch        *b_truthH_eta;   //!
   TBranch        *b_truthH_phi;   //!
   TBranch        *b_truthH_pdgId;   //!
   TBranch        *b_truthH_charge;   //!
   TBranch        *b_truthH_status;   //!
   TBranch        *b_truthH_child_n;   //!
   TBranch        *b_truthH_child_pt;   //!
   TBranch        *b_truthH_child_m;   //!
   TBranch        *b_truthH_child_eta;   //!
   TBranch        *b_truthH_child_phi;   //!
   TBranch        *b_truthH_child_pdgId;   //!
   TBranch        *b_truthH_child_charge;   //!
   TBranch        *b_truthH_child_status;   //!
   TBranch        *b_truthZ_n;   //!
   TBranch        *b_truthZ_pt;   //!
   TBranch        *b_truthZ_m;   //!
   TBranch        *b_truthZ_eta;   //!
   TBranch        *b_truthZ_phi;   //!
   TBranch        *b_truthZ_pdgId;   //!
   TBranch        *b_truthZ_status;   //!
   TBranch        *b_truthZ_pdgId_parent;   //!
   TBranch        *b_truthZ_child_n;   //!
   TBranch        *b_truthZ_child_pt;   //!
   TBranch        *b_truthZ_child_m;   //!
   TBranch        *b_truthZ_child_eta;   //!
   TBranch        *b_truthZ_child_phi;   //!
   TBranch        *b_truthZ_child_pdgId;   //!
   TBranch        *b_truthZ_child_charge;   //!
   TBranch        *b_truthZ_child_status;   //!
   TBranch        *b_truthW_n;   //!
   TBranch        *b_truthW_pt;   //!
   TBranch        *b_truthW_m;   //!
   TBranch        *b_truthW_eta;   //!
   TBranch        *b_truthW_phi;   //!
   TBranch        *b_truthW_pdgId;   //!
   TBranch        *b_truthW_charge;   //!
   TBranch        *b_truthW_status;   //!
   TBranch        *b_truthW_pdgId_parent;   //!
   TBranch        *b_truthW_child_n;   //!
   TBranch        *b_truthW_child_pt;   //!
   TBranch        *b_truthW_child_m;   //!
   TBranch        *b_truthW_child_eta;   //!
   TBranch        *b_truthW_child_phi;   //!
   TBranch        *b_truthW_child_pdgId;   //!
   TBranch        *b_truthW_child_charge;   //!
   TBranch        *b_truthW_child_status;   //!
   TBranch        *b_truthSpecial_n;   //!
   TBranch        *b_truthSpecial_pt;   //!
   TBranch        *b_truthSpecial_m;   //!
   TBranch        *b_truthSpecial_eta;   //!
   TBranch        *b_truthSpecial_phi;   //!
   TBranch        *b_truthSpecial_pdgId;   //!

   MC_NOMINAL(TTree *tree=0);
   virtual ~MC_NOMINAL();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(bool all, bool onlyGGF, bool onlyVBF, bool onlyZH, bool onlyWH, bool LFV, bool Htautau, bool Ztautau, int METOption, bool reco);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef MC_NOMINAL_cxx
MC_NOMINAL::MC_NOMINAL(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/storage/groups/atlsch/uli/samples/GGF_TauE/user.ubaumann.mc12_8TeV.189894.PowHegPythia8_ggH125_taue.merge.e3057_s1773_s1776_r4485_r4540_p1344.hsg4_1.2_NTUP_TAU.root.13493252/user.ubaumann.4576451._000001.NTUP_TAU.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/storage/groups/atlsch/uli/samples/GGF_TauE/user.ubaumann.mc12_8TeV.189894.PowHegPythia8_ggH125_taue.merge.e3057_s1773_s1776_r4485_r4540_p1344.hsg4_1.2_NTUP_TAU.root.13493252/user.ubaumann.4576451._000001.NTUP_TAU.root");
      }
      f->GetObject("tau",tree);

   }
   Init(tree);
}

MC_NOMINAL::~MC_NOMINAL()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t MC_NOMINAL::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t MC_NOMINAL::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void MC_NOMINAL::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   trig_EF_tau_EF_tau20T_medium1_e15vh_medium1 = 0;
   trig_EF_tau_EF_tau20T_medium_mu15 = 0;
   trig_EF_tau_EF_tau20_medium1_mu15 = 0;
   trig_L2_combmuonfeature_L2_2mu13 = 0;
   trig_L2_combmuonfeature_L2_2mu15 = 0;
   trig_L2_combmuonfeature_L2_2mu6 = 0;
   trig_L2_combmuonfeature_L2_mu15 = 0;
   trig_L2_combmuonfeature_L2_mu18_medium = 0;
   trig_L2_combmuonfeature_L2_mu18_tight = 0;
   trig_L2_combmuonfeature_L2_mu18_tight_e7_medium1 = 0;
   trig_L2_combmuonfeature_L2_mu24_medium = 0;
   trig_L2_combmuonfeature_L2_mu24_tight = 0;
   trig_L2_combmuonfeature_L2_mu36_tight = 0;
   trig_L2_combmuonfeature_L2_mu6 = 0;
   trig_L2_combmuonfeature_L2_mu8 = 0;
   trig_EF_trigmuonef_EF_2mu13 = 0;
   trig_EF_trigmuonef_EF_2mu15 = 0;
   trig_EF_trigmuonef_EF_2mu6 = 0;
   trig_EF_trigmuonef_EF_mu15 = 0;
   trig_EF_trigmuonef_EF_mu18_medium = 0;
   trig_EF_trigmuonef_EF_mu18_tight = 0;
   trig_EF_trigmuonef_EF_mu18_tight_2mu4_EFFS = 0;
   trig_EF_trigmuonef_EF_mu18_tight_e7_medium1 = 0;
   trig_EF_trigmuonef_EF_mu18_tight_mu8_EFFS = 0;
   trig_EF_trigmuonef_EF_mu24_medium = 0;
   trig_EF_trigmuonef_EF_mu24_tight = 0;
   trig_EF_trigmuonef_EF_mu24_tight_mu6_EFFS = 0;
   trig_EF_trigmuonef_EF_mu24i_tight = 0;
   trig_EF_trigmuonef_EF_mu36_tight = 0;
   trig_EF_trigmuonef_EF_mu40_MSonly_barrel_tight = 0;
   trig_EF_trigmuonef_EF_mu6 = 0;
   trig_EF_trigmuonef_EF_mu8 = 0;
   trig_EF_trigmugirl_EF_2mu13 = 0;
   trig_EF_trigmugirl_EF_2mu15 = 0;
   trig_EF_trigmugirl_EF_2mu6 = 0;
   trig_EF_trigmugirl_EF_mu15 = 0;
   trig_EF_trigmugirl_EF_mu18_medium = 0;
   trig_EF_trigmugirl_EF_mu18_tight = 0;
   trig_EF_trigmugirl_EF_mu18_tight_2mu4_EFFS = 0;
   trig_EF_trigmugirl_EF_mu18_tight_e7_medium1 = 0;
   trig_EF_trigmugirl_EF_mu18_tight_mu8_EFFS = 0;
   trig_EF_trigmugirl_EF_mu24_medium = 0;
   trig_EF_trigmugirl_EF_mu24_tight = 0;
   trig_EF_trigmugirl_EF_mu24_tight_mu6_EFFS = 0;
   trig_EF_trigmugirl_EF_mu24i_tight = 0;
   trig_EF_trigmugirl_EF_mu36_tight = 0;
   trig_EF_trigmugirl_EF_mu40_MSonly_barrel_tight = 0;
   trig_EF_trigmugirl_EF_mu6 = 0;
   trig_EF_trigmugirl_EF_mu8 = 0;
   tau_Et = 0;
   tau_pt = 0;
   tau_m = 0;
   tau_eta = 0;
   tau_phi = 0;
   tau_charge = 0;
   tau_BDTEleScore = 0;
   tau_BDTJetScore = 0;
   tau_likelihood = 0;
   tau_SafeLikelihood = 0;
   tau_electronVetoLoose = 0;
   tau_electronVetoMedium = 0;
   tau_electronVetoTight = 0;
   tau_muonVeto = 0;
   tau_tauLlhLoose = 0;
   tau_tauLlhMedium = 0;
   tau_tauLlhTight = 0;
   tau_JetBDTSigLoose = 0;
   tau_JetBDTSigMedium = 0;
   tau_JetBDTSigTight = 0;
   tau_EleBDTLoose = 0;
   tau_EleBDTMedium = 0;
   tau_EleBDTTight = 0;
   tau_author = 0;
   tau_numTrack = 0;
   tau_etOverPtLeadTrk = 0;
   tau_leadTrkPt = 0;
   tau_jet_jvtxf = 0;
   tau_track_n = 0;
   tau_track_d0 = 0;
   tau_track_z0 = 0;
   tau_track_phi = 0;
   tau_track_theta = 0;
   tau_track_qoverp = 0;
   tau_track_pt = 0;
   tau_track_eta = 0;
   tau_track_atPV_d0 = 0;
   tau_track_atPV_z0 = 0;
   tau_track_atPV_phi = 0;
   tau_track_atPV_theta = 0;
   tau_track_atPV_qoverp = 0;
   tau_track_atPV_pt = 0;
   tau_track_atPV_eta = 0;
   mc_pt = 0;
   mc_m = 0;
   mc_eta = 0;
   mc_phi = 0;
   mc_status = 0;
   mc_barcode = 0;
   mc_pdgId = 0;
   mc_charge = 0;
   mc_child_index = 0;
   mc_parent_index = 0;
   jet_antikt4truth_E = 0;
   jet_antikt4truth_pt = 0;
   jet_antikt4truth_m = 0;
   jet_antikt4truth_eta = 0;
   jet_antikt4truth_phi = 0;
   el_charge = 0;
   el_author = 0;
   el_isEM = 0;
   el_OQ = 0;
   el_loose = 0;
   el_medium = 0;
   el_tight = 0;
   el_loosePP = 0;
   el_mediumPP = 0;
   el_tightPP = 0;
   el_Ethad = 0;
   el_Ethad1 = 0;
   el_f1 = 0;
   el_Emax2 = 0;
   el_wstot = 0;
   el_emaxs1 = 0;
   el_E237 = 0;
   el_E277 = 0;
   el_weta2 = 0;
   el_f3 = 0;
   el_Etcone20 = 0;
   el_Etcone30 = 0;
   el_Etcone40 = 0;
   el_ptcone20 = 0;
   el_ptcone30 = 0;
   el_ptcone40 = 0;
   el_deltaeta1 = 0;
   el_deltaphi2 = 0;
   el_expectHitInBLayer = 0;
   el_trackd0_physics = 0;
   el_reta = 0;
   el_topoEtcone20 = 0;
   el_topoEtcone30 = 0;
   el_topoEtcone40 = 0;
   el_etap = 0;
   el_Es2 = 0;
   el_etas2 = 0;
   el_phis2 = 0;
   el_cl_E = 0;
   el_cl_pt = 0;
   el_cl_eta = 0;
   el_cl_phi = 0;
   el_trackphi = 0;
   el_trackqoverp = 0;
   el_trackpt = 0;
   el_tracketa = 0;
   el_nBLHits = 0;
   el_nPixHits = 0;
   el_nSCTHits = 0;
   el_nTRTHits = 0;
   el_nBLayerOutliers = 0;
   el_nPixelOutliers = 0;
   el_nSCTOutliers = 0;
   el_nTRTOutliers = 0;
   el_nSiHits = 0;
   el_TRTHighTOutliersRatio = 0;
   el_trackd0pv = 0;
   el_trackz0pv = 0;
   el_tracksigd0pv = 0;
   el_tracksigz0pv = 0;
   el_trackd0pvunbiased = 0;
   el_trackz0pvunbiased = 0;
   el_tracksigd0pvunbiased = 0;
   el_tracksigz0pvunbiased = 0;
   el_topoEtcone20_corrected = 0;
   el_topoEtcone30_corrected = 0;
   el_topoEtcone40_corrected = 0;
   el_ED_median = 0;
   jet_E = 0;
   jet_pt = 0;
   jet_m = 0;
   jet_eta = 0;
   jet_phi = 0;
   jet_EtaOrigin = 0;
   jet_PhiOrigin = 0;
   jet_MOrigin = 0;
   jet_Timing = 0;
   jet_LArQuality = 0;
   jet_sumPtTrk = 0;
   jet_HECQuality = 0;
   jet_NegativeE = 0;
   jet_AverageLArQF = 0;
   jet_BCH_CORR_CELL = 0;
   jet_BCH_CORR_DOTX = 0;
   jet_BCH_CORR_JET = 0;
   jet_SamplingMax = 0;
   jet_fracSamplingMax = 0;
   jet_hecf = 0;
   jet_isUgly = 0;
   jet_isBadLooseMinus = 0;
   jet_isBadLoose = 0;
   jet_isBadMedium = 0;
   jet_isBadTight = 0;
   jet_emfrac = 0;
   jet_Offset = 0;
   jet_EMJES = 0;
   jet_EMJES_EtaCorr = 0;
   jet_EMJESnooffset = 0;
   jet_emscale_E = 0;
   jet_emscale_pt = 0;
   jet_emscale_m = 0;
   jet_emscale_eta = 0;
   jet_emscale_phi = 0;
   jet_ActiveArea = 0;
   jet_ActiveAreaPx = 0;
   jet_ActiveAreaPy = 0;
   jet_ActiveAreaPz = 0;
   jet_ActiveAreaE = 0;
   jet_jvtxf = 0;
   jet_jvtxfFull = 0;
   jet_jvtx_x = 0;
   jet_jvtx_y = 0;
   jet_jvtx_z = 0;
   jet_constscale_E = 0;
   jet_constscale_pt = 0;
   jet_constscale_m = 0;
   jet_constscale_eta = 0;
   jet_constscale_phi = 0;
   jet_flavor_weight_Comb = 0;
   jet_flavor_weight_IP2D = 0;
   jet_flavor_weight_IP3D = 0;
   jet_flavor_weight_SV0 = 0;
   jet_flavor_weight_SV1 = 0;
   jet_flavor_weight_SV2 = 0;
   jet_flavor_weight_SoftMuonTagChi2 = 0;
   jet_flavor_weight_SecondSoftMuonTagChi2 = 0;
   jet_flavor_weight_JetFitterTagNN = 0;
   jet_flavor_weight_JetFitterCOMBNN = 0;
   jet_flavor_weight_MV1 = 0;
   jet_flavor_weight_MV2 = 0;
   jet_flavor_weight_GbbNN = 0;
   jet_flavor_weight_JetFitterCharm = 0;
   jet_flavor_weight_MV3_bVSu = 0;
   jet_flavor_weight_MV3_bVSc = 0;
   jet_flavor_weight_MV3_cVSu = 0;
   jet_flavor_truth_label = 0;
   jet_flavor_truth_dRminToB = 0;
   jet_flavor_truth_dRminToC = 0;
   jet_flavor_truth_dRminToT = 0;
   jet_flavor_truth_BHadronpdg = 0;
   jet_AntiKt4TopoEM_E = 0;
   jet_AntiKt4TopoEM_pt = 0;
   jet_AntiKt4TopoEM_m = 0;
   jet_AntiKt4TopoEM_eta = 0;
   jet_AntiKt4TopoEM_phi = 0;
   jet_AntiKt4TopoEM_EtaOrigin = 0;
   jet_AntiKt4TopoEM_PhiOrigin = 0;
   jet_AntiKt4TopoEM_MOrigin = 0;
   jet_AntiKt4TopoEM_Timing = 0;
   jet_AntiKt4TopoEM_LArQuality = 0;
   jet_AntiKt4TopoEM_sumPtTrk = 0;
   jet_AntiKt4TopoEM_HECQuality = 0;
   jet_AntiKt4TopoEM_NegativeE = 0;
   jet_AntiKt4TopoEM_AverageLArQF = 0;
   jet_AntiKt4TopoEM_BCH_CORR_CELL = 0;
   jet_AntiKt4TopoEM_BCH_CORR_DOTX = 0;
   jet_AntiKt4TopoEM_BCH_CORR_JET = 0;
   jet_AntiKt4TopoEM_SamplingMax = 0;
   jet_AntiKt4TopoEM_fracSamplingMax = 0;
   jet_AntiKt4TopoEM_hecf = 0;
   jet_AntiKt4TopoEM_isUgly = 0;
   jet_AntiKt4TopoEM_isBadLooseMinus = 0;
   jet_AntiKt4TopoEM_isBadLoose = 0;
   jet_AntiKt4TopoEM_isBadMedium = 0;
   jet_AntiKt4TopoEM_isBadTight = 0;
   jet_AntiKt4TopoEM_emfrac = 0;
   jet_AntiKt4TopoEM_Offset = 0;
   jet_AntiKt4TopoEM_EMJES = 0;
   jet_AntiKt4TopoEM_EMJES_EtaCorr = 0;
   jet_AntiKt4TopoEM_EMJESnooffset = 0;
   jet_AntiKt4TopoEM_emscale_E = 0;
   jet_AntiKt4TopoEM_emscale_pt = 0;
   jet_AntiKt4TopoEM_emscale_m = 0;
   jet_AntiKt4TopoEM_emscale_eta = 0;
   jet_AntiKt4TopoEM_emscale_phi = 0;
   jet_AntiKt4TopoEM_ActiveArea = 0;
   jet_AntiKt4TopoEM_ActiveAreaPx = 0;
   jet_AntiKt4TopoEM_ActiveAreaPy = 0;
   jet_AntiKt4TopoEM_ActiveAreaPz = 0;
   jet_AntiKt4TopoEM_ActiveAreaE = 0;
   jet_AntiKt4TopoEM_jvtxf = 0;
   jet_AntiKt4TopoEM_jvtxfFull = 0;
   jet_AntiKt4TopoEM_jvtx_x = 0;
   jet_AntiKt4TopoEM_jvtx_y = 0;
   jet_AntiKt4TopoEM_jvtx_z = 0;
   jet_AntiKt4TopoEM_flavor_weight_Comb = 0;
   jet_AntiKt4TopoEM_flavor_weight_IP2D = 0;
   jet_AntiKt4TopoEM_flavor_weight_IP3D = 0;
   jet_AntiKt4TopoEM_flavor_weight_SV0 = 0;
   jet_AntiKt4TopoEM_flavor_weight_SV1 = 0;
   jet_AntiKt4TopoEM_flavor_weight_SV2 = 0;
   jet_AntiKt4TopoEM_flavor_weight_SoftMuonTagChi2 = 0;
   jet_AntiKt4TopoEM_flavor_weight_SecondSoftMuonTagChi2 = 0;
   jet_AntiKt4TopoEM_flavor_weight_JetFitterTagNN = 0;
   jet_AntiKt4TopoEM_flavor_weight_JetFitterCOMBNN = 0;
   jet_AntiKt4TopoEM_flavor_weight_MV1 = 0;
   jet_AntiKt4TopoEM_flavor_weight_MV2 = 0;
   jet_AntiKt4TopoEM_flavor_weight_GbbNN = 0;
   jet_AntiKt4TopoEM_flavor_weight_JetFitterCharm = 0;
   jet_AntiKt4TopoEM_flavor_weight_MV3_bVSu = 0;
   jet_AntiKt4TopoEM_flavor_weight_MV3_bVSc = 0;
   jet_AntiKt4TopoEM_flavor_weight_MV3_cVSu = 0;
   jet_AntiKt4TopoEM_flavor_truth_label = 0;
   jet_AntiKt4TopoEM_flavor_truth_dRminToB = 0;
   jet_AntiKt4TopoEM_flavor_truth_dRminToC = 0;
   jet_AntiKt4TopoEM_flavor_truth_dRminToT = 0;
   jet_AntiKt4TopoEM_flavor_truth_BHadronpdg = 0;
   el_MET_wpx = 0;
   el_MET_wpy = 0;
   el_MET_wet = 0;
   el_MET_statusWord = 0;
   ph_MET_wpx = 0;
   ph_MET_wpy = 0;
   ph_MET_wet = 0;
   ph_MET_statusWord = 0;
   mu_staco_MET_wpx = 0;
   mu_staco_MET_wpy = 0;
   mu_staco_MET_wet = 0;
   mu_staco_MET_statusWord = 0;
   tau_MET_wpx = 0;
   tau_MET_wpy = 0;
   tau_MET_wet = 0;
   tau_MET_statusWord = 0;
   jet_AntiKt4LCTopo_MET_wpx = 0;
   jet_AntiKt4LCTopo_MET_wpy = 0;
   jet_AntiKt4LCTopo_MET_wet = 0;
   jet_AntiKt4LCTopo_MET_statusWord = 0;
   tau_MET_BDTMedium_wpx = 0;
   tau_MET_BDTMedium_wpy = 0;
   tau_MET_BDTMedium_wet = 0;
   tau_MET_BDTMedium_statusWord = 0;
   jet_AntiKt4LCTopo_MET_BDTMedium_wpx = 0;
   jet_AntiKt4LCTopo_MET_BDTMedium_wpy = 0;
   jet_AntiKt4LCTopo_MET_BDTMedium_wet = 0;
   jet_AntiKt4LCTopo_MET_BDTMedium_statusWord = 0;
   mu_staco_E = 0;
   mu_staco_pt = 0;
   mu_staco_eta = 0;
   mu_staco_phi = 0;
   mu_staco_px = 0;
   mu_staco_py = 0;
   mu_staco_pz = 0;
   mu_staco_charge = 0;
   mu_staco_author = 0;
   mu_staco_matchchi2 = 0;
   mu_staco_etcone20 = 0;
   mu_staco_etcone30 = 0;
   mu_staco_etcone40 = 0;
   mu_staco_ptcone20 = 0;
   mu_staco_ptcone30 = 0;
   mu_staco_ptcone40 = 0;
   mu_staco_isStandAloneMuon = 0;
   mu_staco_isCombinedMuon = 0;
   mu_staco_isLowPtReconstructedMuon = 0;
   mu_staco_isSegmentTaggedMuon = 0;
   mu_staco_isCaloMuonId = 0;
   mu_staco_alsoFoundByLowPt = 0;
   mu_staco_alsoFoundByCaloMuonId = 0;
   mu_staco_loose = 0;
   mu_staco_medium = 0;
   mu_staco_tight = 0;
   mu_staco_z0_exPV = 0;
   mu_staco_id_d0_exPV = 0;
   mu_staco_id_z0_exPV = 0;
   mu_staco_id_phi_exPV = 0;
   mu_staco_id_theta_exPV = 0;
   mu_staco_id_qoverp_exPV = 0;
   mu_staco_me_phi_exPV = 0;
   mu_staco_me_theta_exPV = 0;
   mu_staco_me_qoverp_exPV = 0;
   mu_staco_ms_phi = 0;
   mu_staco_ms_theta = 0;
   mu_staco_ms_qoverp = 0;
   mu_staco_id_phi = 0;
   mu_staco_id_theta = 0;
   mu_staco_id_qoverp = 0;
   mu_staco_me_phi = 0;
   mu_staco_me_theta = 0;
   mu_staco_me_qoverp = 0;
   mu_staco_nBLHits = 0;
   mu_staco_nPixHits = 0;
   mu_staco_nSCTHits = 0;
   mu_staco_nTRTHits = 0;
   mu_staco_nPixHoles = 0;
   mu_staco_nSCTHoles = 0;
   mu_staco_nTRTOutliers = 0;
   mu_staco_nPixelDeadSensors = 0;
   mu_staco_nSCTDeadSensors = 0;
   mu_staco_expectBLayerHit = 0;
   mu_staco_trackd0pv = 0;
   mu_staco_trackz0pv = 0;
   mu_staco_tracksigd0pv = 0;
   mu_staco_tracksigz0pv = 0;
   mu_staco_trackd0pvunbiased = 0;
   mu_staco_trackz0pvunbiased = 0;
   mu_staco_tracksigd0pvunbiased = 0;
   mu_staco_tracksigz0pvunbiased = 0;
   vxp_x = 0;
   vxp_y = 0;
   vxp_z = 0;
   vxp_type = 0;
   vxp_nTracks = 0;
   trig_L1_emtau_eta = 0;
   trig_L1_emtau_phi = 0;
   trig_L1_emtau_EMClus = 0;
   trig_L1_emtau_hadCore = 0;
   trig_EF_tau_Et = 0;
   trig_EF_tau_pt = 0;
   trig_EF_tau_m = 0;
   trig_EF_tau_eta = 0;
   trig_EF_tau_phi = 0;
   trig_Nav_chain_ChainId = 0;
   trig_Nav_chain_RoIType = 0;
   trig_Nav_chain_RoIIndex = 0;
   trig_L1_mu_pt = 0;
   trig_L1_mu_eta = 0;
   trig_L1_mu_phi = 0;
   trig_L1_mu_thrName = 0;
   trig_L2_muonfeature_pt = 0;
   trig_L2_muonfeature_eta = 0;
   trig_L2_muonfeature_phi = 0;
   trig_L2_combmuonfeature_pt = 0;
   trig_L2_combmuonfeature_eta = 0;
   trig_L2_combmuonfeature_phi = 0;
   trig_EF_trigmuonef_track_n = 0;
   trig_EF_trigmuonef_track_MuonType = 0;
   trig_EF_trigmuonef_track_SA_pt = 0;
   trig_EF_trigmuonef_track_SA_eta = 0;
   trig_EF_trigmuonef_track_SA_phi = 0;
   trig_EF_trigmuonef_track_CB_pt = 0;
   trig_EF_trigmuonef_track_CB_eta = 0;
   trig_EF_trigmuonef_track_CB_phi = 0;
   trig_EF_trigmugirl_track_n = 0;
   trig_EF_trigmugirl_track_MuonType = 0;
   trig_EF_trigmugirl_track_SA_pt = 0;
   trig_EF_trigmugirl_track_SA_eta = 0;
   trig_EF_trigmugirl_track_SA_phi = 0;
   trig_EF_trigmugirl_track_CB_pt = 0;
   trig_EF_trigmugirl_track_CB_eta = 0;
   trig_EF_trigmugirl_track_CB_phi = 0;
   trig_RoI_L2_mu_MuonFeature = 0;
   trig_RoI_L2_mu_CombinedMuonFeature = 0;
   trig_RoI_L2_mu_CombinedMuonFeatureStatus = 0;
   trig_RoI_L2_mu_Muon_ROI = 0;
   trig_RoI_EF_mu_Muon_ROI = 0;
   trig_RoI_EF_mu_TrigMuonEFInfoContainer = 0;
   trig_RoI_EF_mu_TrigMuonEFInfoContainerStatus = 0;
   trig_RoI_EF_mu_TrigMuonEFIsolationContainer = 0;
   trig_RoI_EF_mu_TrigMuonEFIsolationContainerStatus = 0;
   mcevt0_weight = 0;
   truthLP_pt = 0;
   truthLP_m = 0;
   truthLP_eta = 0;
   truthLP_phi = 0;
   truthLP_pdgId = 0;
   truthLP_charge = 0;
   truthLP_status = 0;
   truthLP_pdgId_parent = 0;
   truthTau_pt = 0;
   truthTau_m = 0;
   truthTau_eta = 0;
   truthTau_phi = 0;
   truthTau_pdgId = 0;
   truthTau_charge = 0;
   truthTau_status = 0;
   truthTau_pdgId_parent = 0;
   truthTau_isLeptonicDecay = 0;
   truthTau_nProng = 0;
   truthTau_vis_Et = 0;
   truthTau_vis_m = 0;
   truthTau_vis_eta = 0;
   truthTau_vis_phi = 0;
   truthTau_invis_Et = 0;
   truthTau_invis_m = 0;
   truthTau_invis_eta = 0;
   truthTau_invis_phi = 0;
   truthTau_child_n = 0;
   truthTau_child_pt = 0;
   truthTau_child_m = 0;
   truthTau_child_eta = 0;
   truthTau_child_phi = 0;
   truthTau_child_pdgId = 0;
   truthTau_child_charge = 0;
   truthTau_child_status = 0;
   truthH_pt = 0;
   truthH_m = 0;
   truthH_eta = 0;
   truthH_phi = 0;
   truthH_pdgId = 0;
   truthH_charge = 0;
   truthH_status = 0;
   truthH_child_n = 0;
   truthH_child_pt = 0;
   truthH_child_m = 0;
   truthH_child_eta = 0;
   truthH_child_phi = 0;
   truthH_child_pdgId = 0;
   truthH_child_charge = 0;
   truthH_child_status = 0;
   truthZ_pt = 0;
   truthZ_m = 0;
   truthZ_eta = 0;
   truthZ_phi = 0;
   truthZ_pdgId = 0;
   truthZ_status = 0;
   truthZ_pdgId_parent = 0;
   truthZ_child_n = 0;
   truthZ_child_pt = 0;
   truthZ_child_m = 0;
   truthZ_child_eta = 0;
   truthZ_child_phi = 0;
   truthZ_child_pdgId = 0;
   truthZ_child_charge = 0;
   truthZ_child_status = 0;
   truthW_pt = 0;
   truthW_m = 0;
   truthW_eta = 0;
   truthW_phi = 0;
   truthW_pdgId = 0;
   truthW_charge = 0;
   truthW_status = 0;
   truthW_pdgId_parent = 0;
   truthW_child_n = 0;
   truthW_child_pt = 0;
   truthW_child_m = 0;
   truthW_child_eta = 0;
   truthW_child_phi = 0;
   truthW_child_pdgId = 0;
   truthW_child_charge = 0;
   truthW_child_status = 0;
   truthSpecial_pt = 0;
   truthSpecial_m = 0;
   truthSpecial_eta = 0;
   truthSpecial_phi = 0;
   truthSpecial_pdgId = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("EF_2e12Tvh_loose1", &EF_2e12Tvh_loose1, &b_EF_2e12Tvh_loose1);
   fChain->SetBranchAddress("EF_2e7T_loose1_mu6", &EF_2e7T_loose1_mu6, &b_EF_2e7T_loose1_mu6);
   fChain->SetBranchAddress("EF_2e7T_medium1_mu6", &EF_2e7T_medium1_mu6, &b_EF_2e7T_medium1_mu6);
   fChain->SetBranchAddress("EF_2mu13", &EF_2mu13, &b_EF_2mu13);
   fChain->SetBranchAddress("EF_2mu15", &EF_2mu15, &b_EF_2mu15);
   fChain->SetBranchAddress("EF_e12Tvh_medium1_mu8", &EF_e12Tvh_medium1_mu8, &b_EF_e12Tvh_medium1_mu8);
   fChain->SetBranchAddress("EF_e22vh_medium1", &EF_e22vh_medium1, &b_EF_e22vh_medium1);
   fChain->SetBranchAddress("EF_e22vhi_medium1", &EF_e22vhi_medium1, &b_EF_e22vhi_medium1);
   fChain->SetBranchAddress("EF_e24vh_medium1", &EF_e24vh_medium1, &b_EF_e24vh_medium1);
   fChain->SetBranchAddress("EF_e24vhi_medium1", &EF_e24vhi_medium1, &b_EF_e24vhi_medium1);
   fChain->SetBranchAddress("EF_e60_medium1", &EF_e60_medium1, &b_EF_e60_medium1);
   fChain->SetBranchAddress("EF_e7T_loose1_2mu6", &EF_e7T_loose1_2mu6, &b_EF_e7T_loose1_2mu6);
   fChain->SetBranchAddress("EF_e7T_medium1_2mu6", &EF_e7T_medium1_2mu6, &b_EF_e7T_medium1_2mu6);
   fChain->SetBranchAddress("EF_mu18_medium", &EF_mu18_medium, &b_EF_mu18_medium);
   fChain->SetBranchAddress("EF_mu18_tight", &EF_mu18_tight, &b_EF_mu18_tight);
   fChain->SetBranchAddress("EF_mu18_tight_2mu4_EFFS", &EF_mu18_tight_2mu4_EFFS, &b_EF_mu18_tight_2mu4_EFFS);
   fChain->SetBranchAddress("EF_mu18_tight_e7_medium1", &EF_mu18_tight_e7_medium1, &b_EF_mu18_tight_e7_medium1);
   fChain->SetBranchAddress("EF_mu18_tight_mu8_EFFS", &EF_mu18_tight_mu8_EFFS, &b_EF_mu18_tight_mu8_EFFS);
   fChain->SetBranchAddress("EF_mu24_medium", &EF_mu24_medium, &b_EF_mu24_medium);
   fChain->SetBranchAddress("EF_mu24_tight", &EF_mu24_tight, &b_EF_mu24_tight);
   fChain->SetBranchAddress("EF_mu24_tight_mu6_EFFS", &EF_mu24_tight_mu6_EFFS, &b_EF_mu24_tight_mu6_EFFS);
   fChain->SetBranchAddress("EF_mu24i_tight", &EF_mu24i_tight, &b_EF_mu24i_tight);
   fChain->SetBranchAddress("EF_mu36_tight", &EF_mu36_tight, &b_EF_mu36_tight);
   fChain->SetBranchAddress("EF_mu40_MSonly_barrel_tight", &EF_mu40_MSonly_barrel_tight, &b_EF_mu40_MSonly_barrel_tight);
   fChain->SetBranchAddress("EF_tau20T_medium1_e15vh_medium1", &EF_tau20T_medium1_e15vh_medium1, &b_EF_tau20T_medium1_e15vh_medium1);
   fChain->SetBranchAddress("EF_tau20T_medium_mu15", &EF_tau20T_medium_mu15, &b_EF_tau20T_medium_mu15);
   fChain->SetBranchAddress("EF_tau20_medium1_mu15", &EF_tau20_medium1_mu15, &b_EF_tau20_medium1_mu15);
   fChain->SetBranchAddress("trig_EF_tau_EF_tau20T_medium1_e15vh_medium1", &trig_EF_tau_EF_tau20T_medium1_e15vh_medium1, &b_trig_EF_tau_EF_tau20T_medium1_e15vh_medium1);
   fChain->SetBranchAddress("trig_EF_tau_EF_tau20T_medium_mu15", &trig_EF_tau_EF_tau20T_medium_mu15, &b_trig_EF_tau_EF_tau20T_medium_mu15);
   fChain->SetBranchAddress("trig_EF_tau_EF_tau20_medium1_mu15", &trig_EF_tau_EF_tau20_medium1_mu15, &b_trig_EF_tau_EF_tau20_medium1_mu15);
   fChain->SetBranchAddress("trig_L2_combmuonfeature_L2_2mu13", &trig_L2_combmuonfeature_L2_2mu13, &b_trig_L2_combmuonfeature_L2_2mu13);
   fChain->SetBranchAddress("trig_L2_combmuonfeature_L2_2mu15", &trig_L2_combmuonfeature_L2_2mu15, &b_trig_L2_combmuonfeature_L2_2mu15);
   fChain->SetBranchAddress("trig_L2_combmuonfeature_L2_2mu6", &trig_L2_combmuonfeature_L2_2mu6, &b_trig_L2_combmuonfeature_L2_2mu6);
   fChain->SetBranchAddress("trig_L2_combmuonfeature_L2_mu15", &trig_L2_combmuonfeature_L2_mu15, &b_trig_L2_combmuonfeature_L2_mu15);
   fChain->SetBranchAddress("trig_L2_combmuonfeature_L2_mu18_medium", &trig_L2_combmuonfeature_L2_mu18_medium, &b_trig_L2_combmuonfeature_L2_mu18_medium);
   fChain->SetBranchAddress("trig_L2_combmuonfeature_L2_mu18_tight", &trig_L2_combmuonfeature_L2_mu18_tight, &b_trig_L2_combmuonfeature_L2_mu18_tight);
   fChain->SetBranchAddress("trig_L2_combmuonfeature_L2_mu18_tight_e7_medium1", &trig_L2_combmuonfeature_L2_mu18_tight_e7_medium1, &b_trig_L2_combmuonfeature_L2_mu18_tight_e7_medium1);
   fChain->SetBranchAddress("trig_L2_combmuonfeature_L2_mu24_medium", &trig_L2_combmuonfeature_L2_mu24_medium, &b_trig_L2_combmuonfeature_L2_mu24_medium);
   fChain->SetBranchAddress("trig_L2_combmuonfeature_L2_mu24_tight", &trig_L2_combmuonfeature_L2_mu24_tight, &b_trig_L2_combmuonfeature_L2_mu24_tight);
   fChain->SetBranchAddress("trig_L2_combmuonfeature_L2_mu36_tight", &trig_L2_combmuonfeature_L2_mu36_tight, &b_trig_L2_combmuonfeature_L2_mu36_tight);
   fChain->SetBranchAddress("trig_L2_combmuonfeature_L2_mu6", &trig_L2_combmuonfeature_L2_mu6, &b_trig_L2_combmuonfeature_L2_mu6);
   fChain->SetBranchAddress("trig_L2_combmuonfeature_L2_mu8", &trig_L2_combmuonfeature_L2_mu8, &b_trig_L2_combmuonfeature_L2_mu8);
   fChain->SetBranchAddress("trig_EF_trigmuonef_EF_2mu13", &trig_EF_trigmuonef_EF_2mu13, &b_trig_EF_trigmuonef_EF_2mu13);
   fChain->SetBranchAddress("trig_EF_trigmuonef_EF_2mu15", &trig_EF_trigmuonef_EF_2mu15, &b_trig_EF_trigmuonef_EF_2mu15);
   fChain->SetBranchAddress("trig_EF_trigmuonef_EF_2mu6", &trig_EF_trigmuonef_EF_2mu6, &b_trig_EF_trigmuonef_EF_2mu6);
   fChain->SetBranchAddress("trig_EF_trigmuonef_EF_mu15", &trig_EF_trigmuonef_EF_mu15, &b_trig_EF_trigmuonef_EF_mu15);
   fChain->SetBranchAddress("trig_EF_trigmuonef_EF_mu18_medium", &trig_EF_trigmuonef_EF_mu18_medium, &b_trig_EF_trigmuonef_EF_mu18_medium);
   fChain->SetBranchAddress("trig_EF_trigmuonef_EF_mu18_tight", &trig_EF_trigmuonef_EF_mu18_tight, &b_trig_EF_trigmuonef_EF_mu18_tight);
   fChain->SetBranchAddress("trig_EF_trigmuonef_EF_mu18_tight_2mu4_EFFS", &trig_EF_trigmuonef_EF_mu18_tight_2mu4_EFFS, &b_trig_EF_trigmuonef_EF_mu18_tight_2mu4_EFFS);
   fChain->SetBranchAddress("trig_EF_trigmuonef_EF_mu18_tight_e7_medium1", &trig_EF_trigmuonef_EF_mu18_tight_e7_medium1, &b_trig_EF_trigmuonef_EF_mu18_tight_e7_medium1);
   fChain->SetBranchAddress("trig_EF_trigmuonef_EF_mu18_tight_mu8_EFFS", &trig_EF_trigmuonef_EF_mu18_tight_mu8_EFFS, &b_trig_EF_trigmuonef_EF_mu18_tight_mu8_EFFS);
   fChain->SetBranchAddress("trig_EF_trigmuonef_EF_mu24_medium", &trig_EF_trigmuonef_EF_mu24_medium, &b_trig_EF_trigmuonef_EF_mu24_medium);
   fChain->SetBranchAddress("trig_EF_trigmuonef_EF_mu24_tight", &trig_EF_trigmuonef_EF_mu24_tight, &b_trig_EF_trigmuonef_EF_mu24_tight);
   fChain->SetBranchAddress("trig_EF_trigmuonef_EF_mu24_tight_mu6_EFFS", &trig_EF_trigmuonef_EF_mu24_tight_mu6_EFFS, &b_trig_EF_trigmuonef_EF_mu24_tight_mu6_EFFS);
   fChain->SetBranchAddress("trig_EF_trigmuonef_EF_mu24i_tight", &trig_EF_trigmuonef_EF_mu24i_tight, &b_trig_EF_trigmuonef_EF_mu24i_tight);
   fChain->SetBranchAddress("trig_EF_trigmuonef_EF_mu36_tight", &trig_EF_trigmuonef_EF_mu36_tight, &b_trig_EF_trigmuonef_EF_mu36_tight);
   fChain->SetBranchAddress("trig_EF_trigmuonef_EF_mu40_MSonly_barrel_tight", &trig_EF_trigmuonef_EF_mu40_MSonly_barrel_tight, &b_trig_EF_trigmuonef_EF_mu40_MSonly_barrel_tight);
   fChain->SetBranchAddress("trig_EF_trigmuonef_EF_mu6", &trig_EF_trigmuonef_EF_mu6, &b_trig_EF_trigmuonef_EF_mu6);
   fChain->SetBranchAddress("trig_EF_trigmuonef_EF_mu8", &trig_EF_trigmuonef_EF_mu8, &b_trig_EF_trigmuonef_EF_mu8);
   fChain->SetBranchAddress("trig_EF_trigmugirl_EF_2mu13", &trig_EF_trigmugirl_EF_2mu13, &b_trig_EF_trigmugirl_EF_2mu13);
   fChain->SetBranchAddress("trig_EF_trigmugirl_EF_2mu15", &trig_EF_trigmugirl_EF_2mu15, &b_trig_EF_trigmugirl_EF_2mu15);
   fChain->SetBranchAddress("trig_EF_trigmugirl_EF_2mu6", &trig_EF_trigmugirl_EF_2mu6, &b_trig_EF_trigmugirl_EF_2mu6);
   fChain->SetBranchAddress("trig_EF_trigmugirl_EF_mu15", &trig_EF_trigmugirl_EF_mu15, &b_trig_EF_trigmugirl_EF_mu15);
   fChain->SetBranchAddress("trig_EF_trigmugirl_EF_mu18_medium", &trig_EF_trigmugirl_EF_mu18_medium, &b_trig_EF_trigmugirl_EF_mu18_medium);
   fChain->SetBranchAddress("trig_EF_trigmugirl_EF_mu18_tight", &trig_EF_trigmugirl_EF_mu18_tight, &b_trig_EF_trigmugirl_EF_mu18_tight);
   fChain->SetBranchAddress("trig_EF_trigmugirl_EF_mu18_tight_2mu4_EFFS", &trig_EF_trigmugirl_EF_mu18_tight_2mu4_EFFS, &b_trig_EF_trigmugirl_EF_mu18_tight_2mu4_EFFS);
   fChain->SetBranchAddress("trig_EF_trigmugirl_EF_mu18_tight_e7_medium1", &trig_EF_trigmugirl_EF_mu18_tight_e7_medium1, &b_trig_EF_trigmugirl_EF_mu18_tight_e7_medium1);
   fChain->SetBranchAddress("trig_EF_trigmugirl_EF_mu18_tight_mu8_EFFS", &trig_EF_trigmugirl_EF_mu18_tight_mu8_EFFS, &b_trig_EF_trigmugirl_EF_mu18_tight_mu8_EFFS);
   fChain->SetBranchAddress("trig_EF_trigmugirl_EF_mu24_medium", &trig_EF_trigmugirl_EF_mu24_medium, &b_trig_EF_trigmugirl_EF_mu24_medium);
   fChain->SetBranchAddress("trig_EF_trigmugirl_EF_mu24_tight", &trig_EF_trigmugirl_EF_mu24_tight, &b_trig_EF_trigmugirl_EF_mu24_tight);
   fChain->SetBranchAddress("trig_EF_trigmugirl_EF_mu24_tight_mu6_EFFS", &trig_EF_trigmugirl_EF_mu24_tight_mu6_EFFS, &b_trig_EF_trigmugirl_EF_mu24_tight_mu6_EFFS);
   fChain->SetBranchAddress("trig_EF_trigmugirl_EF_mu24i_tight", &trig_EF_trigmugirl_EF_mu24i_tight, &b_trig_EF_trigmugirl_EF_mu24i_tight);
   fChain->SetBranchAddress("trig_EF_trigmugirl_EF_mu36_tight", &trig_EF_trigmugirl_EF_mu36_tight, &b_trig_EF_trigmugirl_EF_mu36_tight);
   fChain->SetBranchAddress("trig_EF_trigmugirl_EF_mu40_MSonly_barrel_tight", &trig_EF_trigmugirl_EF_mu40_MSonly_barrel_tight, &b_trig_EF_trigmugirl_EF_mu40_MSonly_barrel_tight);
   fChain->SetBranchAddress("trig_EF_trigmugirl_EF_mu6", &trig_EF_trigmugirl_EF_mu6, &b_trig_EF_trigmugirl_EF_mu6);
   fChain->SetBranchAddress("trig_EF_trigmugirl_EF_mu8", &trig_EF_trigmugirl_EF_mu8, &b_trig_EF_trigmugirl_EF_mu8);
   fChain->SetBranchAddress("tau_n", &tau_n, &b_tau_n);
   fChain->SetBranchAddress("tau_Et", &tau_Et, &b_tau_Et);
   fChain->SetBranchAddress("tau_pt", &tau_pt, &b_tau_pt);
   fChain->SetBranchAddress("tau_m", &tau_m, &b_tau_m);
   fChain->SetBranchAddress("tau_eta", &tau_eta, &b_tau_eta);
   fChain->SetBranchAddress("tau_phi", &tau_phi, &b_tau_phi);
   fChain->SetBranchAddress("tau_charge", &tau_charge, &b_tau_charge);
   fChain->SetBranchAddress("tau_BDTEleScore", &tau_BDTEleScore, &b_tau_BDTEleScore);
   fChain->SetBranchAddress("tau_BDTJetScore", &tau_BDTJetScore, &b_tau_BDTJetScore);
   fChain->SetBranchAddress("tau_likelihood", &tau_likelihood, &b_tau_likelihood);
   fChain->SetBranchAddress("tau_SafeLikelihood", &tau_SafeLikelihood, &b_tau_SafeLikelihood);
   fChain->SetBranchAddress("tau_electronVetoLoose", &tau_electronVetoLoose, &b_tau_electronVetoLoose);
   fChain->SetBranchAddress("tau_electronVetoMedium", &tau_electronVetoMedium, &b_tau_electronVetoMedium);
   fChain->SetBranchAddress("tau_electronVetoTight", &tau_electronVetoTight, &b_tau_electronVetoTight);
   fChain->SetBranchAddress("tau_muonVeto", &tau_muonVeto, &b_tau_muonVeto);
   fChain->SetBranchAddress("tau_tauLlhLoose", &tau_tauLlhLoose, &b_tau_tauLlhLoose);
   fChain->SetBranchAddress("tau_tauLlhMedium", &tau_tauLlhMedium, &b_tau_tauLlhMedium);
   fChain->SetBranchAddress("tau_tauLlhTight", &tau_tauLlhTight, &b_tau_tauLlhTight);
   fChain->SetBranchAddress("tau_JetBDTSigLoose", &tau_JetBDTSigLoose, &b_tau_JetBDTSigLoose);
   fChain->SetBranchAddress("tau_JetBDTSigMedium", &tau_JetBDTSigMedium, &b_tau_JetBDTSigMedium);
   fChain->SetBranchAddress("tau_JetBDTSigTight", &tau_JetBDTSigTight, &b_tau_JetBDTSigTight);
   fChain->SetBranchAddress("tau_EleBDTLoose", &tau_EleBDTLoose, &b_tau_EleBDTLoose);
   fChain->SetBranchAddress("tau_EleBDTMedium", &tau_EleBDTMedium, &b_tau_EleBDTMedium);
   fChain->SetBranchAddress("tau_EleBDTTight", &tau_EleBDTTight, &b_tau_EleBDTTight);
   fChain->SetBranchAddress("tau_author", &tau_author, &b_tau_author);
   fChain->SetBranchAddress("tau_numTrack", &tau_numTrack, &b_tau_numTrack);
   fChain->SetBranchAddress("tau_etOverPtLeadTrk", &tau_etOverPtLeadTrk, &b_tau_etOverPtLeadTrk);
   fChain->SetBranchAddress("tau_leadTrkPt", &tau_leadTrkPt, &b_tau_leadTrkPt);
   fChain->SetBranchAddress("tau_jet_jvtxf", &tau_jet_jvtxf, &b_tau_jet_jvtxf);
   fChain->SetBranchAddress("tau_track_n", &tau_track_n, &b_tau_track_n);
   fChain->SetBranchAddress("tau_track_d0", &tau_track_d0, &b_tau_track_d0);
   fChain->SetBranchAddress("tau_track_z0", &tau_track_z0, &b_tau_track_z0);
   fChain->SetBranchAddress("tau_track_phi", &tau_track_phi, &b_tau_track_phi);
   fChain->SetBranchAddress("tau_track_theta", &tau_track_theta, &b_tau_track_theta);
   fChain->SetBranchAddress("tau_track_qoverp", &tau_track_qoverp, &b_tau_track_qoverp);
   fChain->SetBranchAddress("tau_track_pt", &tau_track_pt, &b_tau_track_pt);
   fChain->SetBranchAddress("tau_track_eta", &tau_track_eta, &b_tau_track_eta);
   fChain->SetBranchAddress("tau_track_atPV_d0", &tau_track_atPV_d0, &b_tau_track_atPV_d0);
   fChain->SetBranchAddress("tau_track_atPV_z0", &tau_track_atPV_z0, &b_tau_track_atPV_z0);
   fChain->SetBranchAddress("tau_track_atPV_phi", &tau_track_atPV_phi, &b_tau_track_atPV_phi);
   fChain->SetBranchAddress("tau_track_atPV_theta", &tau_track_atPV_theta, &b_tau_track_atPV_theta);
   fChain->SetBranchAddress("tau_track_atPV_qoverp", &tau_track_atPV_qoverp, &b_tau_track_atPV_qoverp);
   fChain->SetBranchAddress("tau_track_atPV_pt", &tau_track_atPV_pt, &b_tau_track_atPV_pt);
   fChain->SetBranchAddress("tau_track_atPV_eta", &tau_track_atPV_eta, &b_tau_track_atPV_eta);
   fChain->SetBranchAddress("mc_n", &mc_n, &b_mc_n);
   fChain->SetBranchAddress("mc_pt", &mc_pt, &b_mc_pt);
   fChain->SetBranchAddress("mc_m", &mc_m, &b_mc_m);
   fChain->SetBranchAddress("mc_eta", &mc_eta, &b_mc_eta);
   fChain->SetBranchAddress("mc_phi", &mc_phi, &b_mc_phi);
   fChain->SetBranchAddress("mc_status", &mc_status, &b_mc_status);
   fChain->SetBranchAddress("mc_barcode", &mc_barcode, &b_mc_barcode);
   fChain->SetBranchAddress("mc_pdgId", &mc_pdgId, &b_mc_pdgId);
   fChain->SetBranchAddress("mc_charge", &mc_charge, &b_mc_charge);
   fChain->SetBranchAddress("mc_child_index", &mc_child_index, &b_mc_child_index);
   fChain->SetBranchAddress("mc_parent_index", &mc_parent_index, &b_mc_parent_index);
   fChain->SetBranchAddress("jet_antikt4truth_n", &jet_antikt4truth_n, &b_jet_antikt4truth_n);
   fChain->SetBranchAddress("jet_antikt4truth_E", &jet_antikt4truth_E, &b_jet_antikt4truth_E);
   fChain->SetBranchAddress("jet_antikt4truth_pt", &jet_antikt4truth_pt, &b_jet_antikt4truth_pt);
   fChain->SetBranchAddress("jet_antikt4truth_m", &jet_antikt4truth_m, &b_jet_antikt4truth_m);
   fChain->SetBranchAddress("jet_antikt4truth_eta", &jet_antikt4truth_eta, &b_jet_antikt4truth_eta);
   fChain->SetBranchAddress("jet_antikt4truth_phi", &jet_antikt4truth_phi, &b_jet_antikt4truth_phi);
   fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   fChain->SetBranchAddress("el_author", &el_author, &b_el_author);
   fChain->SetBranchAddress("el_isEM", &el_isEM, &b_el_isEM);
   fChain->SetBranchAddress("el_OQ", &el_OQ, &b_el_OQ);
   fChain->SetBranchAddress("el_loose", &el_loose, &b_el_loose);
   fChain->SetBranchAddress("el_medium", &el_medium, &b_el_medium);
   fChain->SetBranchAddress("el_tight", &el_tight, &b_el_tight);
   fChain->SetBranchAddress("el_loosePP", &el_loosePP, &b_el_loosePP);
   fChain->SetBranchAddress("el_mediumPP", &el_mediumPP, &b_el_mediumPP);
   fChain->SetBranchAddress("el_tightPP", &el_tightPP, &b_el_tightPP);
   fChain->SetBranchAddress("el_Ethad", &el_Ethad, &b_el_Ethad);
   fChain->SetBranchAddress("el_Ethad1", &el_Ethad1, &b_el_Ethad1);
   fChain->SetBranchAddress("el_f1", &el_f1, &b_el_f1);
   fChain->SetBranchAddress("el_Emax2", &el_Emax2, &b_el_Emax2);
   fChain->SetBranchAddress("el_wstot", &el_wstot, &b_el_wstot);
   fChain->SetBranchAddress("el_emaxs1", &el_emaxs1, &b_el_emaxs1);
   fChain->SetBranchAddress("el_E237", &el_E237, &b_el_E237);
   fChain->SetBranchAddress("el_E277", &el_E277, &b_el_E277);
   fChain->SetBranchAddress("el_weta2", &el_weta2, &b_el_weta2);
   fChain->SetBranchAddress("el_f3", &el_f3, &b_el_f3);
   fChain->SetBranchAddress("el_Etcone20", &el_Etcone20, &b_el_Etcone20);
   fChain->SetBranchAddress("el_Etcone30", &el_Etcone30, &b_el_Etcone30);
   fChain->SetBranchAddress("el_Etcone40", &el_Etcone40, &b_el_Etcone40);
   fChain->SetBranchAddress("el_ptcone20", &el_ptcone20, &b_el_ptcone20);
   fChain->SetBranchAddress("el_ptcone30", &el_ptcone30, &b_el_ptcone30);
   fChain->SetBranchAddress("el_ptcone40", &el_ptcone40, &b_el_ptcone40);
   fChain->SetBranchAddress("el_deltaeta1", &el_deltaeta1, &b_el_deltaeta1);
   fChain->SetBranchAddress("el_deltaphi2", &el_deltaphi2, &b_el_deltaphi2);
   fChain->SetBranchAddress("el_expectHitInBLayer", &el_expectHitInBLayer, &b_el_expectHitInBLayer);
   fChain->SetBranchAddress("el_trackd0_physics", &el_trackd0_physics, &b_el_trackd0_physics);
   fChain->SetBranchAddress("el_reta", &el_reta, &b_el_reta);
   fChain->SetBranchAddress("el_topoEtcone20", &el_topoEtcone20, &b_el_topoEtcone20);
   fChain->SetBranchAddress("el_topoEtcone30", &el_topoEtcone30, &b_el_topoEtcone30);
   fChain->SetBranchAddress("el_topoEtcone40", &el_topoEtcone40, &b_el_topoEtcone40);
   fChain->SetBranchAddress("el_etap", &el_etap, &b_el_etap);
   fChain->SetBranchAddress("el_Es2", &el_Es2, &b_el_Es2);
   fChain->SetBranchAddress("el_etas2", &el_etas2, &b_el_etas2);
   fChain->SetBranchAddress("el_phis2", &el_phis2, &b_el_phis2);
   fChain->SetBranchAddress("el_cl_E", &el_cl_E, &b_el_cl_E);
   fChain->SetBranchAddress("el_cl_pt", &el_cl_pt, &b_el_cl_pt);
   fChain->SetBranchAddress("el_cl_eta", &el_cl_eta, &b_el_cl_eta);
   fChain->SetBranchAddress("el_cl_phi", &el_cl_phi, &b_el_cl_phi);
   fChain->SetBranchAddress("el_trackphi", &el_trackphi, &b_el_trackphi);
   fChain->SetBranchAddress("el_trackqoverp", &el_trackqoverp, &b_el_trackqoverp);
   fChain->SetBranchAddress("el_trackpt", &el_trackpt, &b_el_trackpt);
   fChain->SetBranchAddress("el_tracketa", &el_tracketa, &b_el_tracketa);
   fChain->SetBranchAddress("el_nBLHits", &el_nBLHits, &b_el_nBLHits);
   fChain->SetBranchAddress("el_nPixHits", &el_nPixHits, &b_el_nPixHits);
   fChain->SetBranchAddress("el_nSCTHits", &el_nSCTHits, &b_el_nSCTHits);
   fChain->SetBranchAddress("el_nTRTHits", &el_nTRTHits, &b_el_nTRTHits);
   fChain->SetBranchAddress("el_nBLayerOutliers", &el_nBLayerOutliers, &b_el_nBLayerOutliers);
   fChain->SetBranchAddress("el_nPixelOutliers", &el_nPixelOutliers, &b_el_nPixelOutliers);
   fChain->SetBranchAddress("el_nSCTOutliers", &el_nSCTOutliers, &b_el_nSCTOutliers);
   fChain->SetBranchAddress("el_nTRTOutliers", &el_nTRTOutliers, &b_el_nTRTOutliers);
   fChain->SetBranchAddress("el_nSiHits", &el_nSiHits, &b_el_nSiHits);
   fChain->SetBranchAddress("el_TRTHighTOutliersRatio", &el_TRTHighTOutliersRatio, &b_el_TRTHighTOutliersRatio);
   fChain->SetBranchAddress("el_trackd0pv", &el_trackd0pv, &b_el_trackd0pv);
   fChain->SetBranchAddress("el_trackz0pv", &el_trackz0pv, &b_el_trackz0pv);
   fChain->SetBranchAddress("el_tracksigd0pv", &el_tracksigd0pv, &b_el_tracksigd0pv);
   fChain->SetBranchAddress("el_tracksigz0pv", &el_tracksigz0pv, &b_el_tracksigz0pv);
   fChain->SetBranchAddress("el_trackd0pvunbiased", &el_trackd0pvunbiased, &b_el_trackd0pvunbiased);
   fChain->SetBranchAddress("el_trackz0pvunbiased", &el_trackz0pvunbiased, &b_el_trackz0pvunbiased);
   fChain->SetBranchAddress("el_tracksigd0pvunbiased", &el_tracksigd0pvunbiased, &b_el_tracksigd0pvunbiased);
   fChain->SetBranchAddress("el_tracksigz0pvunbiased", &el_tracksigz0pvunbiased, &b_el_tracksigz0pvunbiased);
   fChain->SetBranchAddress("el_topoEtcone20_corrected", &el_topoEtcone20_corrected, &b_el_topoEtcone20_corrected);
   fChain->SetBranchAddress("el_topoEtcone30_corrected", &el_topoEtcone30_corrected, &b_el_topoEtcone30_corrected);
   fChain->SetBranchAddress("el_topoEtcone40_corrected", &el_topoEtcone40_corrected, &b_el_topoEtcone40_corrected);
   fChain->SetBranchAddress("el_ED_median", &el_ED_median, &b_el_ED_median);
   fChain->SetBranchAddress("jet_n", &jet_n, &b_jet_n);
   fChain->SetBranchAddress("jet_E", &jet_E, &b_jet_E);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_m", &jet_m, &b_jet_m);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_EtaOrigin", &jet_EtaOrigin, &b_jet_EtaOrigin);
   fChain->SetBranchAddress("jet_PhiOrigin", &jet_PhiOrigin, &b_jet_PhiOrigin);
   fChain->SetBranchAddress("jet_MOrigin", &jet_MOrigin, &b_jet_MOrigin);
   fChain->SetBranchAddress("jet_Timing", &jet_Timing, &b_jet_Timing);
   fChain->SetBranchAddress("jet_LArQuality", &jet_LArQuality, &b_jet_LArQuality);
   fChain->SetBranchAddress("jet_sumPtTrk", &jet_sumPtTrk, &b_jet_sumPtTrk);
   fChain->SetBranchAddress("jet_HECQuality", &jet_HECQuality, &b_jet_HECQuality);
   fChain->SetBranchAddress("jet_NegativeE", &jet_NegativeE, &b_jet_NegativeE);
   fChain->SetBranchAddress("jet_AverageLArQF", &jet_AverageLArQF, &b_jet_AverageLArQF);
   fChain->SetBranchAddress("jet_BCH_CORR_CELL", &jet_BCH_CORR_CELL, &b_jet_BCH_CORR_CELL);
   fChain->SetBranchAddress("jet_BCH_CORR_DOTX", &jet_BCH_CORR_DOTX, &b_jet_BCH_CORR_DOTX);
   fChain->SetBranchAddress("jet_BCH_CORR_JET", &jet_BCH_CORR_JET, &b_jet_BCH_CORR_JET);
   fChain->SetBranchAddress("jet_SamplingMax", &jet_SamplingMax, &b_jet_SamplingMax);
   fChain->SetBranchAddress("jet_fracSamplingMax", &jet_fracSamplingMax, &b_jet_fracSamplingMax);
   fChain->SetBranchAddress("jet_hecf", &jet_hecf, &b_jet_hecf);
   fChain->SetBranchAddress("jet_isUgly", &jet_isUgly, &b_jet_isUgly);
   fChain->SetBranchAddress("jet_isBadLooseMinus", &jet_isBadLooseMinus, &b_jet_isBadLooseMinus);
   fChain->SetBranchAddress("jet_isBadLoose", &jet_isBadLoose, &b_jet_isBadLoose);
   fChain->SetBranchAddress("jet_isBadMedium", &jet_isBadMedium, &b_jet_isBadMedium);
   fChain->SetBranchAddress("jet_isBadTight", &jet_isBadTight, &b_jet_isBadTight);
   fChain->SetBranchAddress("jet_emfrac", &jet_emfrac, &b_jet_emfrac);
   fChain->SetBranchAddress("jet_Offset", &jet_Offset, &b_jet_Offset);
   fChain->SetBranchAddress("jet_EMJES", &jet_EMJES, &b_jet_EMJES);
   fChain->SetBranchAddress("jet_EMJES_EtaCorr", &jet_EMJES_EtaCorr, &b_jet_EMJES_EtaCorr);
   fChain->SetBranchAddress("jet_EMJESnooffset", &jet_EMJESnooffset, &b_jet_EMJESnooffset);
   fChain->SetBranchAddress("jet_emscale_E", &jet_emscale_E, &b_jet_emscale_E);
   fChain->SetBranchAddress("jet_emscale_pt", &jet_emscale_pt, &b_jet_emscale_pt);
   fChain->SetBranchAddress("jet_emscale_m", &jet_emscale_m, &b_jet_emscale_m);
   fChain->SetBranchAddress("jet_emscale_eta", &jet_emscale_eta, &b_jet_emscale_eta);
   fChain->SetBranchAddress("jet_emscale_phi", &jet_emscale_phi, &b_jet_emscale_phi);
   fChain->SetBranchAddress("jet_ActiveArea", &jet_ActiveArea, &b_jet_ActiveArea);
   fChain->SetBranchAddress("jet_ActiveAreaPx", &jet_ActiveAreaPx, &b_jet_ActiveAreaPx);
   fChain->SetBranchAddress("jet_ActiveAreaPy", &jet_ActiveAreaPy, &b_jet_ActiveAreaPy);
   fChain->SetBranchAddress("jet_ActiveAreaPz", &jet_ActiveAreaPz, &b_jet_ActiveAreaPz);
   fChain->SetBranchAddress("jet_ActiveAreaE", &jet_ActiveAreaE, &b_jet_ActiveAreaE);
   fChain->SetBranchAddress("jet_jvtxf", &jet_jvtxf, &b_jet_jvtxf);
   fChain->SetBranchAddress("jet_jvtxfFull", &jet_jvtxfFull, &b_jet_jvtxfFull);
   fChain->SetBranchAddress("jet_jvtx_x", &jet_jvtx_x, &b_jet_jvtx_x);
   fChain->SetBranchAddress("jet_jvtx_y", &jet_jvtx_y, &b_jet_jvtx_y);
   fChain->SetBranchAddress("jet_jvtx_z", &jet_jvtx_z, &b_jet_jvtx_z);
   fChain->SetBranchAddress("jet_constscale_E", &jet_constscale_E, &b_jet_constscale_E);
   fChain->SetBranchAddress("jet_constscale_pt", &jet_constscale_pt, &b_jet_constscale_pt);
   fChain->SetBranchAddress("jet_constscale_m", &jet_constscale_m, &b_jet_constscale_m);
   fChain->SetBranchAddress("jet_constscale_eta", &jet_constscale_eta, &b_jet_constscale_eta);
   fChain->SetBranchAddress("jet_constscale_phi", &jet_constscale_phi, &b_jet_constscale_phi);
   fChain->SetBranchAddress("jet_flavor_weight_Comb", &jet_flavor_weight_Comb, &b_jet_flavor_weight_Comb);
   fChain->SetBranchAddress("jet_flavor_weight_IP2D", &jet_flavor_weight_IP2D, &b_jet_flavor_weight_IP2D);
   fChain->SetBranchAddress("jet_flavor_weight_IP3D", &jet_flavor_weight_IP3D, &b_jet_flavor_weight_IP3D);
   fChain->SetBranchAddress("jet_flavor_weight_SV0", &jet_flavor_weight_SV0, &b_jet_flavor_weight_SV0);
   fChain->SetBranchAddress("jet_flavor_weight_SV1", &jet_flavor_weight_SV1, &b_jet_flavor_weight_SV1);
   fChain->SetBranchAddress("jet_flavor_weight_SV2", &jet_flavor_weight_SV2, &b_jet_flavor_weight_SV2);
   fChain->SetBranchAddress("jet_flavor_weight_SoftMuonTagChi2", &jet_flavor_weight_SoftMuonTagChi2, &b_jet_flavor_weight_SoftMuonTagChi2);
   fChain->SetBranchAddress("jet_flavor_weight_SecondSoftMuonTagChi2", &jet_flavor_weight_SecondSoftMuonTagChi2, &b_jet_flavor_weight_SecondSoftMuonTagChi2);
   fChain->SetBranchAddress("jet_flavor_weight_JetFitterTagNN", &jet_flavor_weight_JetFitterTagNN, &b_jet_flavor_weight_JetFitterTagNN);
   fChain->SetBranchAddress("jet_flavor_weight_JetFitterCOMBNN", &jet_flavor_weight_JetFitterCOMBNN, &b_jet_flavor_weight_JetFitterCOMBNN);
   fChain->SetBranchAddress("jet_flavor_weight_MV1", &jet_flavor_weight_MV1, &b_jet_flavor_weight_MV1);
   fChain->SetBranchAddress("jet_flavor_weight_MV2", &jet_flavor_weight_MV2, &b_jet_flavor_weight_MV2);
   fChain->SetBranchAddress("jet_flavor_weight_GbbNN", &jet_flavor_weight_GbbNN, &b_jet_flavor_weight_GbbNN);
   fChain->SetBranchAddress("jet_flavor_weight_JetFitterCharm", &jet_flavor_weight_JetFitterCharm, &b_jet_flavor_weight_JetFitterCharm);
   fChain->SetBranchAddress("jet_flavor_weight_MV3_bVSu", &jet_flavor_weight_MV3_bVSu, &b_jet_flavor_weight_MV3_bVSu);
   fChain->SetBranchAddress("jet_flavor_weight_MV3_bVSc", &jet_flavor_weight_MV3_bVSc, &b_jet_flavor_weight_MV3_bVSc);
   fChain->SetBranchAddress("jet_flavor_weight_MV3_cVSu", &jet_flavor_weight_MV3_cVSu, &b_jet_flavor_weight_MV3_cVSu);
   fChain->SetBranchAddress("jet_flavor_truth_label", &jet_flavor_truth_label, &b_jet_flavor_truth_label);
   fChain->SetBranchAddress("jet_flavor_truth_dRminToB", &jet_flavor_truth_dRminToB, &b_jet_flavor_truth_dRminToB);
   fChain->SetBranchAddress("jet_flavor_truth_dRminToC", &jet_flavor_truth_dRminToC, &b_jet_flavor_truth_dRminToC);
   fChain->SetBranchAddress("jet_flavor_truth_dRminToT", &jet_flavor_truth_dRminToT, &b_jet_flavor_truth_dRminToT);
   fChain->SetBranchAddress("jet_flavor_truth_BHadronpdg", &jet_flavor_truth_BHadronpdg, &b_jet_flavor_truth_BHadronpdg);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_n", &jet_AntiKt4TopoEM_n, &b_jet_AntiKt4TopoEM_n);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_E", &jet_AntiKt4TopoEM_E, &b_jet_AntiKt4TopoEM_E);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_pt", &jet_AntiKt4TopoEM_pt, &b_jet_AntiKt4TopoEM_pt);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_m", &jet_AntiKt4TopoEM_m, &b_jet_AntiKt4TopoEM_m);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_eta", &jet_AntiKt4TopoEM_eta, &b_jet_AntiKt4TopoEM_eta);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_phi", &jet_AntiKt4TopoEM_phi, &b_jet_AntiKt4TopoEM_phi);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_EtaOrigin", &jet_AntiKt4TopoEM_EtaOrigin, &b_jet_AntiKt4TopoEM_EtaOrigin);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_PhiOrigin", &jet_AntiKt4TopoEM_PhiOrigin, &b_jet_AntiKt4TopoEM_PhiOrigin);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_MOrigin", &jet_AntiKt4TopoEM_MOrigin, &b_jet_AntiKt4TopoEM_MOrigin);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_Timing", &jet_AntiKt4TopoEM_Timing, &b_jet_AntiKt4TopoEM_Timing);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_LArQuality", &jet_AntiKt4TopoEM_LArQuality, &b_jet_AntiKt4TopoEM_LArQuality);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_sumPtTrk", &jet_AntiKt4TopoEM_sumPtTrk, &b_jet_AntiKt4TopoEM_sumPtTrk);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_HECQuality", &jet_AntiKt4TopoEM_HECQuality, &b_jet_AntiKt4TopoEM_HECQuality);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_NegativeE", &jet_AntiKt4TopoEM_NegativeE, &b_jet_AntiKt4TopoEM_NegativeE);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_AverageLArQF", &jet_AntiKt4TopoEM_AverageLArQF, &b_jet_AntiKt4TopoEM_AverageLArQF);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_BCH_CORR_CELL", &jet_AntiKt4TopoEM_BCH_CORR_CELL, &b_jet_AntiKt4TopoEM_BCH_CORR_CELL);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_BCH_CORR_DOTX", &jet_AntiKt4TopoEM_BCH_CORR_DOTX, &b_jet_AntiKt4TopoEM_BCH_CORR_DOTX);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_BCH_CORR_JET", &jet_AntiKt4TopoEM_BCH_CORR_JET, &b_jet_AntiKt4TopoEM_BCH_CORR_JET);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_SamplingMax", &jet_AntiKt4TopoEM_SamplingMax, &b_jet_AntiKt4TopoEM_SamplingMax);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_fracSamplingMax", &jet_AntiKt4TopoEM_fracSamplingMax, &b_jet_AntiKt4TopoEM_fracSamplingMax);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_hecf", &jet_AntiKt4TopoEM_hecf, &b_jet_AntiKt4TopoEM_hecf);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_isUgly", &jet_AntiKt4TopoEM_isUgly, &b_jet_AntiKt4TopoEM_isUgly);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_isBadLooseMinus", &jet_AntiKt4TopoEM_isBadLooseMinus, &b_jet_AntiKt4TopoEM_isBadLooseMinus);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_isBadLoose", &jet_AntiKt4TopoEM_isBadLoose, &b_jet_AntiKt4TopoEM_isBadLoose);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_isBadMedium", &jet_AntiKt4TopoEM_isBadMedium, &b_jet_AntiKt4TopoEM_isBadMedium);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_isBadTight", &jet_AntiKt4TopoEM_isBadTight, &b_jet_AntiKt4TopoEM_isBadTight);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_emfrac", &jet_AntiKt4TopoEM_emfrac, &b_jet_AntiKt4TopoEM_emfrac);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_Offset", &jet_AntiKt4TopoEM_Offset, &b_jet_AntiKt4TopoEM_Offset);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_EMJES", &jet_AntiKt4TopoEM_EMJES, &b_jet_AntiKt4TopoEM_EMJES);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_EMJES_EtaCorr", &jet_AntiKt4TopoEM_EMJES_EtaCorr, &b_jet_AntiKt4TopoEM_EMJES_EtaCorr);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_EMJESnooffset", &jet_AntiKt4TopoEM_EMJESnooffset, &b_jet_AntiKt4TopoEM_EMJESnooffset);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_emscale_E", &jet_AntiKt4TopoEM_emscale_E, &b_jet_AntiKt4TopoEM_emscale_E);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_emscale_pt", &jet_AntiKt4TopoEM_emscale_pt, &b_jet_AntiKt4TopoEM_emscale_pt);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_emscale_m", &jet_AntiKt4TopoEM_emscale_m, &b_jet_AntiKt4TopoEM_emscale_m);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_emscale_eta", &jet_AntiKt4TopoEM_emscale_eta, &b_jet_AntiKt4TopoEM_emscale_eta);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_emscale_phi", &jet_AntiKt4TopoEM_emscale_phi, &b_jet_AntiKt4TopoEM_emscale_phi);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_ActiveArea", &jet_AntiKt4TopoEM_ActiveArea, &b_jet_AntiKt4TopoEM_ActiveArea);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_ActiveAreaPx", &jet_AntiKt4TopoEM_ActiveAreaPx, &b_jet_AntiKt4TopoEM_ActiveAreaPx);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_ActiveAreaPy", &jet_AntiKt4TopoEM_ActiveAreaPy, &b_jet_AntiKt4TopoEM_ActiveAreaPy);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_ActiveAreaPz", &jet_AntiKt4TopoEM_ActiveAreaPz, &b_jet_AntiKt4TopoEM_ActiveAreaPz);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_ActiveAreaE", &jet_AntiKt4TopoEM_ActiveAreaE, &b_jet_AntiKt4TopoEM_ActiveAreaE);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_jvtxf", &jet_AntiKt4TopoEM_jvtxf, &b_jet_AntiKt4TopoEM_jvtxf);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_jvtxfFull", &jet_AntiKt4TopoEM_jvtxfFull, &b_jet_AntiKt4TopoEM_jvtxfFull);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_jvtx_x", &jet_AntiKt4TopoEM_jvtx_x, &b_jet_AntiKt4TopoEM_jvtx_x);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_jvtx_y", &jet_AntiKt4TopoEM_jvtx_y, &b_jet_AntiKt4TopoEM_jvtx_y);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_jvtx_z", &jet_AntiKt4TopoEM_jvtx_z, &b_jet_AntiKt4TopoEM_jvtx_z);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_Comb", &jet_AntiKt4TopoEM_flavor_weight_Comb, &b_jet_AntiKt4TopoEM_flavor_weight_Comb);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_IP2D", &jet_AntiKt4TopoEM_flavor_weight_IP2D, &b_jet_AntiKt4TopoEM_flavor_weight_IP2D);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_IP3D", &jet_AntiKt4TopoEM_flavor_weight_IP3D, &b_jet_AntiKt4TopoEM_flavor_weight_IP3D);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_SV0", &jet_AntiKt4TopoEM_flavor_weight_SV0, &b_jet_AntiKt4TopoEM_flavor_weight_SV0);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_SV1", &jet_AntiKt4TopoEM_flavor_weight_SV1, &b_jet_AntiKt4TopoEM_flavor_weight_SV1);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_SV2", &jet_AntiKt4TopoEM_flavor_weight_SV2, &b_jet_AntiKt4TopoEM_flavor_weight_SV2);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_SoftMuonTagChi2", &jet_AntiKt4TopoEM_flavor_weight_SoftMuonTagChi2, &b_jet_AntiKt4TopoEM_flavor_weight_SoftMuonTagChi2);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_SecondSoftMuonTagChi2", &jet_AntiKt4TopoEM_flavor_weight_SecondSoftMuonTagChi2, &b_jet_AntiKt4TopoEM_flavor_weight_SecondSoftMuonTagChi2);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_JetFitterTagNN", &jet_AntiKt4TopoEM_flavor_weight_JetFitterTagNN, &b_jet_AntiKt4TopoEM_flavor_weight_JetFitterTagNN);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_JetFitterCOMBNN", &jet_AntiKt4TopoEM_flavor_weight_JetFitterCOMBNN, &b_jet_AntiKt4TopoEM_flavor_weight_JetFitterCOMBNN);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_MV1", &jet_AntiKt4TopoEM_flavor_weight_MV1, &b_jet_AntiKt4TopoEM_flavor_weight_MV1);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_MV2", &jet_AntiKt4TopoEM_flavor_weight_MV2, &b_jet_AntiKt4TopoEM_flavor_weight_MV2);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_GbbNN", &jet_AntiKt4TopoEM_flavor_weight_GbbNN, &b_jet_AntiKt4TopoEM_flavor_weight_GbbNN);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_JetFitterCharm", &jet_AntiKt4TopoEM_flavor_weight_JetFitterCharm, &b_jet_AntiKt4TopoEM_flavor_weight_JetFitterCharm);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_MV3_bVSu", &jet_AntiKt4TopoEM_flavor_weight_MV3_bVSu, &b_jet_AntiKt4TopoEM_flavor_weight_MV3_bVSu);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_MV3_bVSc", &jet_AntiKt4TopoEM_flavor_weight_MV3_bVSc, &b_jet_AntiKt4TopoEM_flavor_weight_MV3_bVSc);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_MV3_cVSu", &jet_AntiKt4TopoEM_flavor_weight_MV3_cVSu, &b_jet_AntiKt4TopoEM_flavor_weight_MV3_cVSu);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_truth_label", &jet_AntiKt4TopoEM_flavor_truth_label, &b_jet_AntiKt4TopoEM_flavor_truth_label);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_truth_dRminToB", &jet_AntiKt4TopoEM_flavor_truth_dRminToB, &b_jet_AntiKt4TopoEM_flavor_truth_dRminToB);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_truth_dRminToC", &jet_AntiKt4TopoEM_flavor_truth_dRminToC, &b_jet_AntiKt4TopoEM_flavor_truth_dRminToC);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_truth_dRminToT", &jet_AntiKt4TopoEM_flavor_truth_dRminToT, &b_jet_AntiKt4TopoEM_flavor_truth_dRminToT);
   fChain->SetBranchAddress("jet_AntiKt4TopoEM_flavor_truth_BHadronpdg", &jet_AntiKt4TopoEM_flavor_truth_BHadronpdg, &b_jet_AntiKt4TopoEM_flavor_truth_BHadronpdg);
   fChain->SetBranchAddress("Eventshape_rhoKt4EM", &Eventshape_rhoKt4EM, &b_Eventshape_rhoKt4EM);
   fChain->SetBranchAddress("Eventshape_rhoKt4LC", &Eventshape_rhoKt4LC, &b_Eventshape_rhoKt4LC);
   fChain->SetBranchAddress("MET_RefFinal_etx", &MET_RefFinal_etx, &b_MET_RefFinal_etx);
   fChain->SetBranchAddress("MET_RefFinal_ety", &MET_RefFinal_ety, &b_MET_RefFinal_ety);
   fChain->SetBranchAddress("MET_RefFinal_phi", &MET_RefFinal_phi, &b_MET_RefFinal_phi);
   fChain->SetBranchAddress("MET_RefFinal_et", &MET_RefFinal_et, &b_MET_RefFinal_et);
   fChain->SetBranchAddress("MET_RefFinal_sumet", &MET_RefFinal_sumet, &b_MET_RefFinal_sumet);
   fChain->SetBranchAddress("MET_RefEle_etx", &MET_RefEle_etx, &b_MET_RefEle_etx);
   fChain->SetBranchAddress("MET_RefEle_ety", &MET_RefEle_ety, &b_MET_RefEle_ety);
   fChain->SetBranchAddress("MET_RefEle_phi", &MET_RefEle_phi, &b_MET_RefEle_phi);
   fChain->SetBranchAddress("MET_RefEle_et", &MET_RefEle_et, &b_MET_RefEle_et);
   fChain->SetBranchAddress("MET_RefEle_sumet", &MET_RefEle_sumet, &b_MET_RefEle_sumet);
   fChain->SetBranchAddress("MET_RefJet_etx", &MET_RefJet_etx, &b_MET_RefJet_etx);
   fChain->SetBranchAddress("MET_RefJet_ety", &MET_RefJet_ety, &b_MET_RefJet_ety);
   fChain->SetBranchAddress("MET_RefJet_phi", &MET_RefJet_phi, &b_MET_RefJet_phi);
   fChain->SetBranchAddress("MET_RefJet_et", &MET_RefJet_et, &b_MET_RefJet_et);
   fChain->SetBranchAddress("MET_RefJet_sumet", &MET_RefJet_sumet, &b_MET_RefJet_sumet);
   fChain->SetBranchAddress("MET_RefMuon_Staco_etx", &MET_RefMuon_Staco_etx, &b_MET_RefMuon_Staco_etx);
   fChain->SetBranchAddress("MET_RefMuon_Staco_ety", &MET_RefMuon_Staco_ety, &b_MET_RefMuon_Staco_ety);
   fChain->SetBranchAddress("MET_RefMuon_Staco_phi", &MET_RefMuon_Staco_phi, &b_MET_RefMuon_Staco_phi);
   fChain->SetBranchAddress("MET_RefMuon_Staco_et", &MET_RefMuon_Staco_et, &b_MET_RefMuon_Staco_et);
   fChain->SetBranchAddress("MET_RefMuon_Staco_sumet", &MET_RefMuon_Staco_sumet, &b_MET_RefMuon_Staco_sumet);
   fChain->SetBranchAddress("MET_RefGamma_etx", &MET_RefGamma_etx, &b_MET_RefGamma_etx);
   fChain->SetBranchAddress("MET_RefGamma_ety", &MET_RefGamma_ety, &b_MET_RefGamma_ety);
   fChain->SetBranchAddress("MET_RefGamma_phi", &MET_RefGamma_phi, &b_MET_RefGamma_phi);
   fChain->SetBranchAddress("MET_RefGamma_et", &MET_RefGamma_et, &b_MET_RefGamma_et);
   fChain->SetBranchAddress("MET_RefGamma_sumet", &MET_RefGamma_sumet, &b_MET_RefGamma_sumet);
   fChain->SetBranchAddress("MET_RefTau_etx", &MET_RefTau_etx, &b_MET_RefTau_etx);
   fChain->SetBranchAddress("MET_RefTau_ety", &MET_RefTau_ety, &b_MET_RefTau_ety);
   fChain->SetBranchAddress("MET_RefTau_phi", &MET_RefTau_phi, &b_MET_RefTau_phi);
   fChain->SetBranchAddress("MET_RefTau_et", &MET_RefTau_et, &b_MET_RefTau_et);
   fChain->SetBranchAddress("MET_RefTau_sumet", &MET_RefTau_sumet, &b_MET_RefTau_sumet);
   fChain->SetBranchAddress("MET_Track_etx", &MET_Track_etx, &b_MET_Track_etx);
   fChain->SetBranchAddress("MET_Track_ety", &MET_Track_ety, &b_MET_Track_ety);
   fChain->SetBranchAddress("MET_Track_phi", &MET_Track_phi, &b_MET_Track_phi);
   fChain->SetBranchAddress("MET_Track_et", &MET_Track_et, &b_MET_Track_et);
   fChain->SetBranchAddress("MET_Track_sumet", &MET_Track_sumet, &b_MET_Track_sumet);
   fChain->SetBranchAddress("MET_MuonBoy_etx", &MET_MuonBoy_etx, &b_MET_MuonBoy_etx);
   fChain->SetBranchAddress("MET_MuonBoy_ety", &MET_MuonBoy_ety, &b_MET_MuonBoy_ety);
   fChain->SetBranchAddress("MET_MuonBoy_phi", &MET_MuonBoy_phi, &b_MET_MuonBoy_phi);
   fChain->SetBranchAddress("MET_MuonBoy_et", &MET_MuonBoy_et, &b_MET_MuonBoy_et);
   fChain->SetBranchAddress("MET_MuonBoy_sumet", &MET_MuonBoy_sumet, &b_MET_MuonBoy_sumet);
   fChain->SetBranchAddress("MET_RefJet_JVF_etx", &MET_RefJet_JVF_etx, &b_MET_RefJet_JVF_etx);
   fChain->SetBranchAddress("MET_RefJet_JVF_ety", &MET_RefJet_JVF_ety, &b_MET_RefJet_JVF_ety);
   fChain->SetBranchAddress("MET_RefJet_JVF_phi", &MET_RefJet_JVF_phi, &b_MET_RefJet_JVF_phi);
   fChain->SetBranchAddress("MET_RefJet_JVF_et", &MET_RefJet_JVF_et, &b_MET_RefJet_JVF_et);
   fChain->SetBranchAddress("MET_RefJet_JVF_sumet", &MET_RefJet_JVF_sumet, &b_MET_RefJet_JVF_sumet);
   fChain->SetBranchAddress("MET_RefJet_JVFCut_etx", &MET_RefJet_JVFCut_etx, &b_MET_RefJet_JVFCut_etx);
   fChain->SetBranchAddress("MET_RefJet_JVFCut_ety", &MET_RefJet_JVFCut_ety, &b_MET_RefJet_JVFCut_ety);
   fChain->SetBranchAddress("MET_RefJet_JVFCut_phi", &MET_RefJet_JVFCut_phi, &b_MET_RefJet_JVFCut_phi);
   fChain->SetBranchAddress("MET_RefJet_JVFCut_et", &MET_RefJet_JVFCut_et, &b_MET_RefJet_JVFCut_et);
   fChain->SetBranchAddress("MET_RefJet_JVFCut_sumet", &MET_RefJet_JVFCut_sumet, &b_MET_RefJet_JVFCut_sumet);
   fChain->SetBranchAddress("MET_CellOut_Eflow_STVF_etx", &MET_CellOut_Eflow_STVF_etx, &b_MET_CellOut_Eflow_STVF_etx);
   fChain->SetBranchAddress("MET_CellOut_Eflow_STVF_ety", &MET_CellOut_Eflow_STVF_ety, &b_MET_CellOut_Eflow_STVF_ety);
   fChain->SetBranchAddress("MET_CellOut_Eflow_STVF_phi", &MET_CellOut_Eflow_STVF_phi, &b_MET_CellOut_Eflow_STVF_phi);
   fChain->SetBranchAddress("MET_CellOut_Eflow_STVF_et", &MET_CellOut_Eflow_STVF_et, &b_MET_CellOut_Eflow_STVF_et);
   fChain->SetBranchAddress("MET_CellOut_Eflow_STVF_sumet", &MET_CellOut_Eflow_STVF_sumet, &b_MET_CellOut_Eflow_STVF_sumet);
   fChain->SetBranchAddress("MET_CellOut_Eflow_JetArea_etx", &MET_CellOut_Eflow_JetArea_etx, &b_MET_CellOut_Eflow_JetArea_etx);
   fChain->SetBranchAddress("MET_CellOut_Eflow_JetArea_ety", &MET_CellOut_Eflow_JetArea_ety, &b_MET_CellOut_Eflow_JetArea_ety);
   fChain->SetBranchAddress("MET_CellOut_Eflow_JetArea_phi", &MET_CellOut_Eflow_JetArea_phi, &b_MET_CellOut_Eflow_JetArea_phi);
   fChain->SetBranchAddress("MET_CellOut_Eflow_JetArea_et", &MET_CellOut_Eflow_JetArea_et, &b_MET_CellOut_Eflow_JetArea_et);
   fChain->SetBranchAddress("MET_CellOut_Eflow_JetArea_sumet", &MET_CellOut_Eflow_JetArea_sumet, &b_MET_CellOut_Eflow_JetArea_sumet);
   fChain->SetBranchAddress("MET_CellOut_Eflow_JetAreaJVF_etx", &MET_CellOut_Eflow_JetAreaJVF_etx, &b_MET_CellOut_Eflow_JetAreaJVF_etx);
   fChain->SetBranchAddress("MET_CellOut_Eflow_JetAreaJVF_ety", &MET_CellOut_Eflow_JetAreaJVF_ety, &b_MET_CellOut_Eflow_JetAreaJVF_ety);
   fChain->SetBranchAddress("MET_CellOut_Eflow_JetAreaJVF_phi", &MET_CellOut_Eflow_JetAreaJVF_phi, &b_MET_CellOut_Eflow_JetAreaJVF_phi);
   fChain->SetBranchAddress("MET_CellOut_Eflow_JetAreaJVF_et", &MET_CellOut_Eflow_JetAreaJVF_et, &b_MET_CellOut_Eflow_JetAreaJVF_et);
   fChain->SetBranchAddress("MET_CellOut_Eflow_JetAreaJVF_sumet", &MET_CellOut_Eflow_JetAreaJVF_sumet, &b_MET_CellOut_Eflow_JetAreaJVF_sumet);
   fChain->SetBranchAddress("MET_CellOut_Eflow_JetAreaRhoEta5JVF_etx", &MET_CellOut_Eflow_JetAreaRhoEta5JVF_etx, &b_MET_CellOut_Eflow_JetAreaRhoEta5JVF_etx);
   fChain->SetBranchAddress("MET_CellOut_Eflow_JetAreaRhoEta5JVF_ety", &MET_CellOut_Eflow_JetAreaRhoEta5JVF_ety, &b_MET_CellOut_Eflow_JetAreaRhoEta5JVF_ety);
   fChain->SetBranchAddress("MET_CellOut_Eflow_JetAreaRhoEta5JVF_phi", &MET_CellOut_Eflow_JetAreaRhoEta5JVF_phi, &b_MET_CellOut_Eflow_JetAreaRhoEta5JVF_phi);
   fChain->SetBranchAddress("MET_CellOut_Eflow_JetAreaRhoEta5JVF_et", &MET_CellOut_Eflow_JetAreaRhoEta5JVF_et, &b_MET_CellOut_Eflow_JetAreaRhoEta5JVF_et);
   fChain->SetBranchAddress("MET_CellOut_Eflow_JetAreaRhoEta5JVF_sumet", &MET_CellOut_Eflow_JetAreaRhoEta5JVF_sumet, &b_MET_CellOut_Eflow_JetAreaRhoEta5JVF_sumet);
   fChain->SetBranchAddress("MET_RefFinal_STVF_etx", &MET_RefFinal_STVF_etx, &b_MET_RefFinal_STVF_etx);
   fChain->SetBranchAddress("MET_RefFinal_STVF_ety", &MET_RefFinal_STVF_ety, &b_MET_RefFinal_STVF_ety);
   fChain->SetBranchAddress("MET_RefFinal_STVF_phi", &MET_RefFinal_STVF_phi, &b_MET_RefFinal_STVF_phi);
   fChain->SetBranchAddress("MET_RefFinal_STVF_et", &MET_RefFinal_STVF_et, &b_MET_RefFinal_STVF_et);
   fChain->SetBranchAddress("MET_RefFinal_STVF_sumet", &MET_RefFinal_STVF_sumet, &b_MET_RefFinal_STVF_sumet);
   fChain->SetBranchAddress("MET_RefMuon_Track_etx", &MET_RefMuon_Track_etx, &b_MET_RefMuon_Track_etx);
   fChain->SetBranchAddress("MET_RefMuon_Track_ety", &MET_RefMuon_Track_ety, &b_MET_RefMuon_Track_ety);
   fChain->SetBranchAddress("MET_RefMuon_Track_phi", &MET_RefMuon_Track_phi, &b_MET_RefMuon_Track_phi);
   fChain->SetBranchAddress("MET_RefMuon_Track_et", &MET_RefMuon_Track_et, &b_MET_RefMuon_Track_et);
   fChain->SetBranchAddress("MET_RefMuon_Track_sumet", &MET_RefMuon_Track_sumet, &b_MET_RefMuon_Track_sumet);
   fChain->SetBranchAddress("MET_RefMuon_Track_Staco_etx", &MET_RefMuon_Track_Staco_etx, &b_MET_RefMuon_Track_Staco_etx);
   fChain->SetBranchAddress("MET_RefMuon_Track_Staco_ety", &MET_RefMuon_Track_Staco_ety, &b_MET_RefMuon_Track_Staco_ety);
   fChain->SetBranchAddress("MET_RefMuon_Track_Staco_phi", &MET_RefMuon_Track_Staco_phi, &b_MET_RefMuon_Track_Staco_phi);
   fChain->SetBranchAddress("MET_RefMuon_Track_Staco_et", &MET_RefMuon_Track_Staco_et, &b_MET_RefMuon_Track_Staco_et);
   fChain->SetBranchAddress("MET_RefMuon_Track_Staco_sumet", &MET_RefMuon_Track_Staco_sumet, &b_MET_RefMuon_Track_Staco_sumet);
   fChain->SetBranchAddress("MET_LocHadTopo_etx", &MET_LocHadTopo_etx, &b_MET_LocHadTopo_etx);
   fChain->SetBranchAddress("MET_LocHadTopo_ety", &MET_LocHadTopo_ety, &b_MET_LocHadTopo_ety);
   fChain->SetBranchAddress("MET_LocHadTopo_phi", &MET_LocHadTopo_phi, &b_MET_LocHadTopo_phi);
   fChain->SetBranchAddress("MET_LocHadTopo_et", &MET_LocHadTopo_et, &b_MET_LocHadTopo_et);
   fChain->SetBranchAddress("MET_LocHadTopo_sumet", &MET_LocHadTopo_sumet, &b_MET_LocHadTopo_sumet);
   fChain->SetBranchAddress("MET_LocHadTopo_etx_CentralReg", &MET_LocHadTopo_etx_CentralReg, &b_MET_LocHadTopo_etx_CentralReg);
   fChain->SetBranchAddress("MET_LocHadTopo_ety_CentralReg", &MET_LocHadTopo_ety_CentralReg, &b_MET_LocHadTopo_ety_CentralReg);
   fChain->SetBranchAddress("MET_LocHadTopo_sumet_CentralReg", &MET_LocHadTopo_sumet_CentralReg, &b_MET_LocHadTopo_sumet_CentralReg);
   fChain->SetBranchAddress("MET_LocHadTopo_phi_CentralReg", &MET_LocHadTopo_phi_CentralReg, &b_MET_LocHadTopo_phi_CentralReg);
   fChain->SetBranchAddress("MET_LocHadTopo_etx_EndcapRegion", &MET_LocHadTopo_etx_EndcapRegion, &b_MET_LocHadTopo_etx_EndcapRegion);
   fChain->SetBranchAddress("MET_LocHadTopo_ety_EndcapRegion", &MET_LocHadTopo_ety_EndcapRegion, &b_MET_LocHadTopo_ety_EndcapRegion);
   fChain->SetBranchAddress("MET_LocHadTopo_sumet_EndcapRegion", &MET_LocHadTopo_sumet_EndcapRegion, &b_MET_LocHadTopo_sumet_EndcapRegion);
   fChain->SetBranchAddress("MET_LocHadTopo_phi_EndcapRegion", &MET_LocHadTopo_phi_EndcapRegion, &b_MET_LocHadTopo_phi_EndcapRegion);
   fChain->SetBranchAddress("MET_LocHadTopo_etx_ForwardReg", &MET_LocHadTopo_etx_ForwardReg, &b_MET_LocHadTopo_etx_ForwardReg);
   fChain->SetBranchAddress("MET_LocHadTopo_ety_ForwardReg", &MET_LocHadTopo_ety_ForwardReg, &b_MET_LocHadTopo_ety_ForwardReg);
   fChain->SetBranchAddress("MET_LocHadTopo_sumet_ForwardReg", &MET_LocHadTopo_sumet_ForwardReg, &b_MET_LocHadTopo_sumet_ForwardReg);
   fChain->SetBranchAddress("MET_LocHadTopo_phi_ForwardReg", &MET_LocHadTopo_phi_ForwardReg, &b_MET_LocHadTopo_phi_ForwardReg);
   fChain->SetBranchAddress("MET_Truth_NonInt_etx", &MET_Truth_NonInt_etx, &b_MET_Truth_NonInt_etx);
   fChain->SetBranchAddress("MET_Truth_NonInt_ety", &MET_Truth_NonInt_ety, &b_MET_Truth_NonInt_ety);
   fChain->SetBranchAddress("MET_Truth_NonInt_sumet", &MET_Truth_NonInt_sumet, &b_MET_Truth_NonInt_sumet);
   fChain->SetBranchAddress("MET_Truth_PileUp_NonInt_etx", &MET_Truth_PileUp_NonInt_etx, &b_MET_Truth_PileUp_NonInt_etx);
   fChain->SetBranchAddress("MET_Truth_PileUp_NonInt_ety", &MET_Truth_PileUp_NonInt_ety, &b_MET_Truth_PileUp_NonInt_ety);
   fChain->SetBranchAddress("MET_Truth_PileUp_NonInt_sumet", &MET_Truth_PileUp_NonInt_sumet, &b_MET_Truth_PileUp_NonInt_sumet);
   fChain->SetBranchAddress("el_MET_n", &el_MET_n, &b_el_MET_n);
   fChain->SetBranchAddress("el_MET_wpx", &el_MET_wpx, &b_el_MET_wpx);
   fChain->SetBranchAddress("el_MET_wpy", &el_MET_wpy, &b_el_MET_wpy);
   fChain->SetBranchAddress("el_MET_wet", &el_MET_wet, &b_el_MET_wet);
   fChain->SetBranchAddress("el_MET_statusWord", &el_MET_statusWord, &b_el_MET_statusWord);
   fChain->SetBranchAddress("ph_MET_n", &ph_MET_n, &b_ph_MET_n);
   fChain->SetBranchAddress("ph_MET_wpx", &ph_MET_wpx, &b_ph_MET_wpx);
   fChain->SetBranchAddress("ph_MET_wpy", &ph_MET_wpy, &b_ph_MET_wpy);
   fChain->SetBranchAddress("ph_MET_wet", &ph_MET_wet, &b_ph_MET_wet);
   fChain->SetBranchAddress("ph_MET_statusWord", &ph_MET_statusWord, &b_ph_MET_statusWord);
   fChain->SetBranchAddress("mu_staco_MET_n", &mu_staco_MET_n, &b_mu_staco_MET_n);
   fChain->SetBranchAddress("mu_staco_MET_wpx", &mu_staco_MET_wpx, &b_mu_staco_MET_wpx);
   fChain->SetBranchAddress("mu_staco_MET_wpy", &mu_staco_MET_wpy, &b_mu_staco_MET_wpy);
   fChain->SetBranchAddress("mu_staco_MET_wet", &mu_staco_MET_wet, &b_mu_staco_MET_wet);
   fChain->SetBranchAddress("mu_staco_MET_statusWord", &mu_staco_MET_statusWord, &b_mu_staco_MET_statusWord);
   fChain->SetBranchAddress("tau_MET_n", &tau_MET_n, &b_tau_MET_n);
   fChain->SetBranchAddress("tau_MET_wpx", &tau_MET_wpx, &b_tau_MET_wpx);
   fChain->SetBranchAddress("tau_MET_wpy", &tau_MET_wpy, &b_tau_MET_wpy);
   fChain->SetBranchAddress("tau_MET_wet", &tau_MET_wet, &b_tau_MET_wet);
   fChain->SetBranchAddress("tau_MET_statusWord", &tau_MET_statusWord, &b_tau_MET_statusWord);
   fChain->SetBranchAddress("jet_AntiKt4LCTopo_MET_n", &jet_AntiKt4LCTopo_MET_n, &b_jet_AntiKt4LCTopo_MET_n);
   fChain->SetBranchAddress("jet_AntiKt4LCTopo_MET_wpx", &jet_AntiKt4LCTopo_MET_wpx, &b_jet_AntiKt4LCTopo_MET_wpx);
   fChain->SetBranchAddress("jet_AntiKt4LCTopo_MET_wpy", &jet_AntiKt4LCTopo_MET_wpy, &b_jet_AntiKt4LCTopo_MET_wpy);
   fChain->SetBranchAddress("jet_AntiKt4LCTopo_MET_wet", &jet_AntiKt4LCTopo_MET_wet, &b_jet_AntiKt4LCTopo_MET_wet);
   fChain->SetBranchAddress("jet_AntiKt4LCTopo_MET_statusWord", &jet_AntiKt4LCTopo_MET_statusWord, &b_jet_AntiKt4LCTopo_MET_statusWord);
   fChain->SetBranchAddress("tau_MET_BDTMedium_n", &tau_MET_BDTMedium_n, &b_tau_MET_BDTMedium_n);
   fChain->SetBranchAddress("tau_MET_BDTMedium_wpx", &tau_MET_BDTMedium_wpx, &b_tau_MET_BDTMedium_wpx);
   fChain->SetBranchAddress("tau_MET_BDTMedium_wpy", &tau_MET_BDTMedium_wpy, &b_tau_MET_BDTMedium_wpy);
   fChain->SetBranchAddress("tau_MET_BDTMedium_wet", &tau_MET_BDTMedium_wet, &b_tau_MET_BDTMedium_wet);
   fChain->SetBranchAddress("tau_MET_BDTMedium_statusWord", &tau_MET_BDTMedium_statusWord, &b_tau_MET_BDTMedium_statusWord);
   fChain->SetBranchAddress("jet_AntiKt4LCTopo_MET_BDTMedium_n", &jet_AntiKt4LCTopo_MET_BDTMedium_n, &b_jet_AntiKt4LCTopo_MET_BDTMedium_n);
   fChain->SetBranchAddress("jet_AntiKt4LCTopo_MET_BDTMedium_wpx", &jet_AntiKt4LCTopo_MET_BDTMedium_wpx, &b_jet_AntiKt4LCTopo_MET_BDTMedium_wpx);
   fChain->SetBranchAddress("jet_AntiKt4LCTopo_MET_BDTMedium_wpy", &jet_AntiKt4LCTopo_MET_BDTMedium_wpy, &b_jet_AntiKt4LCTopo_MET_BDTMedium_wpy);
   fChain->SetBranchAddress("jet_AntiKt4LCTopo_MET_BDTMedium_wet", &jet_AntiKt4LCTopo_MET_BDTMedium_wet, &b_jet_AntiKt4LCTopo_MET_BDTMedium_wet);
   fChain->SetBranchAddress("jet_AntiKt4LCTopo_MET_BDTMedium_statusWord", &jet_AntiKt4LCTopo_MET_BDTMedium_statusWord, &b_jet_AntiKt4LCTopo_MET_BDTMedium_statusWord);
   fChain->SetBranchAddress("MET_RefFinal_BDTMedium_etx", &MET_RefFinal_BDTMedium_etx, &b_MET_RefFinal_BDTMedium_etx);
   fChain->SetBranchAddress("MET_RefFinal_BDTMedium_ety", &MET_RefFinal_BDTMedium_ety, &b_MET_RefFinal_BDTMedium_ety);
   fChain->SetBranchAddress("MET_RefFinal_BDTMedium_phi", &MET_RefFinal_BDTMedium_phi, &b_MET_RefFinal_BDTMedium_phi);
   fChain->SetBranchAddress("MET_RefFinal_BDTMedium_et", &MET_RefFinal_BDTMedium_et, &b_MET_RefFinal_BDTMedium_et);
   fChain->SetBranchAddress("MET_RefFinal_BDTMedium_sumet", &MET_RefFinal_BDTMedium_sumet, &b_MET_RefFinal_BDTMedium_sumet);
   fChain->SetBranchAddress("MET_RefTau_BDTMedium_etx", &MET_RefTau_BDTMedium_etx, &b_MET_RefTau_BDTMedium_etx);
   fChain->SetBranchAddress("MET_RefTau_BDTMedium_ety", &MET_RefTau_BDTMedium_ety, &b_MET_RefTau_BDTMedium_ety);
   fChain->SetBranchAddress("MET_RefTau_BDTMedium_phi", &MET_RefTau_BDTMedium_phi, &b_MET_RefTau_BDTMedium_phi);
   fChain->SetBranchAddress("MET_RefTau_BDTMedium_et", &MET_RefTau_BDTMedium_et, &b_MET_RefTau_BDTMedium_et);
   fChain->SetBranchAddress("MET_RefTau_BDTMedium_sumet", &MET_RefTau_BDTMedium_sumet, &b_MET_RefTau_BDTMedium_sumet);
   fChain->SetBranchAddress("MET_RefJet_BDTMedium_etx", &MET_RefJet_BDTMedium_etx, &b_MET_RefJet_BDTMedium_etx);
   fChain->SetBranchAddress("MET_RefJet_BDTMedium_ety", &MET_RefJet_BDTMedium_ety, &b_MET_RefJet_BDTMedium_ety);
   fChain->SetBranchAddress("MET_RefJet_BDTMedium_phi", &MET_RefJet_BDTMedium_phi, &b_MET_RefJet_BDTMedium_phi);
   fChain->SetBranchAddress("MET_RefJet_BDTMedium_et", &MET_RefJet_BDTMedium_et, &b_MET_RefJet_BDTMedium_et);
   fChain->SetBranchAddress("MET_RefJet_BDTMedium_sumet", &MET_RefJet_BDTMedium_sumet, &b_MET_RefJet_BDTMedium_sumet);
   fChain->SetBranchAddress("mu_staco_E", &mu_staco_E, &b_mu_staco_E);
   fChain->SetBranchAddress("mu_staco_pt", &mu_staco_pt, &b_mu_staco_pt);
   fChain->SetBranchAddress("mu_staco_eta", &mu_staco_eta, &b_mu_staco_eta);
   fChain->SetBranchAddress("mu_staco_phi", &mu_staco_phi, &b_mu_staco_phi);
   fChain->SetBranchAddress("mu_staco_px", &mu_staco_px, &b_mu_staco_px);
   fChain->SetBranchAddress("mu_staco_py", &mu_staco_py, &b_mu_staco_py);
   fChain->SetBranchAddress("mu_staco_pz", &mu_staco_pz, &b_mu_staco_pz);
   fChain->SetBranchAddress("mu_staco_charge", &mu_staco_charge, &b_mu_staco_charge);
   fChain->SetBranchAddress("mu_staco_author", &mu_staco_author, &b_mu_staco_author);
   fChain->SetBranchAddress("mu_staco_matchchi2", &mu_staco_matchchi2, &b_mu_staco_matchchi2);
   fChain->SetBranchAddress("mu_staco_etcone20", &mu_staco_etcone20, &b_mu_staco_etcone20);
   fChain->SetBranchAddress("mu_staco_etcone30", &mu_staco_etcone30, &b_mu_staco_etcone30);
   fChain->SetBranchAddress("mu_staco_etcone40", &mu_staco_etcone40, &b_mu_staco_etcone40);
   fChain->SetBranchAddress("mu_staco_ptcone20", &mu_staco_ptcone20, &b_mu_staco_ptcone20);
   fChain->SetBranchAddress("mu_staco_ptcone30", &mu_staco_ptcone30, &b_mu_staco_ptcone30);
   fChain->SetBranchAddress("mu_staco_ptcone40", &mu_staco_ptcone40, &b_mu_staco_ptcone40);
   fChain->SetBranchAddress("mu_staco_isStandAloneMuon", &mu_staco_isStandAloneMuon, &b_mu_staco_isStandAloneMuon);
   fChain->SetBranchAddress("mu_staco_isCombinedMuon", &mu_staco_isCombinedMuon, &b_mu_staco_isCombinedMuon);
   fChain->SetBranchAddress("mu_staco_isLowPtReconstructedMuon", &mu_staco_isLowPtReconstructedMuon, &b_mu_staco_isLowPtReconstructedMuon);
   fChain->SetBranchAddress("mu_staco_isSegmentTaggedMuon", &mu_staco_isSegmentTaggedMuon, &b_mu_staco_isSegmentTaggedMuon);
   fChain->SetBranchAddress("mu_staco_isCaloMuonId", &mu_staco_isCaloMuonId, &b_mu_staco_isCaloMuonId);
   fChain->SetBranchAddress("mu_staco_alsoFoundByLowPt", &mu_staco_alsoFoundByLowPt, &b_mu_staco_alsoFoundByLowPt);
   fChain->SetBranchAddress("mu_staco_alsoFoundByCaloMuonId", &mu_staco_alsoFoundByCaloMuonId, &b_mu_staco_alsoFoundByCaloMuonId);
   fChain->SetBranchAddress("mu_staco_loose", &mu_staco_loose, &b_mu_staco_loose);
   fChain->SetBranchAddress("mu_staco_medium", &mu_staco_medium, &b_mu_staco_medium);
   fChain->SetBranchAddress("mu_staco_tight", &mu_staco_tight, &b_mu_staco_tight);
   fChain->SetBranchAddress("mu_staco_z0_exPV", &mu_staco_z0_exPV, &b_mu_staco_z0_exPV);
   fChain->SetBranchAddress("mu_staco_id_d0_exPV", &mu_staco_id_d0_exPV, &b_mu_staco_id_d0_exPV);
   fChain->SetBranchAddress("mu_staco_id_z0_exPV", &mu_staco_id_z0_exPV, &b_mu_staco_id_z0_exPV);
   fChain->SetBranchAddress("mu_staco_id_phi_exPV", &mu_staco_id_phi_exPV, &b_mu_staco_id_phi_exPV);
   fChain->SetBranchAddress("mu_staco_id_theta_exPV", &mu_staco_id_theta_exPV, &b_mu_staco_id_theta_exPV);
   fChain->SetBranchAddress("mu_staco_id_qoverp_exPV", &mu_staco_id_qoverp_exPV, &b_mu_staco_id_qoverp_exPV);
   fChain->SetBranchAddress("mu_staco_me_phi_exPV", &mu_staco_me_phi_exPV, &b_mu_staco_me_phi_exPV);
   fChain->SetBranchAddress("mu_staco_me_theta_exPV", &mu_staco_me_theta_exPV, &b_mu_staco_me_theta_exPV);
   fChain->SetBranchAddress("mu_staco_me_qoverp_exPV", &mu_staco_me_qoverp_exPV, &b_mu_staco_me_qoverp_exPV);
   fChain->SetBranchAddress("mu_staco_ms_phi", &mu_staco_ms_phi, &b_mu_staco_ms_phi);
   fChain->SetBranchAddress("mu_staco_ms_theta", &mu_staco_ms_theta, &b_mu_staco_ms_theta);
   fChain->SetBranchAddress("mu_staco_ms_qoverp", &mu_staco_ms_qoverp, &b_mu_staco_ms_qoverp);
   fChain->SetBranchAddress("mu_staco_id_phi", &mu_staco_id_phi, &b_mu_staco_id_phi);
   fChain->SetBranchAddress("mu_staco_id_theta", &mu_staco_id_theta, &b_mu_staco_id_theta);
   fChain->SetBranchAddress("mu_staco_id_qoverp", &mu_staco_id_qoverp, &b_mu_staco_id_qoverp);
   fChain->SetBranchAddress("mu_staco_me_phi", &mu_staco_me_phi, &b_mu_staco_me_phi);
   fChain->SetBranchAddress("mu_staco_me_theta", &mu_staco_me_theta, &b_mu_staco_me_theta);
   fChain->SetBranchAddress("mu_staco_me_qoverp", &mu_staco_me_qoverp, &b_mu_staco_me_qoverp);
   fChain->SetBranchAddress("mu_staco_nBLHits", &mu_staco_nBLHits, &b_mu_staco_nBLHits);
   fChain->SetBranchAddress("mu_staco_nPixHits", &mu_staco_nPixHits, &b_mu_staco_nPixHits);
   fChain->SetBranchAddress("mu_staco_nSCTHits", &mu_staco_nSCTHits, &b_mu_staco_nSCTHits);
   fChain->SetBranchAddress("mu_staco_nTRTHits", &mu_staco_nTRTHits, &b_mu_staco_nTRTHits);
   fChain->SetBranchAddress("mu_staco_nPixHoles", &mu_staco_nPixHoles, &b_mu_staco_nPixHoles);
   fChain->SetBranchAddress("mu_staco_nSCTHoles", &mu_staco_nSCTHoles, &b_mu_staco_nSCTHoles);
   fChain->SetBranchAddress("mu_staco_nTRTOutliers", &mu_staco_nTRTOutliers, &b_mu_staco_nTRTOutliers);
   fChain->SetBranchAddress("mu_staco_nPixelDeadSensors", &mu_staco_nPixelDeadSensors, &b_mu_staco_nPixelDeadSensors);
   fChain->SetBranchAddress("mu_staco_nSCTDeadSensors", &mu_staco_nSCTDeadSensors, &b_mu_staco_nSCTDeadSensors);
   fChain->SetBranchAddress("mu_staco_expectBLayerHit", &mu_staco_expectBLayerHit, &b_mu_staco_expectBLayerHit);
   fChain->SetBranchAddress("mu_staco_trackd0pv", &mu_staco_trackd0pv, &b_mu_staco_trackd0pv);
   fChain->SetBranchAddress("mu_staco_trackz0pv", &mu_staco_trackz0pv, &b_mu_staco_trackz0pv);
   fChain->SetBranchAddress("mu_staco_tracksigd0pv", &mu_staco_tracksigd0pv, &b_mu_staco_tracksigd0pv);
   fChain->SetBranchAddress("mu_staco_tracksigz0pv", &mu_staco_tracksigz0pv, &b_mu_staco_tracksigz0pv);
   fChain->SetBranchAddress("mu_staco_trackd0pvunbiased", &mu_staco_trackd0pvunbiased, &b_mu_staco_trackd0pvunbiased);
   fChain->SetBranchAddress("mu_staco_trackz0pvunbiased", &mu_staco_trackz0pvunbiased, &b_mu_staco_trackz0pvunbiased);
   fChain->SetBranchAddress("mu_staco_tracksigd0pvunbiased", &mu_staco_tracksigd0pvunbiased, &b_mu_staco_tracksigd0pvunbiased);
   fChain->SetBranchAddress("mu_staco_tracksigz0pvunbiased", &mu_staco_tracksigz0pvunbiased, &b_mu_staco_tracksigz0pvunbiased);
   fChain->SetBranchAddress("top_hfor_type", &top_hfor_type, &b_top_hfor_type);
   fChain->SetBranchAddress("vxp_n", &vxp_n, &b_vxp_n);
   fChain->SetBranchAddress("vxp_x", &vxp_x, &b_vxp_x);
   fChain->SetBranchAddress("vxp_y", &vxp_y, &b_vxp_y);
   fChain->SetBranchAddress("vxp_z", &vxp_z, &b_vxp_z);
   fChain->SetBranchAddress("vxp_type", &vxp_type, &b_vxp_type);
   fChain->SetBranchAddress("vxp_nTracks", &vxp_nTracks, &b_vxp_nTracks);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
   fChain->SetBranchAddress("actualIntPerXing", &actualIntPerXing, &b_actualIntPerXing);
   fChain->SetBranchAddress("averageIntPerXing", &averageIntPerXing, &b_averageIntPerXing);
   fChain->SetBranchAddress("mc_channel_number", &mc_channel_number, &b_mc_channel_number);
   fChain->SetBranchAddress("mc_event_number", &mc_event_number, &b_mc_event_number);
   fChain->SetBranchAddress("mc_event_weight", &mc_event_weight, &b_mc_event_weight);
   fChain->SetBranchAddress("larFlags", &larFlags, &b_larFlags);
   fChain->SetBranchAddress("coreFlags", &coreFlags, &b_coreFlags);
   fChain->SetBranchAddress("larError", &larError, &b_larError);
   fChain->SetBranchAddress("tileError", &tileError, &b_tileError);
   fChain->SetBranchAddress("trig_L1_emtau_n", &trig_L1_emtau_n, &b_trig_L1_emtau_n);
   fChain->SetBranchAddress("trig_L1_emtau_eta", &trig_L1_emtau_eta, &b_trig_L1_emtau_eta);
   fChain->SetBranchAddress("trig_L1_emtau_phi", &trig_L1_emtau_phi, &b_trig_L1_emtau_phi);
   fChain->SetBranchAddress("trig_L1_emtau_EMClus", &trig_L1_emtau_EMClus, &b_trig_L1_emtau_EMClus);
   fChain->SetBranchAddress("trig_L1_emtau_hadCore", &trig_L1_emtau_hadCore, &b_trig_L1_emtau_hadCore);
   fChain->SetBranchAddress("trig_EF_tau_n", &trig_EF_tau_n, &b_trig_EF_tau_n);
   fChain->SetBranchAddress("trig_EF_tau_Et", &trig_EF_tau_Et, &b_trig_EF_tau_Et);
   fChain->SetBranchAddress("trig_EF_tau_pt", &trig_EF_tau_pt, &b_trig_EF_tau_pt);
   fChain->SetBranchAddress("trig_EF_tau_m", &trig_EF_tau_m, &b_trig_EF_tau_m);
   fChain->SetBranchAddress("trig_EF_tau_eta", &trig_EF_tau_eta, &b_trig_EF_tau_eta);
   fChain->SetBranchAddress("trig_EF_tau_phi", &trig_EF_tau_phi, &b_trig_EF_tau_phi);
   fChain->SetBranchAddress("trig_Nav_n", &trig_Nav_n, &b_trig_Nav_n);
   fChain->SetBranchAddress("trig_Nav_chain_ChainId", &trig_Nav_chain_ChainId, &b_trig_Nav_chain_ChainId);
   fChain->SetBranchAddress("trig_Nav_chain_RoIType", &trig_Nav_chain_RoIType, &b_trig_Nav_chain_RoIType);
   fChain->SetBranchAddress("trig_Nav_chain_RoIIndex", &trig_Nav_chain_RoIIndex, &b_trig_Nav_chain_RoIIndex);
   fChain->SetBranchAddress("trig_DB_SMK", &trig_DB_SMK, &b_trig_DB_SMK);
   fChain->SetBranchAddress("trig_L1_mu_n", &trig_L1_mu_n, &b_trig_L1_mu_n);
   fChain->SetBranchAddress("trig_L1_mu_pt", &trig_L1_mu_pt, &b_trig_L1_mu_pt);
   fChain->SetBranchAddress("trig_L1_mu_eta", &trig_L1_mu_eta, &b_trig_L1_mu_eta);
   fChain->SetBranchAddress("trig_L1_mu_phi", &trig_L1_mu_phi, &b_trig_L1_mu_phi);
   fChain->SetBranchAddress("trig_L1_mu_thrName", &trig_L1_mu_thrName, &b_trig_L1_mu_thrName);
   fChain->SetBranchAddress("trig_L2_muonfeature_n", &trig_L2_muonfeature_n, &b_trig_L2_muonfeature_n);
   fChain->SetBranchAddress("trig_L2_muonfeature_pt", &trig_L2_muonfeature_pt, &b_trig_L2_muonfeature_pt);
   fChain->SetBranchAddress("trig_L2_muonfeature_eta", &trig_L2_muonfeature_eta, &b_trig_L2_muonfeature_eta);
   fChain->SetBranchAddress("trig_L2_muonfeature_phi", &trig_L2_muonfeature_phi, &b_trig_L2_muonfeature_phi);
   fChain->SetBranchAddress("trig_L2_combmuonfeature_n", &trig_L2_combmuonfeature_n, &b_trig_L2_combmuonfeature_n);
   fChain->SetBranchAddress("trig_L2_combmuonfeature_pt", &trig_L2_combmuonfeature_pt, &b_trig_L2_combmuonfeature_pt);
   fChain->SetBranchAddress("trig_L2_combmuonfeature_eta", &trig_L2_combmuonfeature_eta, &b_trig_L2_combmuonfeature_eta);
   fChain->SetBranchAddress("trig_L2_combmuonfeature_phi", &trig_L2_combmuonfeature_phi, &b_trig_L2_combmuonfeature_phi);
   fChain->SetBranchAddress("trig_EF_trigmuonef_n", &trig_EF_trigmuonef_n, &b_trig_EF_trigmuonef_n);
   fChain->SetBranchAddress("trig_EF_trigmuonef_track_n", &trig_EF_trigmuonef_track_n, &b_trig_EF_trigmuonef_track_n);
   fChain->SetBranchAddress("trig_EF_trigmuonef_track_MuonType", &trig_EF_trigmuonef_track_MuonType, &b_trig_EF_trigmuonef_track_MuonType);
   fChain->SetBranchAddress("trig_EF_trigmuonef_track_SA_pt", &trig_EF_trigmuonef_track_SA_pt, &b_trig_EF_trigmuonef_track_SA_pt);
   fChain->SetBranchAddress("trig_EF_trigmuonef_track_SA_eta", &trig_EF_trigmuonef_track_SA_eta, &b_trig_EF_trigmuonef_track_SA_eta);
   fChain->SetBranchAddress("trig_EF_trigmuonef_track_SA_phi", &trig_EF_trigmuonef_track_SA_phi, &b_trig_EF_trigmuonef_track_SA_phi);
   fChain->SetBranchAddress("trig_EF_trigmuonef_track_CB_pt", &trig_EF_trigmuonef_track_CB_pt, &b_trig_EF_trigmuonef_track_CB_pt);
   fChain->SetBranchAddress("trig_EF_trigmuonef_track_CB_eta", &trig_EF_trigmuonef_track_CB_eta, &b_trig_EF_trigmuonef_track_CB_eta);
   fChain->SetBranchAddress("trig_EF_trigmuonef_track_CB_phi", &trig_EF_trigmuonef_track_CB_phi, &b_trig_EF_trigmuonef_track_CB_phi);
   fChain->SetBranchAddress("trig_EF_trigmugirl_n", &trig_EF_trigmugirl_n, &b_trig_EF_trigmugirl_n);
   fChain->SetBranchAddress("trig_EF_trigmugirl_track_n", &trig_EF_trigmugirl_track_n, &b_trig_EF_trigmugirl_track_n);
   fChain->SetBranchAddress("trig_EF_trigmugirl_track_MuonType", &trig_EF_trigmugirl_track_MuonType, &b_trig_EF_trigmugirl_track_MuonType);
   fChain->SetBranchAddress("trig_EF_trigmugirl_track_SA_pt", &trig_EF_trigmugirl_track_SA_pt, &b_trig_EF_trigmugirl_track_SA_pt);
   fChain->SetBranchAddress("trig_EF_trigmugirl_track_SA_eta", &trig_EF_trigmugirl_track_SA_eta, &b_trig_EF_trigmugirl_track_SA_eta);
   fChain->SetBranchAddress("trig_EF_trigmugirl_track_SA_phi", &trig_EF_trigmugirl_track_SA_phi, &b_trig_EF_trigmugirl_track_SA_phi);
   fChain->SetBranchAddress("trig_EF_trigmugirl_track_CB_pt", &trig_EF_trigmugirl_track_CB_pt, &b_trig_EF_trigmugirl_track_CB_pt);
   fChain->SetBranchAddress("trig_EF_trigmugirl_track_CB_eta", &trig_EF_trigmugirl_track_CB_eta, &b_trig_EF_trigmugirl_track_CB_eta);
   fChain->SetBranchAddress("trig_EF_trigmugirl_track_CB_phi", &trig_EF_trigmugirl_track_CB_phi, &b_trig_EF_trigmugirl_track_CB_phi);
   fChain->SetBranchAddress("trig_RoI_L2_mu_n", &trig_RoI_L2_mu_n, &b_trig_RoI_L2_mu_n);
   fChain->SetBranchAddress("trig_RoI_L2_mu_MuonFeature", &trig_RoI_L2_mu_MuonFeature, &b_trig_RoI_L2_mu_MuonFeature);
   fChain->SetBranchAddress("trig_RoI_L2_mu_CombinedMuonFeature", &trig_RoI_L2_mu_CombinedMuonFeature, &b_trig_RoI_L2_mu_CombinedMuonFeature);
   fChain->SetBranchAddress("trig_RoI_L2_mu_CombinedMuonFeatureStatus", &trig_RoI_L2_mu_CombinedMuonFeatureStatus, &b_trig_RoI_L2_mu_CombinedMuonFeatureStatus);
   fChain->SetBranchAddress("trig_RoI_L2_mu_Muon_ROI", &trig_RoI_L2_mu_Muon_ROI, &b_trig_RoI_L2_mu_Muon_ROI);
   fChain->SetBranchAddress("trig_RoI_EF_mu_n", &trig_RoI_EF_mu_n, &b_trig_RoI_EF_mu_n);
   fChain->SetBranchAddress("trig_RoI_EF_mu_Muon_ROI", &trig_RoI_EF_mu_Muon_ROI, &b_trig_RoI_EF_mu_Muon_ROI);
   fChain->SetBranchAddress("trig_RoI_EF_mu_TrigMuonEFInfoContainer", &trig_RoI_EF_mu_TrigMuonEFInfoContainer, &b_trig_RoI_EF_mu_TrigMuonEFInfoContainer);
   fChain->SetBranchAddress("trig_RoI_EF_mu_TrigMuonEFInfoContainerStatus", &trig_RoI_EF_mu_TrigMuonEFInfoContainerStatus, &b_trig_RoI_EF_mu_TrigMuonEFInfoContainerStatus);
   fChain->SetBranchAddress("trig_RoI_EF_mu_TrigMuonEFIsolationContainer", &trig_RoI_EF_mu_TrigMuonEFIsolationContainer, &b_trig_RoI_EF_mu_TrigMuonEFIsolationContainer);
   fChain->SetBranchAddress("trig_RoI_EF_mu_TrigMuonEFIsolationContainerStatus", &trig_RoI_EF_mu_TrigMuonEFIsolationContainerStatus, &b_trig_RoI_EF_mu_TrigMuonEFIsolationContainerStatus);
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("lbn", &lbn, &b_lbn);
   fChain->SetBranchAddress("el_n", &el_n, &b_el_n);
   fChain->SetBranchAddress("mu_staco_n", &mu_staco_n, &b_mu_staco_n);
   fChain->SetBranchAddress("mcevt0_event_number", &mcevt0_event_number, &b_mcevt0_event_number);
   fChain->SetBranchAddress("mcevt0_event_scale", &mcevt0_event_scale, &b_mcevt0_event_scale);
   fChain->SetBranchAddress("mcevt0_alphaQCD", &mcevt0_alphaQCD, &b_mcevt0_alphaQCD);
   fChain->SetBranchAddress("mcevt0_alphaQED", &mcevt0_alphaQED, &b_mcevt0_alphaQED);
   fChain->SetBranchAddress("mcevt0_pdf_id1", &mcevt0_pdf_id1, &b_mcevt0_pdf_id1);
   fChain->SetBranchAddress("mcevt0_pdf_id2", &mcevt0_pdf_id2, &b_mcevt0_pdf_id2);
   fChain->SetBranchAddress("mcevt0_pdf_x1", &mcevt0_pdf_x1, &b_mcevt0_pdf_x1);
   fChain->SetBranchAddress("mcevt0_pdf_x2", &mcevt0_pdf_x2, &b_mcevt0_pdf_x2);
   fChain->SetBranchAddress("mcevt0_pdf_scale", &mcevt0_pdf_scale, &b_mcevt0_pdf_scale);
   fChain->SetBranchAddress("mcevt0_pdf1", &mcevt0_pdf1, &b_mcevt0_pdf1);
   fChain->SetBranchAddress("mcevt0_pdf2", &mcevt0_pdf2, &b_mcevt0_pdf2);
   fChain->SetBranchAddress("mcevt0_weight", &mcevt0_weight, &b_mcevt0_weight);
   fChain->SetBranchAddress("truthLP_n", &truthLP_n, &b_truthLP_n);
   fChain->SetBranchAddress("truthLP_pt", &truthLP_pt, &b_truthLP_pt);
   fChain->SetBranchAddress("truthLP_m", &truthLP_m, &b_truthLP_m);
   fChain->SetBranchAddress("truthLP_eta", &truthLP_eta, &b_truthLP_eta);
   fChain->SetBranchAddress("truthLP_phi", &truthLP_phi, &b_truthLP_phi);
   fChain->SetBranchAddress("truthLP_pdgId", &truthLP_pdgId, &b_truthLP_pdgId);
   fChain->SetBranchAddress("truthLP_charge", &truthLP_charge, &b_truthLP_charge);
   fChain->SetBranchAddress("truthLP_status", &truthLP_status, &b_truthLP_status);
   fChain->SetBranchAddress("truthLP_pdgId_parent", &truthLP_pdgId_parent, &b_truthLP_pdgId_parent);
   fChain->SetBranchAddress("truthTau_n", &truthTau_n, &b_truthTau_n);
   fChain->SetBranchAddress("truthTau_pt", &truthTau_pt, &b_truthTau_pt);
   fChain->SetBranchAddress("truthTau_m", &truthTau_m, &b_truthTau_m);
   fChain->SetBranchAddress("truthTau_eta", &truthTau_eta, &b_truthTau_eta);
   fChain->SetBranchAddress("truthTau_phi", &truthTau_phi, &b_truthTau_phi);
   fChain->SetBranchAddress("truthTau_pdgId", &truthTau_pdgId, &b_truthTau_pdgId);
   fChain->SetBranchAddress("truthTau_charge", &truthTau_charge, &b_truthTau_charge);
   fChain->SetBranchAddress("truthTau_status", &truthTau_status, &b_truthTau_status);
   fChain->SetBranchAddress("truthTau_pdgId_parent", &truthTau_pdgId_parent, &b_truthTau_pdgId_parent);
   fChain->SetBranchAddress("truthTau_isLeptonicDecay", &truthTau_isLeptonicDecay, &b_truthTau_isLeptonicDecay);
   fChain->SetBranchAddress("truthTau_nProng", &truthTau_nProng, &b_truthTau_nProng);
   fChain->SetBranchAddress("truthTau_vis_Et", &truthTau_vis_Et, &b_truthTau_vis_Et);
   fChain->SetBranchAddress("truthTau_vis_m", &truthTau_vis_m, &b_truthTau_vis_m);
   fChain->SetBranchAddress("truthTau_vis_eta", &truthTau_vis_eta, &b_truthTau_vis_eta);
   fChain->SetBranchAddress("truthTau_vis_phi", &truthTau_vis_phi, &b_truthTau_vis_phi);
   fChain->SetBranchAddress("truthTau_invis_Et", &truthTau_invis_Et, &b_truthTau_invis_Et);
   fChain->SetBranchAddress("truthTau_invis_m", &truthTau_invis_m, &b_truthTau_invis_m);
   fChain->SetBranchAddress("truthTau_invis_eta", &truthTau_invis_eta, &b_truthTau_invis_eta);
   fChain->SetBranchAddress("truthTau_invis_phi", &truthTau_invis_phi, &b_truthTau_invis_phi);
   fChain->SetBranchAddress("truthTau_child_n", &truthTau_child_n, &b_truthTau_child_n);
   fChain->SetBranchAddress("truthTau_child_pt", &truthTau_child_pt, &b_truthTau_child_pt);
   fChain->SetBranchAddress("truthTau_child_m", &truthTau_child_m, &b_truthTau_child_m);
   fChain->SetBranchAddress("truthTau_child_eta", &truthTau_child_eta, &b_truthTau_child_eta);
   fChain->SetBranchAddress("truthTau_child_phi", &truthTau_child_phi, &b_truthTau_child_phi);
   fChain->SetBranchAddress("truthTau_child_pdgId", &truthTau_child_pdgId, &b_truthTau_child_pdgId);
   fChain->SetBranchAddress("truthTau_child_charge", &truthTau_child_charge, &b_truthTau_child_charge);
   fChain->SetBranchAddress("truthTau_child_status", &truthTau_child_status, &b_truthTau_child_status);
   fChain->SetBranchAddress("truthH_n", &truthH_n, &b_truthH_n);
   fChain->SetBranchAddress("truthH_pt", &truthH_pt, &b_truthH_pt);
   fChain->SetBranchAddress("truthH_m", &truthH_m, &b_truthH_m);
   fChain->SetBranchAddress("truthH_eta", &truthH_eta, &b_truthH_eta);
   fChain->SetBranchAddress("truthH_phi", &truthH_phi, &b_truthH_phi);
   fChain->SetBranchAddress("truthH_pdgId", &truthH_pdgId, &b_truthH_pdgId);
   fChain->SetBranchAddress("truthH_charge", &truthH_charge, &b_truthH_charge);
   fChain->SetBranchAddress("truthH_status", &truthH_status, &b_truthH_status);
   fChain->SetBranchAddress("truthH_child_n", &truthH_child_n, &b_truthH_child_n);
   fChain->SetBranchAddress("truthH_child_pt", &truthH_child_pt, &b_truthH_child_pt);
   fChain->SetBranchAddress("truthH_child_m", &truthH_child_m, &b_truthH_child_m);
   fChain->SetBranchAddress("truthH_child_eta", &truthH_child_eta, &b_truthH_child_eta);
   fChain->SetBranchAddress("truthH_child_phi", &truthH_child_phi, &b_truthH_child_phi);
   fChain->SetBranchAddress("truthH_child_pdgId", &truthH_child_pdgId, &b_truthH_child_pdgId);
   fChain->SetBranchAddress("truthH_child_charge", &truthH_child_charge, &b_truthH_child_charge);
   fChain->SetBranchAddress("truthH_child_status", &truthH_child_status, &b_truthH_child_status);
   fChain->SetBranchAddress("truthZ_n", &truthZ_n, &b_truthZ_n);
   fChain->SetBranchAddress("truthZ_pt", &truthZ_pt, &b_truthZ_pt);
   fChain->SetBranchAddress("truthZ_m", &truthZ_m, &b_truthZ_m);
   fChain->SetBranchAddress("truthZ_eta", &truthZ_eta, &b_truthZ_eta);
   fChain->SetBranchAddress("truthZ_phi", &truthZ_phi, &b_truthZ_phi);
   fChain->SetBranchAddress("truthZ_pdgId", &truthZ_pdgId, &b_truthZ_pdgId);
   fChain->SetBranchAddress("truthZ_status", &truthZ_status, &b_truthZ_status);
   fChain->SetBranchAddress("truthZ_pdgId_parent", &truthZ_pdgId_parent, &b_truthZ_pdgId_parent);
   fChain->SetBranchAddress("truthZ_child_n", &truthZ_child_n, &b_truthZ_child_n);
   fChain->SetBranchAddress("truthZ_child_pt", &truthZ_child_pt, &b_truthZ_child_pt);
   fChain->SetBranchAddress("truthZ_child_m", &truthZ_child_m, &b_truthZ_child_m);
   fChain->SetBranchAddress("truthZ_child_eta", &truthZ_child_eta, &b_truthZ_child_eta);
   fChain->SetBranchAddress("truthZ_child_phi", &truthZ_child_phi, &b_truthZ_child_phi);
   fChain->SetBranchAddress("truthZ_child_pdgId", &truthZ_child_pdgId, &b_truthZ_child_pdgId);
   fChain->SetBranchAddress("truthZ_child_charge", &truthZ_child_charge, &b_truthZ_child_charge);
   fChain->SetBranchAddress("truthZ_child_status", &truthZ_child_status, &b_truthZ_child_status);
   fChain->SetBranchAddress("truthW_n", &truthW_n, &b_truthW_n);
   fChain->SetBranchAddress("truthW_pt", &truthW_pt, &b_truthW_pt);
   fChain->SetBranchAddress("truthW_m", &truthW_m, &b_truthW_m);
   fChain->SetBranchAddress("truthW_eta", &truthW_eta, &b_truthW_eta);
   fChain->SetBranchAddress("truthW_phi", &truthW_phi, &b_truthW_phi);
   fChain->SetBranchAddress("truthW_pdgId", &truthW_pdgId, &b_truthW_pdgId);
   fChain->SetBranchAddress("truthW_charge", &truthW_charge, &b_truthW_charge);
   fChain->SetBranchAddress("truthW_status", &truthW_status, &b_truthW_status);
   fChain->SetBranchAddress("truthW_pdgId_parent", &truthW_pdgId_parent, &b_truthW_pdgId_parent);
   fChain->SetBranchAddress("truthW_child_n", &truthW_child_n, &b_truthW_child_n);
   fChain->SetBranchAddress("truthW_child_pt", &truthW_child_pt, &b_truthW_child_pt);
   fChain->SetBranchAddress("truthW_child_m", &truthW_child_m, &b_truthW_child_m);
   fChain->SetBranchAddress("truthW_child_eta", &truthW_child_eta, &b_truthW_child_eta);
   fChain->SetBranchAddress("truthW_child_phi", &truthW_child_phi, &b_truthW_child_phi);
   fChain->SetBranchAddress("truthW_child_pdgId", &truthW_child_pdgId, &b_truthW_child_pdgId);
   fChain->SetBranchAddress("truthW_child_charge", &truthW_child_charge, &b_truthW_child_charge);
   fChain->SetBranchAddress("truthW_child_status", &truthW_child_status, &b_truthW_child_status);
   fChain->SetBranchAddress("truthSpecial_n", &truthSpecial_n, &b_truthSpecial_n);
   fChain->SetBranchAddress("truthSpecial_pt", &truthSpecial_pt, &b_truthSpecial_pt);
   fChain->SetBranchAddress("truthSpecial_m", &truthSpecial_m, &b_truthSpecial_m);
   fChain->SetBranchAddress("truthSpecial_eta", &truthSpecial_eta, &b_truthSpecial_eta);
   fChain->SetBranchAddress("truthSpecial_phi", &truthSpecial_phi, &b_truthSpecial_phi);
   fChain->SetBranchAddress("truthSpecial_pdgId", &truthSpecial_pdgId, &b_truthSpecial_pdgId);
   Notify();
}

Bool_t MC_NOMINAL::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void MC_NOMINAL::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t MC_NOMINAL::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef MC_NOMINAL_cxx
